<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmonBonificacion extends Model
{
    protected $table = "AdmonBonificacion";

    protected $primaryKey = "idBonificacion";
    
    protected $fillable = ["idInventario","CantidadComprada","UnidadComprada","Estado"];
}
