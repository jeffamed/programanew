<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    //protected $table = "Bancos";

    protected $primaryKey = 'idBanco';

    protected $fillable = ['nombre', 'telefono', 'Estado'];

    //public $timestamps = false;
}
