<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CambioDolar extends Model
{
    protected $table = "Cambio_Dolar";

    protected $primaryKey = "idDolar";

    public $fillable = ['FechaHora','Cambio'];

}
