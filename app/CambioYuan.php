<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CambioYuan extends Model
{
    protected $table = "Cambio_Yuan";

    protected $primaryKey = "idYuan";

    public $fillable = ["FechaHora","Cambio"];

}
