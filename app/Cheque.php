<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    protected $table = 'Cheque';

    protected $primaryKey = 'idCheque';

    protected $fillable = ['idBanco','idTalonario','idCliente','NumCheque','Beneficiario','Monto','Estado','FechaHora','Usuario','Nota'];
}
