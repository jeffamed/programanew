<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChequeMinuta extends Model
{
    private $table = "ChequeMinuta";

    private $primaryKey = "idChequeMinuta";
    
    private $fillable = ['idBanco','idTalonario','idCliente','numChequeMinuta','numRef','beneficiario','monto','estado','Usuario','Nota','fechaHora'];
}
