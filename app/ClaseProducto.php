<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaseProducto extends Model
{
    protected $table = 'Clase_Producto';

    protected $primaryKey = 'idClase';

    public $fillable = ['Nombre','idDepartamento'];

}
