<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'Clientes';

    protected $primaryKey = 'idCliente';

    protected $fillable = ['idZona', 'Nombre', 'Telefono1', 'Telefono2', 'Celular', 'Direccion', 'DireccionHabitacion', 'Cedula', 'TipoVenta', 'Email','LimiteCredito','PlazoPago','FechaIngreso','Estado','idVendedor', 'FechaNac','Observacion','Codigo'];

}
