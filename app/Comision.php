<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
    protected $table = 'ComisionVendedores';

    protected $primaryKey = 'idComision';

    protected $fillable = ['TotalComision','Observaciones','idVendedor','FechaInicio','FechaFin','FechaPago'];
}
