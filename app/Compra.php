<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $table = 'Compras';

    protected $primaryKey = 'idCompra';

    protected $fillable = ['idProveedor','NumRecibo','FechaCompra','Total','Poliza','IVA','DAI','ISC','TipoCompra','EmpresaSolicitante','Observacion'];
}
