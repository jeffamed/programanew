<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use DB;

class EnviarCorreos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enviar:correo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar corte del dia al jefe por correo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cont = DB::table('Inventario as p')->select(DB::raw('count(p.idInventario) as Total'))->get()->pluck('Total');

        Mail::to('jeffamed@gmail.com')->send(new DemoEmail($cont));
    }
}
