<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use DB;


class probandocommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia la venta total del dia al jefe';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cont = DB::table('Facturas as p')->select(DB::raw('count(p.idFactura) as Total'))->get()->pluck('Total');

        Mail::to('jeffamed@gmail.com')->send(new DemoEmail($cont));

        echo('enviado');
    }
}
