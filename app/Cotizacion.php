<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    protected $table = 'Cotizacion';

    protected $primaryKey = 'idCotizacion';

    protected $fillable  = ['idVendedor','idCliente','LimiteCredito','TasaDolar','Total','Estado','TipoCompra','TotalComision','PlazoPago','SaldoRestante','NombreSolicitud'];
}
