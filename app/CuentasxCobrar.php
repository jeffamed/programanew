<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentasxCobrar extends Model
{
    protected $table = 'Cuentas_X_Cobrar';
    
    protected $primaryKey = 'idCuentasxCobrar';

    protected $fillable = ['idFactura', 'FechaVencimiento','FechaRecuperacionV', 'TotalDeuda','SaldoRestante','Estado', 'TotalComision','ManeraCanc','FechaCancelacion','Recibo'];
}
