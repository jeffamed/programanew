<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallaPagosCxC extends Model
{
    protected $table = 'DetallePago';
    
    protected $primaryKey = 'idDetallePago';

    protected $fillable = ['idPagoCxC','idCxC','Monto','Descuento','FechaRecibo'];
}
