<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleBonificacion extends Model
{
    protected $table = "DetalleBonificacion";

    protected $primaryKey = "idDetalle";

    protected $fillable = ["idBonificacion","idInventario","Cantidad","Precio","TotalDado","TotalBonificacion"];
}
