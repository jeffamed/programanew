<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCheque extends Model
{
    protected $table = "DetalleCheque";

    protected $primaryKey = "idDetalleCheque";

    protected $fillable = ['idCheque','idCxC','Monto'];
}
