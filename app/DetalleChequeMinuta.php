<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleChequeMinuta extends Model
{
    protected $table = "DetalleChequeMinuta";

    protected $primaryKey = "idDetChequeMin";

    protected $fillable = ['idChequeMinuta','idCxC','monto','descuento'];
}
