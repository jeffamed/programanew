<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleComision extends Model
{
    protected $table = 'DetalleComision';

    protected $primaryKey = 'idDetalleComision';

    protected $fillable = ['idCxC','idComision','Comision'];
}
