<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompra extends Model
{
    protected $table = 'Detalle_Compras';

    protected $primaryKey = 'idDetalleCompras';

    protected $fillable = ["idInventario","idCompras","Cantidad","PrecioDolar","PrecioYuan","FechaVencimiento","PrecioCordoba","TasaYuan","TasaDolar"];
}
