<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCotizacion extends Model
{
    protected $table = 'DetalleCotizacion';

    protected $primaryKey = 'idDetalle';

    protected $fillable = ['idInventario','idCotizacion','Cantidad','Precio','PorcComision'];
}
