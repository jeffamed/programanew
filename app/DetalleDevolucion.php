<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDevolucion extends Model
{
    protected $table = 'DetalleDevolucion';

    protected $primaryKey = 'idDetalleDevoluciones';

    protected $fillable = ['idDetalleFactura','idDevolucion','Cantidad'];
}
