<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleEntrada extends Model
{
    protected $table = 'Detalle_Entrada';

    protected $primaryKey = 'idDetalleEntrada';
    
    protected $fillable = ['idEntrada','idInventario','Cantidad','Precio'];
}
