<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePrefactura extends Model
{
    protected $table = 'DetallePrefacturas';

    protected $primaryKey = 'idDetPref';

    protected  $fillable = [
        "idPrefactura",
        "idInventario",
        "Precio",
        "PrecioDolar",
        "Cantidad",
        "PorcComision",
        "created_at",
        "updated_at",
    ];
}
