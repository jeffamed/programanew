<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleSalida extends Model
{
    protected $table = 'Detalle_Salidas';

    protected $primaryKey = 'idDetalleSalida';

    protected $fillable = ['idInventario','idSalida','Cantidad','Precio'];
}
