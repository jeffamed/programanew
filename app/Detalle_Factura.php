<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detalle_Factura extends Model
{
    protected $table = 'Detalle_Facturas';

    protected $primaryKer = 'idDetalleFactura';

    protected $fillable = ['idInventario','idFactura','Cantidad','Precio','PrecioDolar','PorcComision','PrecioDolar','TipoPrecio'];
}
