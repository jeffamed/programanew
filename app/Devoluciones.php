<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devoluciones extends Model
{
    protected $table = 'Devoluciones';

    protected $primaryKey = 'idDevoluciones';

    protected $fillable = ['Comentario','Total', 'idFactura', 'TotalComision','Usuario'];
}
