<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntradaProducto extends Model
{
    protected $table = 'Entrada_Producto';

    protected $primaryKey = 'idEntrada';

    protected $fillable= ['FechaEntrada','Comentario','Total','Estado','Usuario'];
}
