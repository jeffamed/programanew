<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = "Estado_Cuenta";

    protected $primaryKey = "idEstado";

    protected $fillable = ["idCliente","idFactura","Concepto","Descripcion","Entrada","Salida"];
}
