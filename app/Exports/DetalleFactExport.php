<?php

namespace App\Exports;

use App\Detalle_Factura;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;

/*class DetalleFactExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    /*public function collection()
    {
        return Detalle_Factura::all();
    }
}
*/

/*class DetalleFactExport implements FromView
{
    private $inicio;
    private $fin;

    public function __construct(string $inicio,string $fin)
    {
        $this->inicio = $inicio;
        $this->fin = $fin;
    }

    public function view(): View
    {
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $ventas = DB::select('exec sp_VentasProdDet ?, ?', [$this->inicio, $this->fin]);

        $total = DB::select('exec sp_VentasProdTotal ?, ?', [$this->inicio, $this->fin]);

        $totalFinal = DB::select('exec sp_TotalVentasProd ?, ?',[$this->inicio, $this->fin]);
        
        $del = Carbon::parse($this->inicio)->format('d-m-Y');
        $al = Carbon::parse($this->fin)->format('d-m-Y');

        return view('pdf.excelventasprod', [
            'ventas'=>$ventas,
            'total'=>$total, 
            'final'=>$totalFinal,
            'hoy'=>$hoy, 
            'user'=>$usuario,
            'del'=>$del, 
            'al'=>$al
        ]);
    }
}*/

class DetalleFactExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $finicio;
    protected $ffin;

    function __construct($inicio, $fin){
        $this->finicio = $inicio;
        $this->ffin = $fin;
    }

    /*public function collection()
    {
        return DB::select(DB::RAW("select c.Codigo,c.Nombre, i.Cod_Producto, i.Nombre as Producto, sum( df.Cantidad) as Cantidad, ISNULL(dc.PrecioCordoba,0)  AS Costo, df.Precio as Precio, 
        df.Precio-ISNULL(dc.PrecioCordoba,0) as Utilidad, Concat(((df.Precio-ISNULL(dc.PrecioCordoba,0))/df.Precio)*100,'%') as PorcUtilidad
        from Detalle_Facturas as df
        left join Inventario as i
        on df.idInventario = i.idInventario
        left join Facturas as f
        on df.idFactura = f.idFactura
        left join Clientes as c
        on f.idCliente = c.idCliente
        left join Detalle_Compras as dc
        on i.idInventario = dc.idInventario
        where CONVERT(varchar,df.created_at,103) >= '15/07/2020'
        group by c.Nombre, i.Cod_Producto, i.Nombre, dc.PrecioCordoba, df.Precio, c.Codigo
        order by c.Nombre"));
    }*/

    public function view(): View
    {
       $usuario =  \Auth::user()->name;

        $todo = DB::select('exec sp_TotalVentasProd ?, ?',[$this->finicio, $this->ffin]);

        $ventas = DB::select('exec sp_VentasProdDet ?, ?', [$this->finicio, $this->ffin]);

        $total = DB::select('exec sp_VentasProdTotal ?, ?', [$this->finicio, $this->ffin]);


        return view('pdf.excelventasprod',["final"=>$todo, "total"=>$total, "ventas"=>$ventas ,"user"=>$usuario,'finicio'=>$this->finicio,'ffin'=>$this->ffin]);
    }
}