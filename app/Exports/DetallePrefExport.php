<?php

namespace App\Exports;

use App\DetallePrefactura;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;

/*class DetallePrefExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    
    public function collection()
    {
        return DetallePrefactura::all();
    }
}*/

class DetallePrefExport implements FromView
{
    private $finicio;
    private $ffin;

    public function __construct($inicio, $fin)
    {
        $this->finicio = $inicio;
        $this->ffin = $fin;
    }

    public function view():View
    {
        $usuario =  \Auth::user()->name;

        $ventas = DB::select('exec sp_PreventasProdDetalle ?, ?', [$this->finicio, $this->ffin]);

        $total = DB::select('exec sp_PreventasProdTotal ?, ?', [$this->finicio, $this->ffin]);

        return view('pdf.excelpreventas',["total"=>$total, "ventas"=>$ventas ,"user"=>$usuario,'finicio'=>$this->finicio,'ffin'=>$this->ffin]);
    }
}
