<?php

namespace App\Exports;

use App\Productos;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use DB;

class InfProdDepExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $departamento;

    function __construct($departamento){
        $this->departamento = $departamento;
    }

    public function view(): View
    {
       $usuario =  \Auth::user()->name;

       $departamento = DB::table('Departamentos')->select('Nombre')->where('idDepartamentos','=',$this->departamento)->pluck('Nombre')->first();

        $info = DB::select('exec sp_informeInvDpto ?', [$this->departamento]);

        return view('pdf.productodepartamento',["info"=>$info,"user"=>$usuario, "dpto"=>$departamento]);
    }
}
