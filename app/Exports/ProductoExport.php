<?php

namespace App\Exports;

use App\Productos;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


use DB;

/*class ProductoExport implements FromCollection
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
/*    public function collection()
    {
        return Productos::select('Cod_Producto','Nombre','Stock')->orderBy('Nombre','asc')->get();
    }
}*/

class ProductoExport implements FromView
{
    public function view(): View
    {
        //$productos = Producto::
        return view('pdf.listadoproducto', [
            //'productos' => Productos::orderBy('Nombre', 'asc')->get()
            'productos' => DB::table('Inventario')->orderBy('Nombre','asc')->get()
        ]);
    }
}

