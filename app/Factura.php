<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $primaryKey = 'idFactura';

    protected $fillable = ['idVendedor', 'idCliente','Estado','MotivoAnulacion','LimiteCredito','TasaDolar','TipoCompra','Total','usuario', 'TipoFactura','TotalDolar','NombreFactura','AutorizadoAnul','NumFact'];

}
