<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdmonBonificacion;
use App\DetalleBonificacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class AdmonBonificacionController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $bonificacion = DB::table('AdmonBonificacion as ab')->join('Inventario as i', 'ab.idInventario' ,'=','i.idInventario')
                            ->select('ab.idBonificacion','i.Nombre as Producto','ab.CantidadComprada as Caja','ab.UnidadComprada as Unidad','ab.Estado')
                            ->orderBy('idBonificacion','desc')->paginate(10);
        } else {
            $bonificacion = DB::table('AdmonBonificacion as ab')->join('Inventario as i', 'ab.idInventario' ,'=','i.idInventario')
                            ->select('ab.idBonificacion','i.Nombre as Producto','ab.CantidadComprada as Caja','ab.UnidadComprada as Unidad','ab.Estado')
                            ->where('i.nombre','like','%'.$buscar.'%')
                            ->orderBy('idBonificacion','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$bonificacion->total(),
                'current_page'=>$bonificacion->currentPage(),
                'per_page'=>$bonificacion->perPage(),
                'last_page'=>$bonificacion->lastPage(),
                'from'=>$bonificacion->firstItem(),
                'to'=>$bonificacion->lastItem(),
            ], 
            'bonificacion' => $bonificacion
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();

            $bonificacion = new AdmonBonificacion();
            $bonificacion->idInventario = $request->idProductoC;
            $bonificacion->CantidadComprada = $request->CantidadC;
            $bonificacion->UnidadComprada = ($request->CantidadC * $request->Unidad);
            $bonificacion->Estado = 'Activo';
            $bonificacion->save();

            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                $detalle = new DetalleBonificacion();
                $detalle->idBonificacion = $bonificacion->idBonificacion;
                $detalle->idInventario = $det['idproducto'];
                $detalle->Cantidad = $det['cantidad'];
                $detalle->Precio = $det['precio'];
                $detalle->TotalBonificacion = $det['total'];
                $detalle->TotalDado = 0;
                $detalle->save();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            DB::rollback();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $bonificacion = AdmonBonificacion::findOrFail($request->idBonificacion);
        $bonificacion->CantidadComprada = $request->CantidadC;
        $bonificacion->UnidadComprada = ($request->CantidadC * $request->Unidad);
        $bonificacion->save();

        $detalles = $request->data;

        foreach ($detalles as $key => $det) {
            $detalle = DetalleBonificacion::where([['idBonificacion','=',$request->idBonificacion],['idInventario','=',$det['idInventario']]])->firstOrFail();
            $detalle->Cantidad = $det['CantidadDar'];
            $detalle->TotalBonificacion = $det['total'];
            $detalle->save();
        }
    }

    public function DatosAdmonBonificacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idBonificacion = $request->id;
        $bonificacion=DB::select('exec sp_DatosAdmonBonificaciones ?', [$idBonificacion]);

        return ['bonificacion'=>$bonificacion];
    }
    public function ProdBonificacion(Request $request)
    {
        $detalle = $request->data;
        foreach($detalle as $key=>$det){
            $cantidad = AdmonBonificacion::select('UnidadComprada')->where('idInventario','=',$det['idInventario'])->firstOrFail();
            $regalia = floor($cantidad/$det['cantidad']);
            if($regalia > 0){
                $bonificacion = DB::select('exec sp_DetProdBonificado ?, ?', $det['idInventario'],$regalia);
            }
        }
        dd($bonificacion);
        return['bonificacion'=>$bonificacion];
    }
    
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $bonificacion = AdmonBonificacion::findOrFail($request->idBonificacion);
        $bonificacion->Estado = 'Inactivo';
        $bonificacion->save();
    }
}
