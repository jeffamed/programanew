<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Banco;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $banco = Banco::orderBy('nombre','asc')->paginate(10);
        } else {
            $banco = Banco::where('nombre','like','%'.$buscar.'%')->orderBy('nombre','asc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$banco->total(),
                'current_page'=>$banco->currentPage(),
                'per_page'=>$banco->perPage(),
                'last_page'=>$banco->lastPage(),
                'from'=>$banco->firstItem(),
                'to'=>$banco->lastItem(),
            ], 
            'banco' => $banco
        ];
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $banco = new Banco();
        $banco->nombre = $request->nombre;
        $banco->telefono = $request->telefono;
        $banco->Estado = 'Activo';
        $banco->save();
    }

    public function ListarBancos(Request $request)
    {
        $banco = DB::select('exec sp_listarBanco');
        return ['bancos'=>$banco];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $banco = Banco::findOrFail($request->idBanco);
        $banco->nombre = $request->nombre;
        $banco->telefono = $request->telefono;
        $banco->Estado = 'Activo';
        $banco->save();
    }
    /* Deactivar Registro */
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $banco = Banco::findOrFail($request->idBanco);
        $banco->Estado = 'Inactivo';
        $banco->save();
    }
    /* Activar Registro */
    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $banco = Banco::findOrFail($request->idBanco);
        $banco->Estado = 'Activo';
        $banco->save();
    }
}
