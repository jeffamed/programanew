<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use ZipArchive;
use DB;
use Illuminate\Support\Carbon;


class CambiaProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function zip(Request $request)
    {   
        if($request->tipo == 'select'){
            $fecha1 = $request->fechaInicio;
            $fecha2= $request->fechaFin;
        }else if($request->tipo == 'siete'){
            $hoy = Carbon::now();
            $fecha1 = $hoy->subWeek()->toDateString();
            $fecha2= $hoy->toDateString();
        }else{
            $hoy = Carbon::now();
            $fecha1 = $hoy->startofMonth()->toDateString();
            $fecha2= $hoy->endofMonth()->toDateString();
        }
        //$imgs = ['01-02-00001.jpg','01-07-00037.jpg','01-07-00048.jpg','20-07-0017.jpg','01-01-034.jpg','01-01-035.jpg','01-01-036.jpg','01-01-037.jpg','19-06-403.jpg','19-06-401.jpg','19-06-405.jpg'];
        $imgs = DB::table('Inventario')->select('Imagen')->whereBetween('created_at',[$fecha1,$fecha2])->get()->pluck('Imagen');
        
        $dowload = true;
        
        if($dowload) {
            // Define Dir Folder
            $public_dir=public_path('/imagenes/productos/');
            // Zip File Name
            $zipFileName = 'ImgSelect.zip';
            // Create ZipArchive Obj
            $zip = new ZipArchive;
            
            if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) !== TRUE) {
                exit("cannot open <>\n");
                /*$zip->deleteName('19-06-457.jpg');
                $zip->deleteName('19-06-461.jpg');
                @$zip->close();*/
            }
            // Add File in ZipArchive
            foreach ($imgs as $img) {
                //$zip->addFile(public_path('imagenes/productos/'.$img.'.jpg'),$img.'.jpg');
                $zip->addFile(public_path('imagenes/productos/'.$img),$img);
            }
            // Close ZipArchive     
            $zip->close();
            // Set Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
            $filetopath=$public_dir.'/'.$zipFileName;
            // Create Download Response
            if(file_exists($filetopath)){
                return response()->download($filetopath,$zipFileName,$headers);
            }
        }
    }

    public function deleteZip()
    {
        if(file_exists(public_path('imagenes/productos/ImgSelect.zip'))){
            unlink(public_path('imagenes/productos/ImgSelect.zip'));
            //$this->zip($request);
        }
        //else{
            //$this->zip($request);
        //}
    }
}
