<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CambioDolar;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CambioDolarController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $dolar = CambioDolar::orderBy('created_at','desc')->paginate(10);
            //$dolar = DB::table('Cambio_Dolar')->select('idDolar','FechaHora',DB::raw('ROUND(Cambio, 2) as Cambio'))->paginate(10);
        } else {
            $dolar = CambioDolar::where('created_at','like','%'.$buscar.'%')->orderBy('created_at','asc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$dolar->total(),
                'current_page'=>$dolar->currentPage(),
                'per_page'=>$dolar->perPage(),
                'last_page'=>$dolar->lastPage(),
                'from'=>$dolar->firstItem(),
                'to'=>$dolar->lastItem(),
            ], 
            'dolar' => $dolar
        ];
    }

    public function ultimoRegistro(Request $request)
    {   
        if (!$request->ajax())  return redirect('/');

        $dolar = DB::table('Cambio_Dolar')->select(DB::RAW('Convert(date, created_at) as Fecha'))->orderBy('created_at','desc')->pluck('Fecha')->first();
        $hoy = Carbon::now()->toDateString();
        if($dolar == $hoy){
            $rps = 'SI';
        } else{
            $rps = 'NO';

        }
        //return ['rps'=>$rps,'dolar'=>$dolar,'hoy'=>$hoy];
        return ['rps'=>$rps];

    }

    public function UltimaTasa(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $tasa = DB::select('exec sp_UltimaTasa');
        return['tasa'=>$tasa];
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $dolar =  new CambioDolar();
        $dolar->Cambio = $request->Cambio;
        $dolar->FechaHora = Carbon::now();
        $dolar->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $dolar = CambioDolar::findOrFail($request->idDolar);
        $dolar->Cambio = $request->Cambio;
        $dolar->save();
    }
}
