<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CambioYuan;
use Illuminate\Support\Carbon;

class CambioYuanController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $yuan = CambioYuan::orderBy('created_at','desc')->paginate(10);
        } else {
            $yuan = CambioYuan::where('created_at','like','%'.$buscar.'%')->orderBy('created_at','asc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$yuan->total(),
                'current_page'=>$yuan->currentPage(),
                'per_page'=>$yuan->perPage(),
                'last_page'=>$yuan->lastPage(),
                'from'=>$yuan->firstItem(),
                'to'=>$yuan->lastItem(),
            ], 
            'yuan' => $yuan
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $yuan =  new CambioYuan();
        $yuan->Cambio = $request->Cambio;
        $yuan->FechaHora = Carbon::now();
        $yuan->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $yuan = CambioYuan::findOrFail($request->idYuan);
        $yuan->Cambio = $request->Cambio;
        $yuan->save();
    }
}
