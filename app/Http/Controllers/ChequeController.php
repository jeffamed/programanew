<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cheque;
use App\DetalleCheque;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;


class ChequeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if ($buscar == '') {
            $cheque = DB::table('Cheque as ch')->join('Bancos as b', 'ch.idBanco', '=', 'b.idBanco')->join('Clientes as c', 'ch.idCliente', '=', 'c.idCliente')->join('Talonarios as t','ch.idTalonario', '=', 't.idTalonario')
            ->select('ch.idCheque', 't.NumeracionInicial', 'c.Nombre as Cliente', 'c.idCliente','b.nombre as Banco',DB::RAW('convert(varchar,CAST(ch.Monto as money),1) as Monto1'), 'ch.Monto','ch.Estado')
                        ->orderBy('ch.idCheque','desc')->paginate(10);
        } else {
            $cheque = DB::table('Cheque as ch')->join('Bancos as b', 'ch.idBanco', '=', 'b.idBanco')->join('Clientes as c', 'ch.idCliente', '=', 'c.idCliente')->join('Talonarios as t','ch.idTalonario', '=', 't.idTalonario')
            ->select('ch.idCheque', 't.NumeracionInicial', 'c.Nombre as Cliente', 'c.idCliente','b.nombre as Banco',DB::RAW('convert(varchar,CAST(ch.Monto as money),1) as Monto1'), 'ch.Monto','ch.Estado')
                    ->where($filtro,'like','%'.$buscar.'%')
                    ->orderBy('ch.idCheque','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$cheque->total(),
                'current_page'=>$cheque->currentPage(),
                'per_page'=>$cheque->perPage(),
                'last_page'=>$cheque->lastPage(),
                'from'=>$cheque->firstItem(),
                'to'=>$cheque->lastItem(),
            ], 
            'cheque' => $cheque
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $cheque = new Cheque();
        $cheque->idTalonario = $request->idTalonario;
        $cheque->idBanco = $request->idBanco;
        $cheque->idCliente = $request->idCliente;
        $cheque->NumCheque = $request->NumCheque;
        $cheque->Beneficiario = $request->Beneficiario;
        $cheque->Monto = $request->Monto;
        $cheque->Usuario = \Auth::user()->name;
        $cheque->FechaHora = Carbon::now();
        $cheque->Estado = 'Pendiente';
        $cheque->Nota = $request->Nota;
        $cheque->save();


        /*try {
            DB::beginTransaction();

            
            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                $detalle = new DetalleCheque();
                $detalle->idCheque = $cheque->idCheque;
                $detalle->idCxC = $det['idCuentasxCobrar'];
                $detalle->Monto = $det['Abono'];
                $detalle->save();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            DB::rollback();
        }*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $cheque = Cheque::findOrFail($request->idcheque);
        $cheque->idTalonario = $request->idTalonario;
        $cheque->idBanco = $request->idBanco;
        $cheque->idCliente = $request->idCliente;
        $cheque->NumCheque = $request->NumCheque;
        $cheque->Beneficiario = $request->Beneficiario;
        $cheque->Monto = $request->Monto;
        $cheque->FechaHora = Carbon::now();
        $cheque->Estado = 'Pendiente';
        $cheque->Nota = $request->Nota;
        $cheque->save();

        $detalles = $request->data;

        foreach ($detalles as $key => $det) {
            $detalle = DetalleCheque::where([['idCheque','=',$request->idcheque],['idCxC','=',$det['idCuentasxCobrar']]])->firstOrFail();
            $detalle->Monto = $det['Abono'];
            $detalle->save();
        }

    }
    public function DatosCheque(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idCheque = $request->id;
        $cheque=DB::select('exec sp_DatosCheque ?', [$idCheque]);

        return ['cheque'=>$cheque];
    }
    
    /* Deactivar Registro */
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $cheque = Cheque::findOrFail($request->idCheque);
        //$cheque->Estado = 'Sin Cobra';
        $cheque->Estado = 'Anulado';
        $cheque->save();
    }
    /* Activar Registro */
    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $detalles = $request->data;

        foreach ($detalles as $key => $det) {
            $detalle = new DetalleCheque();
            $detalle->idCheque = $request->idCheque;
            $detalle->idCxC = $det['idCuentasxCobrar'];
            $detalle->Monto = $det['Abono'];
            $detalle->Descuento = $det['Descuento'];
            $detalle->save();
        }

        $cheque = Cheque::findOrFail($request->idCheque);
        $cheque->Estado = 'Aplicado';
        $cheque->save();
    }
}
