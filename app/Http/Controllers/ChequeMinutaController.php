<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ChequeMinutaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if ($buscar == '') {
            $cheque = DB::table('ChequeMinuta as ch')->join('Bancos as b', 'ch.idBanco', '=', 'b.idBanco')->join('Clientes as c', 'ch.idCliente', '=', 'c.idCliente')->join('Talonarios as t','ch.idTalonario', '=', 't.idTalonario')
                    ->select('ch.idChequeMinuta','ch.numChequeMinuta', 't.NumeracionInicial', 'c.Nombre as Cliente', 'c.idCliente','b.nombre as Banco',DB::RAW('convert(varchar,CAST(ch.monto as money),1) as Monto1'), 'ch.monto','ch.estado')
                    ->orderBy('ch.idChequeMinuta','desc')->paginate(10);
        } else {
            $cheque = DB::table('ChequeMinuta as ch')->join('Bancos as b', 'ch.idBanco', '=', 'b.idBanco')->join('Clientes as c', 'ch.idCliente', '=', 'c.idCliente')->join('Talonarios as t','ch.idTalonario', '=', 't.idTalonario')
            ->select('ch.idChequeMinuta','ch.numChequeMinuta', 't.NumeracionInicial', 'c.Nombre as Cliente', 'c.idCliente','b.nombre as Banco',DB::RAW('convert(varchar,CAST(ch.monto as money),1) as Monto1'), 'ch.monto','ch.estado')
                    ->where($filtro,'like','%'.$buscar.'%')
                    ->orderBy('ch.idChequeMinuta','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$cheque->total(),
                'current_page'=>$cheque->currentPage(),
                'per_page'=>$cheque->perPage(),
                'last_page'=>$cheque->lastPage(),
                'from'=>$cheque->firstItem(),
                'to'=>$cheque->lastItem(),
            ], 
            'cheque' => $cheque
        ];
    }
}
