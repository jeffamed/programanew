<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClaseProducto;
use Illuminate\Support\Facades\DB;

class ClaseProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;

        if($buscar == ''){
            //$clase = ClaseProducto::orderBy('Nombre','asc')->paginate(10);
            $clase = DB::table('Clase_Producto as cp')
                        ->join('Departamentos as d','cp.idDepartamento','=','d.idDepartamentos')
                        ->select('cp.idClase','cp.Nombre as Categoria','d.Nombre as Departamento')
                        ->orderBy('cp.Nombre','asc')->paginate(10);
        }else{
            $clase = DB::table('Clase_Producto as cp')
                        ->join('Departamentos as d','cp.idDepartamento','=','d.idDepartamentos')
                        ->select('cp.idClase','cp.Nombre as Categoria','d.Nombre as Departamento')           
                        ->where('cp.Nombre','like','%'.$buscar.'%')
                        ->orderBy('cp.Nombre','asc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$clase->total(),
                'current_page'=>$clase->currentPage(),
                'per_page'=>$clase->perPage(),
                'last_page'=>$clase->lastPage(),
                'from'=>$clase->firstItem(),
                'to'=>$clase->lastItem(),
            ], 
            'clase' => $clase
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $clase = new ClaseProducto();
        $clase->Nombre = $request->Nombre;
        $clase->idDepartamento = $request->iddepartamento;
        $clase->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $clase = ClaseProducto::findOrFail($request->idClase);
        $clase->Nombre = $request->Nombre;
        $clase->idDepartamenot = $request->iddepartamento;
        $clase->save();
    }

    public function SeleccionarClase(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $id = $request->Departamento;

        $clase = DB::select('exec sp_listarClase ?', [$id]);

        return ['clase'=>$clase];
    }

    public function allSelect(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $clase = ClaseProducto::orderBy('Nombre','asc')->get();
        return ['clases' => $clase];
    }

}
