<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if($buscar == ''){
            $cliente = DB::table('Clientes as c')->join('Zonas as z','c.idZona','=','z.idZona')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')
                        ->select('c.idCliente', 'z.idZona','z.Nombre as Zonas', 'v.idVendedor','v.Nombre as Vendedor', 'c.Nombre as Clientes', 'c.Telefono1', 'c.Telefono2', 'c.Celular', 'c.Direccion', 'c.DireccionHabitacion',
                        'c.Cedula', 'c.TipoVenta','c.Email','c.LimiteCredito','c.PlazoPago', 'c.Estado', 'c.Codigo','c.Observacion','c.FechaNac', DB::RAW('CONVERT(varchar,c.created_at,103) as FRegistro'))
                        ->orderBy('c.idCliente', 'desc')->paginate(10);
        }else{
            $cliente = DB::table('Clientes as c')->join('Zonas as z','c.idZona','=','z.idZona')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')
                        ->select('c.idCliente', 'z.idZona','z.Nombre as Zonas', 'v.idVendedor','v.Nombre as Vendedor', 'c.Nombre as Clientes', 'c.Telefono1', 'c.Telefono2', 'c.Celular', 'c.Direccion', 'c.DireccionHabitacion',
                        'c.Cedula', 'c.TipoVenta','c.Email','c.LimiteCredito','c.PlazoPago', 'c.Estado', 'c.Codigo','c.Observacion','c.FechaNac', DB::RAW('CONVERT(varchar,c.created_at,103) as FRegistro'))
                        ->where('c.Nombre','like','%'. $buscar .'%')
                        ->orderBy('c.idCliente', 'desc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$cliente->total(),
                'current_page'=>$cliente->currentPage(),
                'per_page'=>$cliente->perPage(),
                'last_page'=>$cliente->lastPage(),
                'from'=>$cliente->firstItem(),
                'to'=>$cliente->lastItem(),
            ],
            'cliente' => $cliente
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response                                                                    
     */
    public function store(Request $request)
    {
        $count=DB::table('Clientes')->where('Codigo','like','T%')->count();
        $cantidad = intval($count + 1);
        
        if (!$request->ajax())  return redirect('/');
        $cliente = new Cliente();
        $cliente->idZona = $request->idZona;
        $cliente->idVendedor = $request->idVendedor;
        $cliente->Nombre = $request->Nombre;
        $cliente->Telefono1 = $request->Telefono1;
        $cliente->Telefono2 = $request->Telefono2;
        $cliente->Celular = $request->Celular;
        $cliente->Direccion = $request->Direccion;
        $cliente->DireccionHabitacion = $request->DireccionHabitacion;
        $cliente->Cedula = $request->Cedula;
        $cliente->TipoVenta = $request->TipoVenta;
        $cliente->Email = $request->Email;
        $cliente->LimiteCredito = $request->LimiteCredito;
        $cliente->PlazoPago = $request->PlazoPago;
        $cliente->FechaNac = $request->FechaNac;
        $cliente->Observacion = $request->Observacion;
        $cliente->FechaIngreso = Carbon::now();
        if ($request->tipoC=='Permanente') {
            $cliente->Codigo = $request->Codigo;
        }else{
            $cliente->Codigo = 'T'.($cantidad);
        }
        $cliente->Estado = 'Activo';
        $cliente->save();
    }

    public function SeleccionarCliente(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $search = $request->buscar;

        $cliente = DB::table('Clientes as c')->join('Vendedores as v', 'c.idVendedor', '=', 'v.idVendedor')->join('Zonas as z', 'c.idZona', '=', 'z.idZona')
                    ->leftjoin('Facturas as f', 'c.idCliente', '=', 'f.idCliente')->leftjoin('Cuentas_X_Cobrar as cc','f.idFactura', '=', 'cc.idFactura')
                    ->select('c.idCliente','c.Codigo',DB::RAW("concat(c.Codigo,'-',c.Nombre) as Cliente"), 'c.LimiteCredito', 'c.PlazoPago', 'v.idVendedor', 'v.Nombre as Vendedor', 'z.Nombre as Zona', 'c.Direccion' ,'c.TipoVenta', 'c.Telefono1 as Telefono',
                    DB::RAW("ISNULL(sum(CASE WHEN cc.Estado = 'Activo' then cc.SaldoRestante END),0) as Deuda") )
                    ->where([
                        ['c.Estado', '=', 'Activo'],
                        ['c.Nombre', 'like', $search.'%'],
                        ])
                    ->orWhere('c.Codigo', 'like', '%'.$search.'%')
                    ->groupBy('c.LimiteCredito', 'c.PlazoPago','v.idVendedor', 'v.Nombre', 'z.Nombre', 'c.idCliente','c.Nombre','c.Codigo','c.TipoVenta','c.Direccion','c.Telefono1')
                    ->orderBy('c.idCliente')->take(50)->get();       
       //$cliente = DB::select('exec sp_listarClientes ? ', [$search]);
        //$cliente = DB::select('exec sp_listarClientes');

        return['cliente'=>$cliente];
    }

    public function SeleccionarClienteNombre(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $search = $request->buscar;

        $cliente = DB::table('Clientes as c')->join('Vendedores as v', 'c.idVendedor', '=', 'v.idVendedor')->join('Zonas as z', 'c.idZona', '=', 'z.idZona')
                    ->leftjoin('Facturas as f', 'c.idCliente', '=', 'f.idCliente')->leftjoin('Cuentas_X_Cobrar as cc','f.idFactura', '=', 'cc.idFactura')
                    ->select('c.idCliente','c.Codigo','c.Nombre as Cliente', 'c.LimiteCredito', 'c.PlazoPago', 'v.idVendedor', 'v.Nombre as Vendedor', 'z.Nombre as Zona', 'c.Direccion' ,'c.TipoVenta', 'c.Telefono1 as Telefono',
                    DB::RAW("ISNULL(sum(CASE WHEN cc.Estado = 'Activo' then cc.SaldoRestante END),0) as Deuda") )
                    ->where([
                        ['c.Estado', '=', 'Activo'],
                        ['c.Nombre', 'like', $search.'%'],
                        ])
                    ->orWhere('c.Codigo', 'like', '%'.$search.'%')
                    ->groupBy('c.LimiteCredito', 'c.PlazoPago','v.idVendedor', 'v.Nombre', 'z.Nombre', 'c.idCliente','c.Nombre','c.Codigo','c.TipoVenta','c.Direccion','c.Telefono1')
                    ->orderBy('c.idCliente')->take(50)->get();       
        return['cliente'=>$cliente];
    }

    public function SelectClienteFact(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $search = $request->buscar;

        $cliente = DB::table('Clientes as c')->join('Vendedores as v', 'c.idVendedor', '=', 'v.idVendedor')->join('Zonas as z', 'c.idZona', '=', 'z.idZona')
                    ->leftjoin('Facturas as f', 'c.idCliente', '=', 'f.idCliente')->leftjoin('Cuentas_X_Cobrar as cc','f.idFactura', '=', 'cc.idFactura')
                    ->select('c.idCliente','c.Nombre', 'c.Codigo','c.LimiteCredito', 'c.PlazoPago', 'v.idVendedor', 'v.Nombre as Vendedor', 'z.Nombre as Zona', 'c.Direccion' ,'c.TipoVenta', 'c.Telefono1 as Telefono',
                    DB::RAW("ISNULL(sum(CASE WHEN cc.Estado = 'Activo' then cc.SaldoRestante END),0) as Deuda") )
                    ->where([
                        ['c.Estado', '=', 'Activo'],
                        ['c.Codigo', 'like', '%'.$search],
                        ])
                    ->groupBy('c.LimiteCredito','c.Codigo' ,'c.PlazoPago','v.idVendedor', 'v.Nombre', 'z.Nombre', 'c.idCliente','c.Nombre','c.Codigo','c.TipoVenta','c.Direccion','c.Telefono1')
                    ->orderBy('c.idCliente')->take(50)->get();       

        return['cliente'=>$cliente];
    }
    
    public function BuscarNombre(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $search = $request->buscar;

        $cliente = DB::table('Clientes as c')->join('Vendedores as v', 'c.idVendedor', '=', 'v.idVendedor')->join('Zonas as z', 'c.idZona', '=', 'z.idZona')
                    ->leftjoin('Facturas as f', 'c.idCliente', '=', 'f.idCliente')->leftjoin('Cuentas_X_Cobrar as cc','f.idFactura', '=', 'cc.idFactura')
                    ->select('c.idCliente','c.Codigo','c.Nombre', 'c.LimiteCredito', 'c.PlazoPago', 'v.idVendedor', 'v.Nombre as Vendedor', 'z.Nombre as Zona', 'c.Direccion' ,'c.TipoVenta', 'c.Telefono1 as Telefono',
                    DB::RAW("ISNULL(sum(CASE WHEN cc.Estado = 'Activo' then cc.SaldoRestante END),0) as Deuda") )
                    ->where([
                        ['c.Nombre', 'like', '%'.$search.'%'],
                        ['c.Estado', '=', 'Activo']
                        ])
                    ->groupBy('c.LimiteCredito', 'c.PlazoPago','v.idVendedor', 'v.Nombre', 'z.Nombre', 'c.idCliente','c.Nombre','c.Codigo','c.TipoVenta','c.Direccion','c.Telefono1')
                    ->orderBy('c.idCliente')->take(10)->get();       
        
        return['cliente'=>$cliente];
    }

    public function ClienteFactura(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $clientes = DB::select('exec sp_listarClientesxPagar');
        return ['clientes'=>$clientes];
    }

    public function ClienteCheque(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $clientes = DB::select('exec sp_listarClientesSaldo');
        return ['clientes'=>$clientes];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        if (!$request->ajax())  return redirect('/');
        $cliente = Cliente::findOrFail($request->idCliente);
        $cliente->idZona = $request->idZona;
        $cliente->idVendedor = $request->idVendedor;
        $cliente->Nombre = $request->Nombre;
        $cliente->Telefono1 = $request->Telefono1;
        $cliente->Telefono2 = $request->Telefono2;
        $cliente->Celular = $request->Celular;
        $cliente->Direccion = $request->Direccion;
        $cliente->DireccionHabitacion = $request->DireccionHabitacion;
        $cliente->Cedula = $request->Cedula;
        $cliente->TipoVenta = $request->TipoVenta;
        $cliente->Email = $request->Email;
        $cliente->LimiteCredito = $request->LimiteCredito;
        $cliente->PlazoPago = $request->PlazoPago;
        $cliente->FechaNac = $request->FechaNac;
        $cliente->Observacion = $request->Observacion;
        $cliente->FechaIngreso = Carbon::now();
        $cliente->Codigo = $request->Codigo;
        $cliente->Estado = 'Activo';
        $cliente->save();
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $cliente = Cliente::findOrFail($request->idCliente);
        $cliente->Estado = 'Inactivo';
        $cliente->save();
    }

    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $cliente = Cliente::findOrFail($request->idCliente);
        $cliente->Estado = 'Activo';
        $cliente->save();
    }

    public function SeleccionarAll(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $buscar = $request->buscar;
        if($buscar == ''){
            $clientes = Cliente::orderBy('Nombre','asc')->get();
        }else{
            $clientes = Cliente::orderBy('Nombre','asc')->where('Nombre','LIKE','%'.$buscar.'%')->get();
        }
        return ['clientes'=>$clientes];
    }
    
    public function ActualizarDeuda(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $search = $request->buscar;

        $cliente = DB::table('Clientes as c')->join('Vendedores as v', 'c.idVendedor', '=', 'v.idVendedor')->join('Zonas as z', 'c.idZona', '=', 'z.idZona')
                    ->leftjoin('Facturas as f', 'c.idCliente', '=', 'f.idCliente')->leftjoin('Cuentas_X_Cobrar as cc','f.idFactura', '=', 'cc.idFactura')
                    ->select(DB::RAW("ISNULL(sum(CASE WHEN cc.Estado = 'Activo' then cc.SaldoRestante END),0) as Deuda") )
                    ->where([
                        ['c.Estado', '=', 'Activo'],
                        ['c.idCliente', '=', $search],
                        ])
                    ->groupBy('c.LimiteCredito', 'c.PlazoPago','v.idVendedor', 'v.Nombre', 'z.Nombre', 'c.idCliente','c.Nombre','c.Codigo','c.TipoVenta','c.Direccion','c.Telefono1')
                    ->orderBy('c.idCliente')->take(1)->get();       
        return['cliente'=>$cliente];
    }

}
