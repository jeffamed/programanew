<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comision;
use App\DetalleComision;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ComisionController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $comisiones = DB::table('ComisionVendedores as cv')->join('Vendedores as v','cv.idVendedor','=','v.idVendedor')
                        ->select('cv.idComision', 'v.Nombre', 'cv.TotalComision', 'cv.Observaciones') ->orderBy('cv.idComision', 'desc')
                       ->paginate(10);
        }else{
            $comisiones =  DB::table('ComisionVendedores as cv')->join('Vendedores as v','cv.idVendedor','=','v.idVendedor')
                            ->select('cv.idComision', 'v.Nombre', 'cv.TotalComision', 'cv.Observaciones')
                            ->where('v.Nombre','like','%'. $buscar .'%')
                            ->orderBy('cv.idComision', 'desc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$comisiones->total(),
                'current_page'=>$comisiones->currentPage(),
                'per_page'=>$comisiones->perPage(),
                'last_page'=>$comisiones->lastPage(),
                'from'=>$comisiones->firstItem(),
                'to'=>$comisiones->lastItem(),
            ],
            'comisiones' =>$comisiones
        ];
    }

    public function Fechas(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $anio= $request->anio;
        $mes = $request->mes;
        $quince =  $request->quince;
        $hoy = Carbon::now();
        $primero =  Carbon::createFromDate($anio, $mes, 1,'America/Managua')->startofMonth()->format('d-m-Y');
        $segundo =  Carbon::createFromDate($anio, $mes, 1,'America/Managua')->endofMonth()->format('d-m-Y');
        if($quince == 1){
            $actual = $primero;
            $ultimo = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('d-m-Y');
        }else if($quince == 2){
            $actual = Carbon::createFromDate($anio, $mes, 16,'America/Managua')->format('d-m-Y');
            $ultimo = $segundo;
        }

        return ['actual'=>$actual,'ultimo'=>$ultimo];
    }

    public function fechaActual()
    {
        if (!$request->ajax())  return redirect('/');

        $hoy=Carbon::now('America/Managua')->format('Y-m-d');
        return ['hoy'=>$hoy];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        try {
            DB::beginTransaction();
            
           $comision = new Comision();
           $comision->idVendedor = $request->id;
           $comision->TotalComision = $request->TotalComision;
           $comision->Observaciones = $request->Observacion;
           $comision->FechaInicio = $request->FechaInicio;
           $comision->FechaFin = $request->FechaFin;
           $fin=Carbon::parse($request->FechaInicio);
           $inicio=Carbon::parse($request->FechaFin);
           if ($inicio->day>=1 && $fin->day<=15) {
                $comision->FechaPago = Carbon::now()->startOfMonth()->addDay(14)->format('d-m-Y');
           }else{
                $comision->FechaPago = Carbon::now()->endOfMonth()->format('d-m-Y');
           }
           $comision->save();

            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                $detalle =  new DetalleComision();
                $detalle->idComision = $comision->idComision;
                $detalle->idCxC = $det['idCuentasxCobrar'];
                $detalle->Comision = $det['TotalComision'];
                $detalle->save();
            }
            
            DB::commit();

        } catch (\Throwable $th) {
            DB::rollback();
        }
    }

    public function DatosComision(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idComision = $request->id;
        $comision=DB::select('exec sp_DatosComisiones ?', [$idComision]);

        return ['comision'=>$comision];
    }

    public function DetalleFactura(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idFactura = $request->id;
        $detalle = DB::select('exec sp_DetalleComision ? ',[$idFactura]);

        return ['detalle'=>$detalle];
    }

    public function generarComisiones($id,$quincena,$mes,$anio)
    {
        $fechaprim = Carbon::createFromDate($anio, $mes, 1,'America/Managua')->startofMonth()->format('Y-m-d');
        $fechaultimo = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('Y-m-d');
        $fecha = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('Y-m-d');
        if($quincena == 1){
            $fechaprim = Carbon::createFromDate($anio, $mes, 1,'America/Managua')->startofMonth()->format('Y-m-d');
            $fechaultimo = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('Y-m-d');
            $fecha = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('Y-m-d');
        }else if($quincena == 2){
            $fechaprim = Carbon::createFromDate($anio, $mes, 16,'America/Managua')->format('Y-m-d');
            $fechaultimo = Carbon::createFromDate($anio, $mes, 1,'America/Managua')->endofMonth()->format('Y-m-d');
            $fecha = Carbon::createFromDate($anio, $mes, 30,'America/Managua')->format('Y-m-d');
        }

        $comision = DB::select('exec sp_DetalleComisionpdf ?, ?, ?', [$id,$fechaprim,$fechaultimo]);
        $totalfact = DB::select('exec sp_totalFacturaxComision ?, ?, ?', [$id,$fechaprim,$fechaultimo]);
        $detalle = DB::select('exec sp_VendedorComision ?,?,?', [$id,$fechaprim,$fechaultimo]);
        $detalle1 = DB::select('exec sp_VendedorComisionNombre ?,?,?', [$id,$fechaprim,$fechaultimo]);
        $primero = $detalle1[0];
        $hoy = Carbon::now()->format('d-m-Y');
        $totalcomision = DB::table('ComisionVendedores')->count()+1;
        $pdf = \PDF::loadView('pdf.comision', ['comision'=>$comision,'hoy'=>$hoy,'total'=>$totalcomision,'fecha'=>$fecha,
         'primero'=>$fechaprim,'ultimo'=>$fechaultimo, 'totalfact'=>$totalfact, 'detalle'=>$detalle,'detalle1'=>$primero])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function DetalleComisiones($id)
    {
        $comision = DB::select('exec sp_DetallexComision ?', [$id]);
        $totalfactura = DB::select('exec sp_TotalFacturaComision ?', [$id]);
        $hoy = Carbon::now()->format('d-m-Y');
        $detalle = DB::table('ComisionVendedores as cv')->join('Vendedores as v','cv.idVendedor','=','v.idVendedor')
                    ->select('cv.idComision', 'v.Nombre as Vendedor','cv.Observaciones','cv.FechaInicio','cv.FechaFin','cv.FechaPago','cv.TotalComision')
                    ->where('cv.idComision','=',$id)->get();
        $pdf = \PDF::loadView('pdf.comisionoriginal', ['comision'=>$comision,'hoy'=>$hoy,'totalfactura'=>$totalfactura,'detalle'=>$detalle])->setPaper("letter","portrait");
        
        return $pdf->stream();
    }

}
