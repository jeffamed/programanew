<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Compra;
use App\DetalleCompra;
use App\Kardex;
use Illuminate\Support\Facades\DB;

class CompraController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $compra = DB::table('Compras')
                        ->select('idCompra','NumRecibo',DB::RAW('convert(varchar,CAST(Total as money),1) as Total'), 'TipoCompra', DB::RAW('CONVERT(varchar,FechaCompra,105) as fechacompra'), DB::RAW('CONVERT(varchar,created_at,105) as fecharegistro')) 
                        ->orderBy('idCompra', 'desc')
                        ->paginate(10);
        }else{
            $compra = DB::table('Compras')  
                        ->select('idCompra','NumRecibo',DB::RAW('convert(varchar,CAST(Total as money),1) as Total'), 'TipoCompra', DB::RAW('CONVERT(varchar,FechaCompra,105) as fechacompra'), DB::RAW('CONVERT(varchar,created_at,105) as fecharegistro')) 
                        ->where($filtro,'like','%'. $buscar .'%')
                        ->orderBy('idCompra', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$compra->total(),
                'current_page'=>$compra->currentPage(),
                'per_page'=>$compra->perPage(),
                'last_page'=>$compra->lastPage(),
                'from'=>$compra->firstItem(),
                'to'=>$compra->lastItem(),
            ],
            'compra' =>$compra
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
     if (!$request->ajax())  return redirect('/');
        try {

            DB::beginTransaction();
            $compra = new Compra();
            $compra->idProveedor = $request->idProveedor;
            $compra->NumRecibo = $request->NumRecibo;
            $compra->FechaCompra = $request->FechaCompra;
            $compra->TipoCompra = $request->TipoCompra;
            if ($request->TipoCompra == 'Contable') {
                $compra->Poliza = $request->Poliza;
                $compra->IVA = $request->IVA;
                $compra->DAI = $request->DAI;
                $compra->ISC = $request->ISC;
            }
            $compra->EmpresaSolicitante = $request->EmpresaSolicitante;
            $compra->Observacion = $request->Observacion;
            $compra->Total = $request->Total;
            $compra->save();

            $detalles = $request->data;
            
            foreach ($detalles as $ep => $det) {
                $detalle = new DetalleCompra();
                $detalle->idCompras = $compra->idCompra;
                $detalle->idInventario  = $det['idproducto'];
                $detalle->Cantidad = $det['cantidad'];
                $detalle->PrecioDolar = $det['precioDolar'];
                $detalle->PrecioYuan = $det['precioYuan'];
                $detalle->PrecioCordoba = $det['precioCordoba'];
                $detalle->FechaVencimiento = $det['vencimiento'];
                $detalle->TasaYuan = $det['tasaYuan'];
                $detalle->TasaDolar = $det['tasaDolar'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idproducto'];
                $kardex->NumDoc = $compra->idCompra;
                $kardex->Descripcion = 'Compra No. Rec: '.$request->NumRecibo;
                $kardex->TipoDoc = 'Compra';
                $kardex->Cantidad = $det['cantidad'];
                $kardex->Cliente = $request->EmpresaSolicitante;
                $kardex->Costo = $det['precioCordoba'];
                $kardex->Observacion =  $request->Observacion;
                $kardex->save();
            }

            DB::commit();

            //return ['id' => $compra->idCotizacion];
            
        } catch (\Throwable $th) {
            DB::rollBack();
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $compra = Compra::findOrFail($request->idCompra);
        $compra->idProveedor = $request->idProveedor;
        $compra->NumRecibo = $request->NumRecibo;
        $compra->FechaCompra = $request->FechaCompra;
        $compra->TipoCompra = $request->TipoCompra;
        $compra->Poliza = $request->Poliza;
        $compra->IVA = $request->IVA;
        $compra->DAI = $request->DAI;
        $compra->ISC = $request->ISC;
        $compra->EmpresaSolicitante = $request->EmpresaSolicitante;
        $compra->Observacion = $request->Observacion;
        $compra->Total = $request->Total;
        $compra->save();

        $detalles = $request->data;
        $detalles2 = $request->data2;

        foreach ($detalles as $key => $det) {
            $detalle = DetalleCompra::where([['idCompras','=',$request->idCompra],['idInventario','=',$det['idInventario']]])->firstOrFail();
            $detalle->Cantidad = $det['Cantidad'];
            $detalle->PrecioDolar = $det['PrecioDolar'];
            $detalle->PrecioYuan = $det['PrecioYuan'];
            $detalle->PrecioCordoba = $det['PrecioCordoba'];
            $detalle->FechaVencimiento = $det['FechaVencimiento'];
            $detalle->TasaYuan = $det['TasaYuan'];
            $detalle->TasaDolar = $det['TasaDolar'];
            $detalle->save();

            $kardex = Kardex::where([['idInventario','=',$det['idInventario']],['NumDoc','=',$request->idCompra],['TipoDoc','=','Compra']])->firstOrFail();
            $kardex->Cantidad = $det['Cantidad'];
            $kardex->save();
        }
        foreach ($detalles2 as $ep => $det) {
            $detalle = new DetalleCompra();
            $detalle->idCompras = $compra->idCompra;
            $detalle->idInventario  = $det['idproducto'];
            $detalle->Cantidad = $det['cantidad'];
            $detalle->PrecioDolar = $det['precioDolar'];
            $detalle->PrecioYuan = $det['precioYuan'];
            $detalle->PrecioCordoba = $det['precioCordoba'];
            $detalle->FechaVencimiento = $det['vencimiento'];
            $detalle->TasaYuan = $det['tasaYuan'];
            $detalle->TasaDolar = $det['tasaDolar'];
            $detalle->save();

            $kardex = new Kardex();
            $kardex->idInventario = $det['idproducto'];
            $kardex->NumDoc = $compra->idCompra;
            $kardex->Descripcion = 'Compra No. Rec: '.$request->NumRecibo;
            $kardex->TipoDoc = 'Compra';
            $kardex->Cantidad = $det['cantidad'];
            $kardex->Cliente = $request->EmpresaSolicitante;
            $kardex->Costo = $det['precioCordoba'];
            $kardex->Observacion =  $request->Observacion;
            $kardex->save();
        }

    }

    public function DatosCompra(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
        $id = $request->idCompra;
        $detalle = DB::select('exec sp_CargarCompras ?', [$id]);
        $totales = DB::table('Detalle_Compras')->select(DB::RAW('Convert(money,sum(Cantidad * PrecioDolar),1) as TotalDolares, Convert(money,sum(Cantidad * PrecioYuan),1) as TotalYuanes'))->where('idCompras','=',$id)->get();
        return ['datos'=>$detalle, 'totales'=>$totales];
    }

}
