<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cotizacion;
use App\DetalleCotizacion;
use App\Factura;
use App\Detalle_Factura;
use App\Kardex;
use App\CuentasxCobrar;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CotizacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $cotizacion = DB::table('Cotizacion as c')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')->join('Clientes as cl','c.idCliente','=','cl.idCliente')
                        ->select('c.idCotizacion','v.Nombre as Vendedor', 'v.idVendedor','cl.Nombre as Cliente', 'cl.idCliente','c.Total', 'c.created_at', 'c.TipoCompra', 'c.TotalComision','c.Estado') 
                        ->orderBy('c.idCotizacion', 'desc')
                       ->paginate(10);
        }else{
            $cotizacion = DB::table('Cotizacion as c')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')->join('Clientes as cl','c.idCliente','=','cl.idCliente')
                        ->select('c.idCotizacion','v.Nombre as Vendedor', 'cl.Nombre as Cliente', 'c.Total', 'c.created_at', 'c.TipoCompra', 'c.TotalComision','c.Estado') 
                        ->where($filtro,'like','%'. $buscar .'%')
                        ->orderBy('c.idCotizacion', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$cotizacion->total(),
                'current_page'=>$cotizacion->currentPage(),
                'per_page'=>$cotizacion->perPage(),
                'last_page'=>$cotizacion->lastPage(),
                'from'=>$cotizacion->firstItem(),
                'to'=>$cotizacion->lastItem(),
            ],
            'cotizacion' =>$cotizacion
        ];
    }

    /*public function DetalleFactura(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->idFactura;
        $detalle = DB::select('exec sp_listarDetalleFactura ?', [$id]);

        return ['detalle' => $detalle];
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    if (!$request->ajax())  return redirect('/');
        try {

            DB::beginTransaction();
            $cotizacion = new Cotizacion();
            $cotizacion->idVendedor = $request->idVendedor;
            $cotizacion->idCliente = $request->idCliente;
            $cotizacion->LimiteCredito = $request->creditoDisponible;
            $cotizacion->TasaDolar = $request->TasaDolar;
            $cotizacion->Total = $request->Total;
            $cotizacion->TipoCompra = $request->TipoCompra;
            $cotizacion->PlazoPago = $request->Plazo;
            if ($request->TipoCompra == 'Contado') {
                $cotizacion->TotalComision = 0;
                $cotizacion->SaldoRestante = 0;
            }else{
                $cotizacion->TotalComision = $request->TotalComision;
                $cotizacion->SaldoRestante = $request->Total;
            }
            $cotizacion->Estado = 'Cotizando';
            $cotizacion->NombreSolicitud = $request->NombreSolicitante;
            $cotizacion->save();

            $detalles = $request->data;
            
            foreach ($detalles as $ep => $det) {
                $detalle = new DetalleCotizacion();
                $detalle->idCotizacion = $cotizacion->idCotizacion;
                $detalle->idInventario  = $det['idInventario'];
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->PorcComision = $det['PorcComision'];
                $detalle->save();
                /*$detalle->idInventario  = $det['idproducto'];
                $detalle->Cantidad = $det['cantidad'];
                $detalle->Precio = $det['precio'];
                $detalle->PorcComision = $det['comision'];
                $detalle->save();*/
            }

            DB::commit();

            return ['id' => $cotizacion->idCotizacion];
            
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
     if (!$request->ajax())  return redirect('/');

        $cotizacion = Cotizacion::findOrFail($request->idCotizacion);
        $cotizacion->idVendedor = $request->idVendedor;
        $cotizacion->idCliente = $request->idCliente;
        $cotizacion->LimiteCredito = $request->creditoDisponible;
        $cotizacion->TasaDolar = $request->TasaDolar;
        $cotizacion->Total = $request->Total;
        $cotizacion->TipoCompra = $request->TipoCompra;
        $cotizacion->PlazoPago = $request->Plazo;
        if ($request->TipoCompra == 'Contado') {
            $cotizacion->TotalComision = 0;
            $cotizacion->SaldoRestante = 0;
        }else{
            $cotizacion->TotalComision = $request->TotalComision;
            $cotizacion->SaldoRestante = $request->Total;
        }
        $cotizacion->Estado = 'Cotizando';
        $cotizacion->NombreSolicitud = $request->NombreSolicitante;
        $cotizacion->save();

        $detalles = $request->data;
        $eliminar = $request->delete;
        
        foreach ($detalles as $ep => $det) {
            $detalle = DetalleCotizacion::where([['idCotizacion','=',$request->idCotizacion],['idInventario','=',$det['idInventario']]])->first();
            if ($detalle == Null) {
                $detalle = new DetalleCotizacion();
                $detalle->idCotizacion = $request->idCotizacion;
                $detalle->idInventario = $det['idInventario'];
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->PorcComision = $det['PorcComision'];
                $detalle->save();
            }else{
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->save();

            }
        }

        if(count($eliminar)<>0){
            foreach ($eliminar as $dl => $elim) {
                $detalle = DetalleCotizacion::where([['idCotizacion','=',$request->idCotizacion],['idInventario','=',$elim['idInventario']]])->first();
                $detalle->delete();
            }
        }
    }


    public function generarCotizacion($id)
    {
        $cotizacion = DB::select('exec sp_listaDatosCotizacion ?', [$id]);

        $detalle = DB::select('exec sp_CargarDetalleCotizacion ?', [$id]);

        $total  = DB::select('exec sp_TotalesComision ?', [$id]);

        $pdf = \PDF::loadView('pdf.cotizacion', ['cotizacion'=>$cotizacion,'detalle'=>$detalle,'total'=>$total])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function generarCotizacionMini($id)
    {
        $cotizacion = DB::select('exec sp_listaDatosCotizacion ?', [$id]);

        $detalle = DB::select('exec sp_CargarDetalleCotizacion ?', [$id]);
     
        $total  = DB::select('exec sp_TotalesComision ?', [$id]);

        $pdf = \PDF::loadView('pdf.cotizacionMini', ['cotizacion'=>$cotizacion,'detalle'=>$detalle, "total"=>$total])->setPaper("letter","landscape");
        return $pdf->stream();
    }

    public function facturar(Request $request)
    {
           
        if (!$request->ajax())  return redirect('/');
        
        try {
            
            DB::beginTransaction();
             $factura = new Factura();
            $factura->idVendedor = $request->idVendedor;
            $factura->idCliente = $request->idCliente;
            $factura->LimiteCredito = $request->creditoDisponible;
            $factura->TipoCompra = $request->TipoCompra;
            $factura->TasaDolar = $request->TasaDolar;
            $factura->TipoFactura = $request->TipoFactura;
            $factura->Total = $request->Total;
            //$factura->TotalDolar = $request->TotalDolar;
            $factura->usuario =  $request->usuario;
            $factura->NombreFactura = $request->nombreFact;
            if($request->TipoCompra == 'Contado')
            {
                $factura->Estado = 'Pagado';
            }else{
                $factura->Estado = 'Facturado';
            }
            $factura->NumFact=DB::raw("NEXT VALUE FOR [dbo].[SecFact]");
            $factura->save();

            $detalles = $request->data;
            
            foreach ($detalles as $ep => $det) {
                $detalle = new Detalle_Factura();
                $detalle->idFactura = $factura->idFactura;
                $detalle->idInventario  = $det['idInventario'];
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->PrecioDolar = 0;
                $detalle->PorcComision = $det['PorcComision'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idInventario'];
                $kardex->NumDoc = $factura->idFactura;
                $kardex->Descripcion = 'Factura No.: '.$factura->idFactura;
                $kardex->TipoDoc = 'Factura';
                $kardex->Cantidad = $det['Cantidad'];
                $kardex->Cliente = $request->Cliente;
                $kardex->Costo = $det['Precio'];
                $kardex->save();
            }
            
            $cuentas = new CuentasxCobrar();
            $cuentas->idFactura = $factura->idFactura;
            $cuentas->TotalDeuda = $request->Total;
            $cuentas->Estado = 'Activo';
            if ($request->TipoCompra == 'Contado') {
                $cuentas->SaldoRestante = 0;
                $cuentas->TotalComision = 0;
                $cuentas->FechaVencimiento = Carbon::now();
                $cuentas->FechaRecuperacionV = Carbon::now();
                $cuentas->ManeraCanc = 'Contado';
                $cuentas->FechaCancelacion = Carbon::now();
            }else{
                $cuentas->SaldoRestante = $request->Total;
                $cuentas->TotalComision = $request->TotalComision;
                $cuentas->FechaVencimiento = Carbon::now()->addDay($request->Plazo);
                $cuentas->FechaRecuperacionV = Carbon::now()->addDay(62);
            }
            $cuentas->save();

            DB::commit();

            return ['id' => $factura->idFactura];
            
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function DetalleCotizacion(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
        $id = $request->idCotizacion;
        $detalle = DB::select('exec sp_CargarDetalleCotizacion ?', [$id]);
        return ['detalle'=>$detalle];
    }

    public function DatosCotizacion(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
        $id = $request->idCotizacion;
        $datos = DB::select('exec sp_CargarDatosCotizacion ?', [$id]);
        return ['datos'=>$datos];
    }

    public function Estado(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $cotizacion = Cotizacion::findOrFail($request->idCotiz);
       $cotizacion->Estado = "Facturado";
       $cotizacion->idFactura = $request->idFact;
       $cotizacion->save();
    }

}
