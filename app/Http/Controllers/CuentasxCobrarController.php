<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

use Illuminate\Support\Facades\Auth;


class CuentasxCobrarController extends Controller
{
   
    public function Comision(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $vendedor = $request->id;
        $mes = $request->mes;
        $anio = $request->anio;
        $quincena = $request->quincena;
        $primero =  Carbon::createFromDate($anio, $mes, 1,'America/Managua')->startofMonth()->format('Y-m-d');
        $segundo =  Carbon::createFromDate($anio, $mes, 1,'America/Managua')->endofMonth()->format('Y-m-d');
        if($quincena == 1){
            $actual = $primero;
            $ultimo = Carbon::createFromDate($anio, $mes, 15,'America/Managua')->format('Y-m-d');
        }else if($quincena == 2){
            $actual = Carbon::createFromDate($anio, $mes, 16,'America/Managua')->format('Y-m-d');
            $ultimo = $segundo;
        }

        $comision=DB::select('exec sp_CargaComision ?, ?, ?', [$vendedor, $actual, $ultimo]);

        return ['comision'=>$comision];
    }


    public function NameUser()
    {
        $usuario =  \Auth::user()->name;

        return ['usuario'=>$usuario];
    }

}