<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departamentos;
use Illuminate\Support\Facades\DB;

class DepartamentosController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $buscar = $request->buscar;

        if($buscar == ''){
            $departamento = Departamentos::orderBy('Nombre', 'asc')->paginate(10);
        }else{
            $departamento = Departamentos::where('Nombre','like','%'. $buscar .'%')->orderBy('Nombre', 'asc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$departamento->total(),
                'current_page'=>$departamento->currentPage(),
                'per_page'=>$departamento->perPage(),
                'last_page'=>$departamento->lastPage(),
                'from'=>$departamento->firstItem(),
                'to'=>$departamento->lastItem(),
            ],
            'departamento' => $departamento
        ];
        //return $marca;
    }


    public function SeleccionarDepartamentos(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $departamento = DB::select('exec sp_Departamento');

        return ['departamento'=>$departamento];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $departamento = new Departamentos();
        $departamento->Nombre = $request->Nombre;
        $departamento->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $departamento = Departamentos::findOrFail($request->idDepartamento);
        $departamento->Nombre = $request->Nombre;
        $departamento->save();
    }

    public function SeleccionarAll(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        if($buscar == ''){
            $departamentos = Departamentos::orderBy('Nombre','asc')->get();
        }else{
            $departamentos = Departamentos::where('Nombre','LIKE','%'.$buscar.'%')->orderBy('Nombre','asc')->get();

        }
        return ["departamentos"=>$departamentos];
    }
}
