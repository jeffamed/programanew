<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Devoluciones;
use App\DetalleDevolucion;
use App\NotasCredito;
use App\Kardex;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DevolucionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $devoluciones = DB::table('Devoluciones as d')->join('Facturas as f','d.idFactura', '=', 'f.idFactura')->join('Clientes as c','f.idCliente','=','c.idCliente')
                        ->select( 'd.idDevoluciones', 'd.idFactura', 'c.Nombre', 'd.Total', 'd.created_at') ->orderBy('d.idDevoluciones', 'desc')
                       ->paginate(10);
        }else{
            $devoluciones =  DB::table('Devoluciones as d')->join('Facturas as f','d.idFactura', '=', 'f.idFactura')->join('Clientes as c','f.idCliente','=','c.idCliente')
                            ->select('d.idDevoluciones', 'd.idFactura', 'c.Nombre', 'd.Total', 'd.created_at')
                            ->where($filtro,'like','%'. $buscar .'%')
                            ->orderBy('d.idDevoluciones', 'desc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$devoluciones->total(),
                'current_page'=>$devoluciones->currentPage(),
                'per_page'=>$devoluciones->perPage(),
                'last_page'=>$devoluciones->lastPage(),
                'from'=>$devoluciones->firstItem(),
                'to'=>$devoluciones->lastItem(),
            ],
            'devoluciones' =>$devoluciones
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        try {
            DB::beginTransaction();
            
            $devolucion = new Devoluciones();
            $devolucion->Comentario = $request->Comentario;
            $devolucion->Total = $request->Total;
            $devolucion->TotalComision = $request->TotalComision;
            $devolucion->idFactura = $request->idfactura;
            $devolucion->save();

            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                if ($det['Devoluciones'] != 0 ) {
                    $detalle =  new DetalleDevolucion();
                    $detalle->idDevolucion = $devolucion->idDevoluciones;
                    $detalle->idDetalleFactura = $det['idDetalleFactura'];
                    $detalle->Cantidad = $det['Devoluciones'];
                    $devolucion->Usuario=\Auth::user()->name;
                    $detalle->save();

                    $kardex = new Kardex();
                    $kardex->idInventario = $det['idInventario'];
                    $kardex->NumDoc = $devolucion->idDevoluciones;
                    $kardex->Descripcion = 'Devolución No.: '.$devolucion->idDevoluciones;
                    $kardex->TipoDoc = 'Devolucion';
                    $kardex->Cliente = $request->Cliente;
                    $kardex->Cantidad = $det['Devoluciones'];
                    $kardex->Costo = $det['Precio'];
                    $kardex->Observacion = $request->Comentario;
                    $kardex->save();
                }
            }

            $tasaactual = DB::table('Cambio_Dolar')->select('Cambio')->orderBy('idDolar','desc')->get()->pluck('Cambio')->first();
            $tasaactual = (float)$tasaactual;
        
            $nota = new NotasCredito();
            $nota->idCliente = $request->idCliente;
            $nota->idFactura = $request->idfactura;
            $nota->Monto = $request->Total;
            $nota->TipoNota = 'Devolucion';
            $nota->TasaCambio = $tasaactual;
            $nota->Observacion = 'Devolucion porque ' . $request->Comentario;
            $nota->NoDevolucion = $devolucion->idDevoluciones;
            $nota->save(); 

            DB::commit();

            return ['devolucion'=>$devolucion->idDevoluciones];
        } catch (\Throwable $th) {
            DB::rollback();
        }
    }

    public function DatosDevolucion(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $idDevolucion = $request->id;
        $devolucion=DB::select('exec sp_DatosDevolucion ?', [$idDevolucion]);

        return ['devolucion'=>$devolucion];
    }

    public function PrintDevoluciones($id)
    {
        $fecha = Carbon::now();
        $hoy = $fecha->format('d/m/Y');
        $usuario = \Auth::user()->name;
        $datos = DB::table('Devoluciones as d')->join('Facturas as f','d.idFactura','=','f.idFactura')->join('Clientes as c','f.idCliente','=','c.idCliente')->
        select('d.idDevoluciones',DB::RAW('CONVERT(varchar,d.created_at,103) as fecha'),'d.Comentario','c.Codigo','c.Nombre as Cliente','d.Total','f.TipoCompra','f.idFactura')
        ->where('d.idDevoluciones','=',$id)->get();
        $detalle = DB::table('DetalleDevolucion as dd')->join('Detalle_Facturas as df','dd.idDetalleFactura','=','df.idDetalleFactura')
        ->join('Inventario as i','df.idInventario','=','i.idInventario')
        ->select('i.Cod_Producto','i.Nombre','dd.Cantidad','df.Precio')
        ->where('dd.idDevolucion','=',$id)
        ->get();
        $pdf = \PDF::loadView('pdf.imprimirdevolucion', ['datos'=>$datos,'detalle'=>$detalle, "hoy"=>$hoy, "user"=>$usuario])->setPaper("letter","portrait");
        return $pdf->stream();
    }
    
    public function deshabilitar(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $factura = Devoluciones::findOrFail($request->idDevolucion);
       $factura->Anular = $request->MotivoAnulacion;
       $factura->Estado = 'Anulada';
       $factura->save();
    }
}
