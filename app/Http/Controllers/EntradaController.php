<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EntradaProducto;
use App\DetalleEntrada;
use App\Kardex;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class EntradaController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $entrada = EntradaProducto::orderBy('idEntrada','desc')->paginate(10);
        } else {
            $entrada = EntradaProducto::where('idEntrada','like','%'.$buscar.'%')
                    ->orderBy('idEntrada','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$entrada->total(),
                'current_page'=>$entrada->currentPage(),
                'per_page'=>$entrada->perPage(),
                'last_page'=>$entrada->lastPage(),
                'from'=>$entrada->firstItem(),
                'to'=>$entrada->lastItem(),
            ], 
            'entrada' => $entrada
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();

            $entrada = new EntradaProducto();
            $entrada->Comentario = $request->Observacion;
            $entrada->Total = $request->Total;
            $entrada->FechaEntrada = $request->FechaEntrada;
            $entrada->Estado = 'Activo';
            $entrada->Usuario = \Auth::user()->name;
            $entrada->save();

            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                $detalle = new DetalleEntrada();
                $detalle->idEntrada = $entrada->idEntrada;
                $detalle->idInventario = $det['idproducto'];
                $detalle->Cantidad = $det['entrada'];
                $detalle->Precio = $det['precio'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idproducto'];
                $kardex->NumDoc = $entrada->idEntrada;
                $kardex->Descripcion = 'Entrada No.: '.$entrada->idEntrada;
                $kardex->TipoDoc = 'Entrada';
                $kardex->Cliente = \Auth::user()->name;
                $kardex->Cantidad = $det['entrada'];
                $kardex->Costo = $det['precio'];
                $kardex->Observacion = $request->Observacion;
                $kardex->save();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            DB::rollback();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $entrada = EntradaProducto::findOrFail($request->idEntrada);
        $entrada->Comentario = $request->Observacion;
        $entrada->Total = $request->Total;
        $entrada->FechaEntrada = $request->FechaEntrada;
        $entrada->Estado = 'Activo';
        $entrada->save();

        $detalles = $request->data;

        foreach ($detalles as $key => $det) {
            $detalle = DetalleEntrada::where([['idEntrada','=',$request->idEntrada],['idInventario','=',$det['idInventario']]])->firstOrFail();
            $detalle->Cantidad = $det['Cantidad'];
            $detalle->save();

            $kardex = Kardex::where([['TipoDoc','=','Entrada'],['idInventario','=',$det['idInventario']],['NumDoc','=',$request->idEntrada]])->firstOrFail();
            $kardex->Cantidad = $det['Cantidad'];
            $kardex->save();
        }

    }
    public function DatosEntrada(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idEntrada = $request->id;
        $entrada=DB::select('exec sp_DatosEntrada ?', [$idEntrada]);

        return ['entrada'=>$entrada];
    }

    public function CostoProducto(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idInventario = $request->id;
        $costo=DB::select('exec sp_UltimoCostoProd ?', [$idInventario]);

        return ['costo'=>$costo];
    }
    
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $entrada = EntradaProducto::findOrFail($request->idEntrada);
        $entrada->Estado = 'Inactivo';
        $entrada->save();
    }

    public function ultimoRegistro(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = EntradaProducto::select('idEntrada')->orderBy('idEntrada','desc')->pluck('idEntrada')->first();
        return ['id'=>$id];
    }

    public function printEntrada($id)
    {
        $fecha = Carbon::now();
        $hoy = $fecha->format('d/m/Y');
        $usuario = \Auth::user()->name;
        $datos = DB::table('Entrada_Producto as e')
                ->select('e.idEntrada',DB::RAW('CONVERT(varchar,e.FechaEntrada,103) as fecha'),'e.Comentario','e.Total')
                ->where('e.idEntrada','=',$id)
                ->get();
        $detalle = DB::table('Detalle_Entrada as de')
                ->join('Inventario as i','de.idInventario','=','i.idInventario')
                ->select('i.Cod_Producto','i.Nombre','de.Cantidad','de.Precio')
                ->where('idEntrada','=',$id)
                ->get();
        $pdf = \PDF::loadView('pdf.entrada', ['datos'=>$datos,'detalle'=>$detalle,"user"=>$usuario ,"hoy"=>$hoy])->setPaper("letter","portrait");
        return $pdf->stream();
    }
}
