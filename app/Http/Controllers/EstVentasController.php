<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EstVentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $FInicio = $request->Inicio;
        $FFin = $request->Fin;
        $mostrar = $request->Mostrar;
        $ejeY = $request->EjeY;
        $ejeX = $request->EjeX;
        if($mostrar == 'Cantidad'){
            if($ejeY == 'Categoria' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaCatVendCantidad ?, ?",[$FInicio,$FFin]);
            } else if($ejeY == 'Producto' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaProdVendCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaClienteVendCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Categoria' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaCatZonaCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Producto' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaProdZonaCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaClienteZonaCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Categoria' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaCategoriaMesCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Producto' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaProdCantidad ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaClienteMesCantidad ?, ?",[$FInicio,$FFin]);
            }
        }
        else{
            if($ejeY == 'Categoria' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaCatVendMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Producto' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaProdVendMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Vendedor'){
                $detalle = DB::select("exec sp_EstVentaClienteVendMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Categoria' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaCatZonaMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Producto' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaProdZonaMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Zona'){
                $detalle = DB::select("exec sp_EstVentaClienteZonaMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Categoria' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaCategoriaMesMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Producto' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaProdMesMonto ?, ?",[$FInicio,$FFin]);
            }else if($ejeY == 'Cliente' && $ejeX == 'Mes'){
                $detalle = DB::select("exec sp_EstVentaClienteMesMonto ?, ?",[$FInicio,$FFin]);
            }
        }
            return['detalles'=>$detalle];
    }
}
