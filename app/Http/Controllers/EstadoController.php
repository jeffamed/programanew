<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class EstadoController extends Controller
{
    public function CargarEstado(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->id;
        $estado = DB::select('exec sp_EstadoCuenta ?', [$id]);

        return ['estado'=>$estado];
    }

    public function CargarId(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->id;
        $estado = DB::select('exec sp_idEstadoCuenta ?', [$id]);

        return ['idestado'=>$estado];
    }
}
