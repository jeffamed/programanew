<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factura;
use App\Detalle_Factura;
use App\CuentasxCobrar;
use App\Kardex;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use Mike42\Escpos\Printer; 
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class FacturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $factura = DB::table('Facturas as f')->join('Vendedores as v','f.idVendedor','=','v.idVendedor')->join('Clientes as c','f.idCliente','=','c.idCliente')
                        ->select('f.idFactura','v.Nombre as Vendedor','f.idCliente','f.NombreFactura as Cliente','c.LimiteCredito','c.PlazoPago','f.Estado',
                        'f.LimiteCredito as Credito','f.created_at','f.Total', 'f.TipoCompra')
                        ->orderBy('f.idFactura', 'desc')
                       ->paginate(10);
        }else{
            $factura =  DB::table('Facturas as f')->join('Vendedores as v','f.idVendedor','=','v.idVendedor')->join('Clientes as c','f.idCliente','=','c.idCliente')
                        ->select('f.idFactura','v.Nombre as Vendedor','f.idCliente','c.Nombre as Cliente','c.LimiteCredito','c.PlazoPago','f.Estado',
                        'f.LimiteCredito as Credito','f.created_at','f.Total', 'f.TipoCompra')
                        ->where($filtro,'like','%'. $buscar .'%')
                        ->orderBy('f.idFactura', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$factura->total(),
                'current_page'=>$factura->currentPage(),
                'per_page'=>$factura->perPage(),
                'last_page'=>$factura->lastPage(),
                'from'=>$factura->firstItem(),
                'to'=>$factura->lastItem(),
            ],
            'factura' =>$factura
        ];
    }

    public function DetalleFactura(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->idFactura;
        $detalle = DB::select('exec sp_listarDetalleFactura ?', [$id]);

        return ['detalle' => $detalle];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        if (!$request->ajax())  return redirect('/');
        try {

            DB::beginTransaction();
            $factura = new Factura();
            $factura->idVendedor = $request->idVendedor;
            $factura->idCliente = $request->idCliente;
            $factura->LimiteCredito = $request->creditoDisponible;
            $factura->TipoCompra = $request->TipoCompra;
            $factura->TasaDolar = $request->TasaDolar;
            $factura->TipoFactura = $request->TipoFactura;
            $factura->Total = $request->Total;
            $factura->TotalDolar = $request->TotalDolar;
            $factura->usuario =  \Auth::user()->name;
            $factura->NombreFactura = $request->nombreFact;
            $factura->NumFact = DB::RAW("NEXT VALUE FOR [dbo].[SecFact]");
            if($request->TipoCompra == 'Contado')
            {
                $factura->Estado = 'Pagado';
            }else{
                $factura->Estado = 'Facturado';
            }
            $factura->save();

            $detalles = $request->data;
            
            foreach ($detalles as $ep => $det) {
                $detalle = new Detalle_Factura();
                $detalle->idFactura = $factura->idFactura;
                $detalle->idInventario  = $det['idproducto'];
                $detalle->Cantidad = $det['cantidad'];
                $detalle->Precio = $det['precio'];
                $detalle->PrecioDolar = $det['preciodolar'];
                $detalle->PorcComision = $det['comision'];
                $detalle->TipoPrecio = $det['tipoP'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idproducto'];
                $kardex->NumDoc = $factura->idFactura;
                $kardex->Descripcion = 'Factura No.: '.$factura->idFactura;
                $kardex->TipoDoc = 'Factura';
                $kardex->Cantidad = $det['cantidad'];
                $kardex->Cliente = $request->Cliente;
                $kardex->Costo = $det['precio'];
                $kardex->save();
            }
            
            $cuentas = new CuentasxCobrar();
            $cuentas->idFactura = $factura->idFactura;
            $cuentas->TotalDeuda = $request->Total;
            $cuentas->Estado = 'Activo';
            if ($request->TipoCompra == 'Contado') {
                $cuentas->SaldoRestante = 0;
                $cuentas->TotalComision = 0;
                $cuentas->FechaVencimiento = Carbon::now();
                $cuentas->FechaRecuperacionV = Carbon::now();
                $cuentas->ManeraCanc = 'Contado';
                $cuentas->FechaCancelacion = Carbon::now();
            }else{
                $cuentas->SaldoRestante = $request->Total;
                $cuentas->TotalComision = $request->TotalComision;
                $cuentas->FechaVencimiento = Carbon::now()->addDay($request->Plazo);
                $cuentas->FechaRecuperacionV = Carbon::now()->addDay(62);
            }
            $cuentas->save();

            DB::commit();

            return ['id' => $factura->idFactura];
            
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function generarFactura($id, $nota)
    {
        $notas = $nota;

        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.factura', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total, "notas"=>$nota])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function generarFacturaMini($id, $nota)
    {
        $notas = $nota;
        
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);
     
        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.facturaMini', ['factura'=>$factura,'detalle'=>$detalle, "total"=>$total, "notas"=>$nota])->setPaper("letter","landscape");
        return $pdf->stream();
    }

    public function generarFacturaCinta($id, $nota)
    {
        /* 
        $fecha = Carbon::now();
        //$hoy = $fecha.format('d/m/Y');
        //$hora = $fecha.format('H:i:s');

        $cliente = DB::table('Facturas as f')->join('Clientes as c','f.idCliente','=','c.idCliente')->select('c.Nombre')->where('f.idFactura','=',$id)->pluck('c.Nombre')->first();
        $codigo = DB::table('Facturas as f')->join('Clientes as c','f.idCliente','=','c.idCliente')->select('c.Codigo')->where('f.idFactura','=',$id)->pluck('c.Codigo')->first();
        $tipo = DB::table('Facturas as f')->select('f.TipoCompra')->where('f.idFactura','=',$id)->pluck('f.TipoCompra')->first();
        $vendedor = DB::table('Facturas as f')->join('Vendedores as v','f.idVendedor','=','v.idVendedor')->select('v.Nombre')->where('f.idFactura','=',$id)->pluck('v.Nombre')->first();
        $tasa = DB::table('Facturas as f')->select('f.TasaDolar')->where('f.idFactura','=',$id)->pluck('f.TasaDolar')->first();
        $total = DB::table('Facturas as f')->select('f.Total')->where('f.idFactura','=',$id)->pluck('f.Total')->first();
        $detalle = DB ::table('Detalle_Facturas as df')->join('Inventario as i','df.idInventario','=','i.idInventario')->select('df.Nombre','df.Cod_Producto','df.Cantidad','df.Precio',DB::RAW('df.Cantidad * df.Precio as Total'))->where('df.idFactura','=',$id)->get();
        /* Quitar el ; de extension : intl del php.ini*/
        try {
            /*$connector = new WindowsPrintConnector("Epson TM-U220"); carpintero */
            //$profile = CapabilityProfile :: load ( "simple" );
            //$connector = new NetworkPrintConnector("\\192.168.3.155\EPSON TM-T20II Receipt");
            //$connector = new NetworkPrintConnector("\\192.168.2.100\EPSON%TM-U220%Receipt");
            /*$connector = new WindowsPrintConnector("smb://192.168.2.100/Epson TM-U220 Receipt");
            $printer = new Printer($connector);
            $printer -> text("El Carpintero\n");
            $printer -> text("Semafaro del Ministerio\n");
            $printer -> text("de Gobernacion 1c al N y 1/2 al O\n");
            $printer -> text("Cel: (505) 8551-2376\n");
            $printer -> text("ESTO ES UNA PRUEBA DESDE EL PREDIO\n");*/

                            
                ///$connector = new FilePrintConnector("/dev/usb/ESDPRT001");

                $connector = new NetworkPrintConnector("192.168.2.100",9100);
                $printer = new Printer($connector);

                /* Text */
                $printer -> text("Hello world\n");
                $printer -> cut();

                /* Line feeds */
                $printer -> text("ABC");
                $printer -> feed(7);
                $printer -> text("DEF");
                $printer -> feedReverse(3);
                $printer -> text("GHI");
                $printer -> feed();
                $printer -> cut();

           /* $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> text($id." Fecha: ".$hoy."\n");
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> text("Condición: ".$tipo." \t Hora: ".$hora."\n");
            $printer -> text("Codigo:".$codigo."\n");
            $printer -> text("Cliente: ".$cliente." - ".$nota." \n");
            $printer -> text("Vendedor: ".$vendedor." \n");
            $printer -> text("Caja: 1 \t T/C: ".$tasa." \n");
            $printer -> text("==========================================\n");
            $printer -> text("Descrip/Cod \t Cantidad \t P.Unit \t Total \n");
            $printer -> text("==========================================\n");
            foreach ($detalles as $d) {
                $printer -> text($d->Nombre."\n");
                $printer -> text($d->Cod_Producto."\t".$d->Cantidad."\t".$d->Precio."\t".$d->Total."\n");
            }
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> text("SubTotal C$: ".$total." \n");
            $printer -> text("Descuento C$: 0.00 \n");
            $printer -> text("Total Neto C$: ".$total." \n");
            $printer -> text("Pago C$: ".$total." \n");
            $printer -> text("Vuelto C$: 0.00 \n");
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> text("Managua \n");*/

            /*$printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> text("0000069188 Fecha: ".$hoy."\n");
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> text("Condición: Contado \t Hora: ".$hora."\n");
            $printer -> text("Codigo:".$id."\n");
            $printer -> text("Cliente: Fulanito Perez \n");
            $printer -> text("Vendedor: Casa Matriz \n");
            $printer -> text("Caja: 1 \t T/C: 34.10 \n");
            $printer -> text("==========================================\n");
            $printer -> text("Descrip/Cod \t Cantidad \t P.Unit \t Total \n");
            $printer -> text("==========================================\n");
            $printer -> text("6112-96 Haladera\n");
            $printer -> text("01-07-00147 \t 2.00 \t 25.00 \t 50.00 \n");
            $printer -> text("12MM*12MM(200MM) Haladera Liviana\n");
            $printer -> text("01-07-00074 \t 4.00 \t 29.00 \t 116.00 \n");
            $printer -> setJustification(Printer::JUSTIFY_RIGHT);
            $printer -> text("SubTotal C$: 166.00 \n");
            $printer -> text("Descuento C$: 0.00 \n");
            $printer -> text("Total Neto C$: 166.00\n");
            $printer -> text("Pago C$: 166.00 \n");
            $printer -> text("Vuelto C$: 0.00 \n");
            $printer -> setJustification(Printer::JUSTIFY_LEFT);
            $printer -> text("Managua \n");*/
            
            $printer->feed();

            $printer -> cut();

            $printer -> close();
            
        } catch(Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function generarFacturaDolar($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.facturadolar', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function generarFacturaMiniDolar($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);
     
        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.facturaminidolar', ['factura'=>$factura,'detalle'=>$detalle, "total"=>$total])->setPaper("letter","landscape");
        return $pdf->stream();
    }

    public function FacturasClienteD(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $facturas = DB::select('exec sp_DetalleClienteConDeuda');
        return['facturas'=>$facturas];
    }

    public function FacturasxCobrar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idCliente = $request->idCliente;
        $facturas = DB::select('exec sp_listarCxC ?',[$idCliente]);
        return['facturas'=>$facturas];
    }

    public function DetalleProductoClienteD(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idFactura = $request->id;
        $facturas = DB::select('exec sp_DetalleProductoClienteDeuda ?',[$idFactura]);
        return['detallefacturas'=>$facturas];
    }

    public function FacturasVencidas(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $vencida = DB::select('exec sp_TotalFactVencidas');
        return ['vencida'=>$vencida];
    }

    public function deshabilitar(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $factura = Factura::findOrFail($request->idFactura);
       $factura->MotivoAnulacion = $request->MotivoAnulacion;
       $factura->AutorizadoAnul = $request->Autorizado;
       $factura->Estado = 'Anulada';
       $factura->save();

        $detFact = Detalle_Factura::select('idInventario','Cantidad','Precio')->where('idFactura','=',$request->idFactura)->get();

        foreach($detFact as $det){
            $kardex = new Kardex();
            $kardex->idInventario = $det->idInventario;
            $kardex->NumDoc = $request->idFactura;
            $kardex->Descripcion = 'Anul. Fact No.: '.$request->idFactura;
            $kardex->TipoDoc = 'AnulFact';
            $kardex->Cantidad = $det->Cantidad;
            $kardex->Costo = $det->Precio;
            $kardex->save();
        }
    }

    public function ultimoRegistro(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = Factura::select('idFactura')->orderBy('idFactura','desc')->pluck('idFactura')->first();
        return ['id'=>$id];
    }

    public function pagina1($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page1', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function pagina2($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page2', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total ])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function pagina3($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page3', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total ])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function pagina4($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page4', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total ])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function pagina12($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page12', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total ])->setPaper("letter","landscape");
        return $pdf->stream();
    }

    public function pagina34($id)
    {
        $factura = DB::select('exec sp_listaDatosFactura ?', [$id]);

        $detalle = DB::select('exec sp_DetalleFactura ?', [$id]);

        $total  = DB::select('exec sp_TotalesFactura ?', [$id]);

        $pdf = \PDF::loadView('pdf.page34', ['factura'=>$factura,'detalle'=>$detalle,'total'=>$total ])->setPaper("letter","landscape");
        return $pdf->stream();
    }

}
