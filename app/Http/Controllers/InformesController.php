<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\DetalleFactExport;
use App\Exports\InfProdDepExport;

use Illuminate\Http\Request;
use PHPJasper\PHPJasper;

class InformesController extends Controller
{

    public function JasperReport()
    {
        $jdbc_dir = 'C:\Users\development\Desktop\ProgramaNew\vendor\geekcom\phpjasper-laravel\bin\jasperstarter\jdbc';
        $extension = 'html';
        $nombre = 'listaprod';
        $filename = $nombre.time();
        $output = base_path('public/reports/'.$filename);
        $input =  storage_path('app/public/relatorios/Listado_Producto.jrxml');
        //$input = base_path('vendor/geekcom/phpjasper-laravel/examples/hello_world.jrxml');  
        //$output = base_path('vendor/geekcom/phpjasper-laravel/examples');  
        $options = [ 
            'format' => ['xls'],
            'locale' => 'en',
            'params' => [],
            'db_connection' => [
                'driver' => 'generic',
                'username' => 'sa',
                'password' => 'System32',
                'host' => '192.168.0.15',
                'database' => 'BDMARNOR',
                'port' => '1440',
                'jdbc_driver' => 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
                'jdbc_url' => 'jdbc:sqlserver://192.168.0.15:1440;databaseName=BDMARNOR',
                'jdbc_dir' => $jdbc_dir,
            ]
        ];
        
        $jasper = new PHPJasper;

        $jasper->compile($input)->execute();

        //$x = $jasper->process(storage_path('app/public/relatorios/hello_world.jasper'),$output,$options)->execute();

        $jasper->process(storage_path('app/public/relatorios/Listado_Producto.jasper'),$output,$options)->execute();
        
        header('Content-Description: application/xls');
        header('Content-Type: application/xls');
        header('Content-Disposition:; filename='.$filename);
        readfile($output.'.xls');
        unlink($output.'.xls');
        //readfile($output.'/'.$filename);
        //unlink($output.'/'.$filename);
        flush();

        /*$file = $output.'.'.$extension;
        if(!file_exists($file)){
            abort(404);
        }
        if($extension == 'xls'){
            header('Content-Description: Archivo Excel');
            header('Content-Type: application/x-miexcel');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length:'.filesize($file));
            flush();
            readfile($file);
            unlink($file);
            die();
        }
        else if($extension == 'pdf'){
            return response()->file($file)->deleteFileAfterSend();
        }*/
        //PHPJasper::compile(storage_path('app/public').'/relatorios/Listado_Producto.jrxml')->execute();

        /*$stringPar = array();

        foreach($request->parametros as $par){
            foreach($par as $key => $value){
                $stringPar[$key] = $value;
            }
        }*/
        /*JasperPHP::process(
            //storage_path('app/public/relatorios/',$request->archivo),
            storage_path('app/public/relatorios/Listado_Producto.jasper'),
            $output,
            array($extension),
            array('Descripcion'=>'SINEVOL'),
            $this->getDatabaseConfig(),
            "es-NI"
        )->execute();*/

        /*JasperPHP::process(
           $input,
            $output,
            array($extension),
            array(), //array de parametros
            $this->getDatabaseConfig(),
            true,
            true,
            "es-NI"
        )->output();*/


    }

    public function FacturaContCred(Request $request)
    {
        $jdbc_dir =  base_path('vendor\geekcom\phpjasper-laravel\bin\jasperstarter\jdbc');
        $extension = 'html';
        $nombre = 'listaprod';
        $filename = $nombre.time();
        $output = base_path('public/reports/'.$filename);
        $input =  storage_path('app/public/relatorios/ListaFactura.jrxml'); 
        $options = [ 
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'inicio' => $request->FInicio,
                'final' => $request->FFin
            ],
            'db_connection' => [
                'driver' => 'generic',
                'username' => 'sa',
                'password' => 'System32',
                'host' => 'S3RV3R',
                'database' => 'BDMARNOR',
                'port' => '1440',
                'jdbc_driver' => 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
                'jdbc_url' => 'jdbc:sqlserver://S3RV3R:1440;databaseName=BDMARNOR',
                'jdbc_dir' => $jdbc_dir,
            ]
        ];
        
        $jasper = new PHPJasper;

        $jasper->compile($input)->execute();

        $jasper->process(storage_path('app/public/relatorios/ListaFactura.jasper'),$output,$options)->execute();
        
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename='.$filename);
        readfile($output.'.pdf');
        unlink($output.'.pdf');
        flush();
    }

    public function FacturaContado(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactContado ?, ?', [$inicio, $fin]);

        $total = DB::select('exec sp_TotalFactContado ?, ?', [$inicio, $fin]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factcontado',['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function FacturaCredito(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactCredito ?, ?', [$inicio, $fin]);

        $total = DB::select('exec sp_TotalFactCredito ?, ?', [$inicio, $fin]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factcredito', ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function FacturaCliente(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idCliente;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactCliente ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalFactCliente ?, ?, ?', [$inicio, $fin, $id]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factcliente', ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function FacturaVendedor(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idVendedor;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalFactVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factvendedor', ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function FacturaZona(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idZona;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactZona ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalFactZona ?, ?, ?', [$inicio, $fin, $id]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factzona', ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function FacturaAnulada(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_FactAnuladas ?, ?', [$inicio, $fin]);

        $total = DB::select('exec sp_TotalFactAnuladas ?, ?', [$inicio, $fin]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factanulada',  ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function ReciboVendedor(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idVendedor;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;


        $recibo = DB::select('exec sp_ReciboVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalReciboVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.recibovendedor', ['recibo'=>$recibo,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function CalendarioCobro(Request $request)
    {
        $fecha = $request->Fecha;
        $idVendedor = $request->idVendedor;
        $idZona = $request->idZona;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $calendario = DB::select('exec sp_CalendarioCobro ?, ?, ?', [$fecha, $idVendedor, $idZona]);

        $totalZona = DB::select('exec sp_TotalCalendarioZona ?, ?, ?', [$fecha, $idVendedor, $idZona]);

        $totalCliente = DB::select('exec sp_TotalCalendarioCliente ?, ?, ?', [$fecha, $idVendedor, $idZona]);

        $del = Carbon::parse($fecha)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.calendariocobro', ['calendario'=>$calendario, 'zona'=>$totalZona, 'cliente'=>$totalCliente,'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function ChequeVendedor(Request $request)
    {
        $id = $request->idVendedor;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $cheque = DB::select('exec sp_ChequeVendedor ?', [$id]);

        $total = DB::select('exec sp_TotalChequeVendedor ?', [$id]);

        $pdf = \PDF::loadView('pdf.chequevendedor', ['cheque'=>$cheque, 'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario])->setPaper("letter","portrait");

        return $pdf->stream();
    }

    public function ChequeCobrado(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idBanco;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario = \Auth::user()->name;

        $cheque = DB::select('exec sp_ChequeCobrado ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalChequeCobrado ?, ?, ?', [$id, $inicio, $fin]);

        $pdf = \PDF::loadView('pdf.chequecobrado', ['cheque'=>$cheque, 'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function ChequeAnulado(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idBanco;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $cheque = DB::select('exec sp_ChequeAnulado ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalChequeAnulado ?, ?, ?', [$id, $inicio, $fin]);

        $pdf = \PDF::loadView('pdf.chequeanulado', ['cheque'=>$cheque, 'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function ChequePendiente(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idBanco;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $cheque = DB::select('exec sp_ChequePendiente ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalChequePendiente ?, ?, ?', [$id, $inicio, $fin]);

        $pdf = \PDF::loadView('pdf.chequependiente', ['cheque'=>$cheque,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function VentaProducto(Request $request)
    {
        Carbon::setLocale(config('app.locale'));
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $ventas = DB::select('exec sp_VentasProdDet ?, ?', [$inicio, $fin]);

        $total = DB::select('exec sp_VentasProdTotal ?, ?', [$inicio, $fin]);

        $totalFinal = DB::select('exec sp_TotalVentasProd ?, ?',[$inicio, $fin]);
        
        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.ventasProducto', ['ventas'=>$ventas,'total'=>$total, 'final'=>$totalFinal,'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al     
        ])->setPaper("letter","landscape");
        return $pdf->stream();
    }

    public function ExcelVentas(Request $request)
    {   
        $inicio = $request->FInicio;
        $fin = $request->FFin;

        //return (new DetalleFactExport($inicio,$fin))->download('ventas.xlsx');
        return Excel::download(new DetalleFactExport($inicio, $fin),'ventas.xlsx');
    }

    public function DevolucionCliente(Request $request)
    {
        Carbon::setLocale(config('app.locale'));
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $ventas = DB::select('exec sp_DetalleDevolucCliente ?, ?', [$inicio, $fin]);

        $total = DB::select('exec sp_TotalDevolucCliente ?, ?', [$inicio, $fin]);

        
        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.devolucioncliente', ['ventas'=>$ventas,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al     
        ])->setPaper("letter","portrait");
        return $pdf->stream();
    }

    public function InfoProductoDepartamento(Request $request)
    {   
        $departamento = $request->departamento;
        return Excel::download(new InfProdDepExport($departamento),'productodepartamento.xlsx');
    }

    public function PrefacturaVendedor(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;
        $id = $request->idVendedor;
        $hoy = Carbon::now()->format('d-m-Y');
        $usuario =  \Auth::user()->name;

        $factura = DB::select('exec sp_PrefactVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $total = DB::select('exec sp_TotalPrefactVendedor ?, ?, ?', [$inicio, $fin, $id]);

        $del = Carbon::parse($inicio)->format('d-m-Y');
        $al = Carbon::parse($fin)->format('d-m-Y');

        $pdf = \PDF::loadView('pdf.factvendedor', ['factura'=>$factura,'total'=>$total, 'hoy'=>$hoy, 'user'=>$usuario, 'del'=>$del, 'al'=>$al])->setPaper("letter","portrait");
        return $pdf->stream();
    }
}
