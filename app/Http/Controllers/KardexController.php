<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use PHPJasper\PHPJasper;



class KardexController extends Controller
{
    public function CargarKardex(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->idProd;
        $inicio = $request->Inicio;
        $fin = $request->Fin;
        $filtro = $request->Filtro;
        if ($filtro == 'ninguno') {
            $kardex = DB::select('exec sp_KardexProdName ?, ?, ?', [$id, $inicio, $fin]);
        }else{
            $kardex = DB::select('exec sp_KardexProdFilter ?, ?, ?, ?', [$id, $inicio, $fin, $filtro]);
        }

        return ['kardex'=>$kardex];
    }

    public function PrintProduct(Request $request)
    {
        $jdbc_dir =  base_path('vendor\geekcom\phpjasper-laravel\bin\jasperstarter\jdbc');
        $extension = 'html';
        $nombre = 'kardexprod';
        $filename = $nombre.time();
        $output = base_path('public/reports/'.$filename);
        $input =  storage_path('app/public/relatorios/KardexProdEsp.jrxml'); 
        $options = [ 
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'idInventario' => (int)$request->idProd,
                'Usuario' => \Auth::user()->name,
                'FInicio' => $request->Inicio,
                'FFin' => $request->Fin,
            ],
            'db_connection' => [
                'driver' => 'generic',
                'username' => 'sa',
                'password' => 'System32',
                'host' => 'S3RV3R',
                'database' => 'BDMARNOR',
                'port' => '1440',
                'jdbc_driver' => 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
                'jdbc_url' => 'jdbc:sqlserver://S3RV3R:1440;databaseName=BDMARNOR',
                'jdbc_dir' => $jdbc_dir,
            ]
        ];
        
        $jasper = new PHPJasper;

        $jasper->compile($input)->execute();

        $jasper->process(storage_path('app/public/relatorios/KardexProdEsp.jasper'),$output,$options)->execute();
        
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename='.$filename);
        readfile($output.'.pdf');
        unlink($output.'.pdf');
        flush();
    }

    public function SummaryProduct(Request $request)
    {
        $jdbc_dir =  base_path('vendor\geekcom\phpjasper-laravel\bin\jasperstarter\jdbc');
        $extension = 'html';
        $nombre = 'kardexCompleto';
        $filename = $nombre.time();
        $output = base_path('public/reports/'.$filename);
        $input =  storage_path('app/public/relatorios/kardexComp.jrxml'); 
        $options = [ 
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'inicio' => $request->Inicio,
                'fin' => $request->Fin,
                'usuario' => \Auth::user()->name,
            ],
            'db_connection' => [
                'driver' => 'generic',
                'username' => 'sa',
                'password' => 'System32',
                'host' => 'S3RV3R',
                'database' => 'BDMARNOR',
                'port' => '1440',
                'jdbc_driver' => 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
                'jdbc_url' => 'jdbc:sqlserver://S3RV3R:1440;databaseName=BDMARNOR',
                'jdbc_dir' => $jdbc_dir,
            ]
        ];
        
        $jasper = new PHPJasper;

        $jasper->compile($input)->execute();

        $jasper->process(storage_path('app/public/relatorios/kardexComp.jasper'),$output,$options)->execute();
        
        header('Content-Description: application/pdf');
        header('Content-Type: application/pdf');
        header('Content-Disposition:; filename='.$filename);
        readfile($output.'.pdf');
        unlink($output.'.pdf');
        flush();
    }
}
