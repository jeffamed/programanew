<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use DB;
class MailDemoController extends Controller
{
    public function store(Request $request)
    {
        $cont = DB::table('Inventario as p')->select(DB::raw('count(p.idInventario) as Total'))->get()->pluck('Total');

        //$cont=['Total'=>'1256'];

        Mail::to('jeffamed@gmail.com')->send(new DemoEmail($cont));

        return 'Mensaje Enviado';
    }
}
