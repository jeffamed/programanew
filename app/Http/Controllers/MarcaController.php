<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marca;
use Illuminate\Support\Facades\DB;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        //$marca  = Marca::all();
        $buscar = $request->buscar;

        if($buscar == ''){
            $marca = Marca::orderBy('nombre', 'asc')->paginate(10);
        }else{
            $marca = Marca::where('nombre','like','%'. $buscar .'%')->orderBy('nombre', 'asc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$marca->total(),
                'current_page'=>$marca->currentPage(),
                'per_page'=>$marca->perPage(),
                'last_page'=>$marca->lastPage(),
                'from'=>$marca->firstItem(),
                'to'=>$marca->lastItem(),
            ],
            'marca' => $marca
        ];
        //return $marca;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $marca = new Marca();
        $marca->nombre = $request->nombre;
        $marca->Estado = 'Activo';
        $marca->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $marca = Marca::findOrFail($request->idMarca);
        $marca->nombre = $request->nombre;
        $marca->Estado = 'Activo';
        $marca->save();
    }

    public function SeleccionarMarca(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $marca = DB::select('exec sp_listarMarca');

        return ['marca'=>$marca];
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $marca = Marca::findOrFail($request->idMarca);
        $marca->Estado = 'Inactivo';
        $marca->save();
    }

    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $marca = Marca::findOrFail($request->idMarca);
        $marca->Estado = 'Activo';
        $marca->save();
    }

}
