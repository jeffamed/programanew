<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotaDebito;
use Illuminate\Support\Facades\DB;

class NotaDebitoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        
        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $notas = DB::table('NotaDebito as nd')->join('Clientes as c','nd.idCliente', '=', 'c.idCliente')->join('Pagos_CxC as pcc', 'nd.idRecibo', '=', 'pcc.idPagoCxC')->join('Talonarios as t', 'pcc.idTalonario', '=', 't.idTalonario')
                        ->select('nd.idNota', 'c.Nombre as Cliente', 't.NumeracionInicial as Recibo', 'nd.Monto', DB::RAW('convert(varchar,nd.created_at,103) as FechaRegistro'), 'nd.Estado','nd.idFactAplic') 
                        ->orderBy('nd.idNota', 'desc')
                       ->paginate(10);
        }else{
            $notas = DB::table('NotaDebito as nd')->join('Clientes as c','nd.idCliente', '=', 'c.idCliente')->join('Pagos_CxC as pcc', 'nd.idRecibo', '=', 'pcc.idPagoCxC')->join('Talonarios as t', 'pcc.idTalonario', '=', 't.idTalonario')
                        ->select('nd.idNota', 'c.Nombre as Cliente', 't.NumeracionInicial as Recibo', 'nd.Monto', DB::RAW('convert(varchar,nd.created_at,103) as FechaRegistro'), 'nd.Estado','nd.idFactAplic') 
                        ->where($filtro,'like','%'.$buscar.'%')
                        ->orderBy('nd.idNota', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$notas->total(),
                'current_page'=>$notas->currentPage(),
                'per_page'=>$notas->perPage(),
                'last_page'=>$notas->lastPage(),
                'from'=>$notas->firstItem(),
                'to'=>$notas->lastItem(),
            ],
            'notas' =>$notas
        ];
    }

    public function ConfirmarActualizacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $contraseña = $request->contraseña;
        $verificar = DB::table('Autorizacion')->select('Nombre')->where('contraseña','=',$contraseña)->get();
        return ['verificar'=>$verificar];
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $nota = NotaDebito::findOrFail($request->idNota);
        $nota->Monto = $request->Monto;
        $nota->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function aplicar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $nota = NotaDebito::findOrFail($request->idNota);
        $nota->idFactAplic = $request->idFactura;
        $nota->Estado = 'Aplicado';
        $nota->save();
    }

    public function DatosNotaD(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idNota = $request->id;
        $nota=DB::select('exec sp_CargarNotasD ?', [$idNota]);

        return ['nota'=>$nota];
    }

}
