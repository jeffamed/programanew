<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotasCredito;
use Illuminate\Support\Facades\DB;

class NotasCreditoController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        
        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $notas = DB::table('NotasCreditos as nc')->join('Clientes as c','nc.idCliente','=','c.idCliente')
                        ->select('nc.idNota', 'nc.idCliente', 'c.Nombre as Cliente', 'nc.idFactura', 'nc.Monto', 'nc.TipoNota', DB::RAW('convert(varchar,nc.created_at,103) as FechaRegistro')) 
                        ->orderBy('nc.idNota', 'desc')
                       ->paginate(10);
        }else{
            $notas =  DB::table('NotasCreditos as nc')->join('Clientes as c','nc.idCliente','=','c.idCliente')
                            ->select('nc.idNota', 'nc.idCliente', 'c.Nombre as Cliente', 'nc.idFactura', 'nc.Monto', 'nc.TipoNota', DB::RAW('convert(varchar,nc.created_at,103) as FechaRegistro'))
                            ->where($filtro,'like','%'. $buscar .'%')
                            ->orderBy('nc.idNota', 'desc')
                            ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$notas->total(),
                'current_page'=>$notas->currentPage(),
                'per_page'=>$notas->perPage(),
                'last_page'=>$notas->lastPage(),
                'from'=>$notas->firstItem(),
                'to'=>$notas->lastItem(),
            ],
            'notas' =>$notas
        ];
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        $nota = new NotasCredito();
        $nota->idCliente = $request->idCliente;
        $nota->idFactura = $request->idFactura;
        $nota->Monto = $request->Monto;
        $nota->TipoNota = $request->Tipo;
        $nota->TasaCambio = $request->Tasa;
        $nota->Observacion = $request->Observacion;
        $nota->save();
    }

    /**
     * Store a newly update resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        $nota = NotasCredito::findOrFail($request->idNota);
        $nota->idCliente = $request->idCliente;
        $nota->idFactura = $request->idFactura;
        $nota->Monto = $request->Monto;
        $nota->TipoNota = $request->Tipo;
        $nota->TasaCambio = $request->Tasa;
        $nota->Observacion = $request->Observacion;
        $nota->save();
    }

    public function ConfirmarActualizacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $contraseña = $request->contraseña;
        $verificar = DB::table('Autorizacion')->select('Nombre')->where('contraseña','=',$contraseña)->get();
        return ['verificar'=>$verificar];
    }

    public function DatosNotaC(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idNota = $request->id;
        $nota=DB::select('exec sp_CargarNotasC ?', [$idNota]);

        return ['nota'=>$nota];
    }

    public function SaldoRestante(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idCliente = $request->idCliente;
        $idFactura = $request->idFactura;
        $facturas = DB::select('exec sp_SaldoRestNotaC ?, ?',[$idCliente, $idFactura]);
        return['facturas'=>$facturas];
    }
}
