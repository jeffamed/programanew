<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PagosCxC;
use App\DetallaPagosCxC;
use App\NotaDebito;
//use App\Estado;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PagosCxCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        
        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $pagos = DB::table('Pagos_CxC as pcc')->join('Clientes as c', 'pcc.idCliente', '=', 'c.idCliente')->join('Talonarios as t','pcc.idTalonario','=','t.idtalonario')
                        ->select('pcc.idPagoCxC as idPago','c.Nombre as Cliente',DB::RAW('convert(varchar,CAST(pcc.Abono as money),1) as Abono') , 't.NumeracionInicial','pcc.created_at','pcc.Estado', DB::RAW('convert(varchar,pcc.FechaRegistro,103) as FechaRegistro'))
                        ->orderBy('pcc.idPagoCxC', 'desc')
                        ->paginate(10);
        }else{
            $pagos =  DB::table('Pagos_CxC as pcc')->join('Clientes as c', 'pcc.idCliente', '=', 'c.idCliente')->join('Talonarios as t','pcc.idTalonario','=','t.idtalonario')
                        ->select('pcc.idPagoCxC as idPago','c.Nombre as Cliente',DB::RAW('convert(varchar,CAST(pcc.Abono as money),1) as Abono') , 't.NumeracionInicial','pcc.created_at','pcc.Estado',DB::RAW('convert(varchar,pcc.FechaRegistro,103) as FechaRegistro'))
                        ->where($filtro,'like','%'. $buscar .'%')
                        ->orderBy('pcc.idPagoCxC', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$pagos->total(),
                'current_page'=>$pagos->currentPage(),
                'per_page'=>$pagos->perPage(),
                'last_page'=>$pagos->lastPage(),
                'from'=>$pagos->firstItem(),
                'to'=>$pagos->lastItem(),
            ],
            'pagos' =>$pagos
        ];
        
    }

    public function lastRef(Request $request)
    {
        $id = DB::table('Pagos_CxC')->select('idPagoCxC')->orderBy('idPagoCxC','desc')->first();
        return ['numeroref'=>$id];
    }

    public function fechadia(Request $request)
    {
        $hoy= Carbon::now()->toDateString();
        return ['fecha'=>$hoy];
    }

    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();

            $pagos = new PagosCxC();
            $pagos->idTalonario = $request->idTalonario;
            $pagos->Abono = $request->Abono;
            $pagos->idCliente = $request->idCliente;
            $pagos->Observacion = $request->Observacion;
            $pagos->usuario =  \Auth::user()->name;
            $pagos->FechaRegistro = $request->FRegistro;
            $pagos->Estado = 'Aplicado';
            $pagos->save();

            $detalle = $request->data;

            foreach ($detalle as $key => $det) {
                $detallep = new DetallaPagosCxC();
                $detallep->idPagoCxC = $pagos->idPagoCxC;
                $detallep->idCxC = $det['idCuentasxCobrar'];
                $detallep->Monto = $det['Abono'];
                if ($det['Descuento'] == '') {
                    $detallep->Descuento = 0;
                }else{
                    $detallep->Descuento = $det['Descuento'];
                }
                $detallep->FechaRecibo = $request->FRegistro;
                $detallep->save();
            }

            if($request->NotaDebito > 0){
                $nota = new NotaDebito();
                $nota->idRecibo = $pagos->idPagoCxC;
                $nota->idCliente = $request->idCliente;
                $nota->Monto = $request->NotaDebito;
                $nota->Estado = 'Sin Aplicar';
                $nota->save();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            DB::rollback();
        }

    }

    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $pagos = PagosCxC::findOrFail($request->idPago);
        $pagos->idTalonario = $request->idTalonario;
        $pagos->Abono = $request->Abono;
        $pagos->idCliente = $request->idCliente;
        $pagos->Observacion = $request->Observacion;
        $pagos->Estado = 'Aplicado';
        $pagos->FechaRegistro = $request->FRegistro;
        $pagos->save();

        $detalle = $request->data;

        foreach ($detalle as $key => $det) {
            $detallep = DetallaPagosCxC::where([['idPagoCxC','=',$request->idPago],['idCxC','=',$det['idCuentasxCobrar']]])->firstOrFail();
            $detallep->Monto = $det['Monto'];
            $detallep->Descuento = $det['Descuento'];
            $detallep->FechaRecibo = $request->FRegistro;
            $detallep->save();
        }

        $nota = NotaDebito::where('idRecibo','=', $request->idPago)->first();
        if ($nota!=null) {
            $nota->Monto = $request->Abono - $request->Total;
            $nota->save();
        }
    }

/*    public function ConfirmarAnulacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $contraseña = $request->contraseña;
        $verificar = DB::table('Autorizacion')->select('Nombre')->where('contraseña','=',$contraseña)->get();
        return ['verificar'=>$verificar];
    }*/

    public function ConfirmarAnulacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $contraseña = $request->contraseña;
        $verificar = DB::table('users')->select('name')->where([['pin','=',$contraseña],['state','=','Activo']])->get();
        return ['verificar'=>$verificar];
        
    }

    public function DatosRecibo(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $idRecibo = $request->id;
        $recibo=DB::select('exec sp_DatosRecibo ?', [$idRecibo]);

        return ['recibo'=>$recibo];
    }

    public function DatosRecibo2(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        
        $idRecibo = $request->id;
        $recibo=DB::select('exec sp_DatosRecibo2 ?', [$idRecibo]);

        return ['recibo'=>$recibo];
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $pagos = PagosCxC::findOrFail($request->idPago);
        $pagos->MotivoAnulacion = $request->MotivoAnulacion;
        $pagos->Estado = 'Anulada';
        $pagos->save();
    }
}
