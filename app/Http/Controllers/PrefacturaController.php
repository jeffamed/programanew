<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prefactura;
use App\DetallePrefactura;
use App\Factura;
use App\Detalle_Factura;
use App\Kardex;
use App\CuentasxCobrar;
use App\Productos;
use Carbon\Carbon;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DetallePrefExport;

class PrefacturaController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $prefactura = DB::table('Prefacturas as c')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')->join('Clientes as cl','c.idCliente','=','cl.idCliente')
                        ->select('c.idPrefactura','v.Nombre as Vendedor', 'v.idVendedor','cl.Nombre as Cliente', 'cl.idCliente','c.Total', 'c.created_at', 'c.TipoCompra', 'c.TotalComisiones','c.Estado') 
                        ->orderBy('c.idPrefactura', 'desc')
                       ->paginate(10);
        }else{
            $prefactura = DB::table('Prefacturas as c')->join('Vendedores as v','c.idVendedor','=','v.idVendedor')->join('Clientes as cl','c.idCliente','=','cl.idCliente')
                        ->select('c.idPrefactura','v.Nombre as Vendedor', 'cl.Nombre as Cliente', 'c.Total', 'c.created_at', 'c.TipoCompra', 'c.TotalComisiones','c.Estado') 
                        ->where($filtro,'like','%'. $buscar .'%')
                        ->orderBy('c.idPrefactura', 'desc')
                        ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$prefactura->total(),
                'current_page'=>$prefactura->currentPage(),
                'per_page'=>$prefactura->perPage(),
                'last_page'=>$prefactura->lastPage(),
                'from'=>$prefactura->firstItem(),
                'to'=>$prefactura->lastItem(),
            ],
            'prefactura' =>$prefactura
        ];
    }

    public function invoce(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            
            DB::beginTransaction();
            $factura = new Factura();
            $factura->idVendedor = $request->idVendedor;
            $factura->idCliente = $request->idCliente;
            $factura->LimiteCredito = $request->creditoDisponible;
            $factura->TipoCompra = $request->TipoCompra;
            $factura->TasaDolar = $request->TasaDolar;
            $factura->TipoFactura = $request->TipoFactura;
            $factura->Total = $request->Total;
            //$factura->TotalDolar = $request->TotalDolar;
            $factura->usuario =  $request->usuario;
            $factura->NombreFactura = $request->nombreFact;
            if($request->TipoCompra == 'Contado')
            {
                $factura->Estado = 'Pagado';
            }else{
                $factura->Estado = 'Facturado';
            }
            $factura->NumFact=DB::raw("NEXT VALUE FOR [dbo].[SecFact]");
            $factura->save();

            $detalles = $request->data;
            
            foreach ($detalles as $ep => $det) {
                $detalle = new Detalle_Factura();
                $detalle->idFactura = $factura->idFactura;
                $detalle->idInventario  = $det['idInventario'];
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->PrecioDolar = 0;
                $detalle->PorcComision = $det['PorcComision'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idInventario'];
                $kardex->NumDoc = $request->idPrefactura;
                $kardex->Descripcion = 'Retorno Prefac No.: '.$request->idPrefactura;
                $kardex->TipoDoc = 'RetornoPref';
                $kardex->Cantidad = $det['Cantidad'];
                $kardex->Cliente = $request->Cliente;
                $kardex->Costo = $det['Precio'];
                $kardex->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idInventario'];
                $kardex->NumDoc = $factura->idFactura;
                $kardex->Descripcion = 'Factura No.: '.$factura->idFactura;
                $kardex->TipoDoc = 'Factura';
                $kardex->Cantidad = $det['Cantidad'];
                $kardex->Cliente = $request->Cliente;
                $kardex->Costo = $det['Precio'];
                $kardex->save();
            }
            
            $cuentas = new CuentasxCobrar();
            $cuentas->idFactura = $factura->idFactura;
            $cuentas->TotalDeuda = $request->Total;
            $cuentas->Estado = 'Activo';
            if ($request->TipoCompra == 'Contado') {
                $cuentas->SaldoRestante = 0;
                $cuentas->TotalComision = 0;
                $cuentas->FechaVencimiento = Carbon::now();
                $cuentas->FechaRecuperacionV = Carbon::now();
                $cuentas->ManeraCanc = 'Contado';
                $cuentas->FechaCancelacion = Carbon::now();
            }else{
                $cuentas->SaldoRestante = $request->Total;
                $cuentas->TotalComision = $request->TotalComision;
                $cuentas->FechaVencimiento = Carbon::now()->addDay($request->Plazo);
                $cuentas->FechaRecuperacionV = Carbon::now()->addDay(62);
            }
            $cuentas->save();

            DB::commit();

            return ['id' => $factura->idFactura];
            
        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
     if (!$request->ajax())  return redirect('/');

        $prefactura = Prefactura::findOrFail($request->idPrefactura);
        $prefactura->idCliente = $request->idCliente;
        $prefactura->idVendedor = $request->idVendedor;
        $prefactura->TipoFactura = $request->TipoFactura;
        $prefactura->TipoCompra = $request->TipoCompra;
        $prefactura->Estado = 'Habilitado';
        $prefactura->Total = $request->Total;
        if ($request->TipoCompra == 'Contado') {
            $prefactura->TotalComisiones = 0;
        }else{
            $prefactura->TotalComisiones = $request->TotalComision;
        }
        $prefactura->save();

        $detalles = $request->data;
        $eliminar = $request->delete;
        
        foreach ($detalles as $ep => $det) {
            $detalle = DetallePrefactura::where([['idPrefactura','=',$request->idPrefactura],['idInventario','=',$det['idInventario']]])->first();
            if ($detalle == Null) {
                $detalle = new DetallePrefactura();
                $detalle->idPrefactura = $request->idPrefactura;
                $detalle->idInventario = $det['idInventario'];
                $detalle->Precio = $det['Precio'];
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->PorcComision = $det['PorcComision'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idInventario'];
                $kardex->NumDoc = $request->idPrefactura;
                $kardex->Descripcion = 'Prefactura No.: '.$request->idPrefactura;
                $kardex->TipoDoc = 'Prefactura';
                $kardex->Cantidad = $det['Cantidad'];
                $kardex->Cliente = $request->NombreSolicitante;
                $kardex->Costo = $det['Precio'];
                $kardex->save();

            }else{
                $detalle->Cantidad = $det['Cantidad'];
                $detalle->Precio = $det['Precio'];
                $detalle->save();

                /*$kardex = Kardex::where([['NumDoc','=',$request->idPrefactura],['TipoDoc','=','Prefactura'],['idInventario','=',$det['idInventario']]])->first();
                $kardex->Cantidad = $det['Cantidad'];
                $kardex->Costo = $det['Precio'];
                $kardex->save();*/
                
                $inventario = Productos::where('idInventario','=',$det['idInventario'])->first();
                $inventario->Stock = $inventario->Stock - intval($det['Cantidad']);
                $inventario->Stock = $inventario->Stock + intval($det['CantInicial']);
                $inventario->save();
            }
        }

        if(count($eliminar)>0){
            foreach ($eliminar as $dl => $elim) {
                $inventario = Productos::where('idInventario','=',$elim['idInventario'])->first();
                $inventario->Stock = $inventario->Stock + intval($elim['Cantidad']);
                $inventario->save();
                $detalle = DetallePrefactura::where([['idPrefactura','=',$request->idPrefactura],['idInventario','=',$elim['idInventario']]])->first();
                $detalle->delete();

                $kardex = Kardex::where([['NumDoc','=',$request->idPrefactura],['TipoDoc','=','Prefactura'],['idInventario','=',$elim['idInventario']]])->first();
                $kardex->Cantidad = 0;
                $kardex->save();
            }
        }
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $entrada = Prefactura::findOrFail($request->idPrefactura);
        $entrada->Estado = 'Anulado';
        $entrada->save();
    }
    public function datos(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->idPrefactura;
        $datos = DB::select('exec sp_CargarDatosPrefactura ?', [$id]);
        return ['datos'=>$datos];
    }
    public function detalle(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = $request->idPrefactura;
        $detalle = DB::select('exec sp_CargarDetallePrefactura ?', [$id]);
        return ['detalle'=>$detalle];
    }

    public function Estado(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $prefact = Prefactura::findOrFail($request->idPrefactura);
       $prefact->Estado = "Facturado";
       $prefact->NumFactura = $request->idFact;
       $prefact->save();
    }

    public function ProdPrefactura(Request $request)
    {
        $inicio = $request->FInicio;
        $fin = $request->FFin;

        return Excel::download(new DetallePrefExport($inicio, $fin),'prefacturas.xlsx');
    }

    public function ConsultaInventario(Request $request)
    {
        $id = $request->idInventario;
        $total = DB::table('DetallePrefacturas as dp')->join('Prefacturas as p','dp.idPrefactura','=','p.idPrefactura')
                ->select(DB::RAW('ISNULL(sum(dp.Cantidad),0) as TotalCantidad'))
                ->where([['p.Estado','=','Habilitado'],['dp.idInventario','=',$id]])->pluck('TotalCantidad')->first();

        return ['total'=>$total];

    }
}
