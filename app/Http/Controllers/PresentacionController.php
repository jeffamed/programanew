<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Presentacion;
use Illuminate\Support\Facades\DB;

class PresentacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $presentacion = Presentacion::orderBy('nombre','asc')->paginate(10);
        } else {
            $presentacion = Presentacion::where('nombre','like','%'.$buscar.'%')->orderBy('nombre','asc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$presentacion->total(),
                'current_page'=>$presentacion->currentPage(),
                'per_page'=>$presentacion->perPage(),
                'last_page'=>$presentacion->lastPage(),
                'from'=>$presentacion->firstItem(),
                'to'=>$presentacion->lastItem(),
            ], 
            'presentacion' => $presentacion
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $presentacion =  new Presentacion();
        $presentacion->Nombre = $request->Nombre;
        $presentacion->Descripcion = $request->Descripcion;
        $presentacion->save();
    }

    public function SeleccionarPresentacion(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $presentacion = DB::select('exec sp_listarPresentacion');

        return ['presentacion'=>$presentacion];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $presentacion = Presentacion::findOrFail($request->idPresentacion);
        $presentacion->Nombre = $request->Nombre;
        $presentacion->Descripcion = $request->Descripcion;
        $presentacion->save();
    }

}
