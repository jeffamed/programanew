<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductoVencidos;

class ProductoVencidosController extends Controller
{
    public function store(Request $request)
    {
        $producto = new ProductoVencidos();
        $producto->idInventario = $request->id;
        $producto->Nombre = $request->Nombre;
        $producto->FechaVenc = $request->Fecha;
        $producto->save();
    }
}
