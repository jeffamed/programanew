<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use App\Productos;
use App\InventarioInfo;
use App\Kardex;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use App\Exports\ProductoExport;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $buscar2 = $request->buscar2;
        $filtro = $request->filtro;

       if ($buscar == '') {
            $productos = DB::table('Inventario as i')->join('Departamentos as d', 'i.idDepartamento', '=', 'd.idDepartamentos')->join('Marcas as m','i.idMarca', '=', 'm.idMarca')
                            ->join('Clase_Producto as cp','i.idClase', '=', 'cp.idClase')->join('Presentacion as p','i.idPresentacion', '=', 'p.idPresentacion')
                            ->join('Unidad_Medida as um','i.idUndMedida', '=', 'um.idUnidadMedida')->join('Proveedores as pv','i.idProveedor', '=', 'pv.idProveedor')->leftjoin('InventarioInfoAdic as ii','i.idInventario','=','ii.idInventario')
                            ->select('i.idInventario','d.idDepartamentos','d.Nombre as Departamento' ,'m.idMarca', 'm.Nombre as Marca', 'cp.idClase','cp.Nombre as Clase','p.idPresentacion','p.Nombre as Presentacion','um.idUnidadMedida','um.Nombre as Unidad','pv.idProveedor','pv.Nombre as Proveedor',
                            'i.Cod_Producto','i.Nombre as Inventario', 'i.Stock', 'i.Descripcion', 'i.PorcComision', 'i.Unidad_de_Paquete','i.Precio1','i.Precio2','i.Precio3','i.Precio4','i.Precio5','i.PrecioDolar1','i.PrecioDolar2','i.PrecioDolar3','i.PrecioDolar4',
                            'i.PrecioDolar5','i.CantidadMax1','i.CantidadMax2','i.CantidadMax3','i.CantidadMax4','i.CantidadMax5','i.CantidadMin1','i.CantidadMin2','i.CantidadMin3','i.CantidadMin4','i.CantidadMin5','i.Estado','i.Cod_Barra','i.Imagen','i.NombreOriginal',
                            'ii.info1','ii.info2','ii.info3','ii.info4','ii.info5')
                            ->orderBy('i.Nombre', 'asc')->paginate(10);
        } else {
            $productos =  DB::table('Inventario as i')->join('Departamentos as d', 'i.idDepartamento', '=', 'd.idDepartamentos')->join('Marcas as m','i.idMarca', '=', 'm.idMarca')
                            ->join('Clase_Producto as cp','i.idClase', '=', 'cp.idClase')->join('Presentacion as p','i.idPresentacion', '=', 'p.idPresentacion')
                            ->join('Unidad_Medida as um','i.idUndMedida', '=', 'um.idUnidadMedida')->join('Proveedores as pv','i.idProveedor', '=', 'pv.idProveedor')->leftjoin('InventarioInfoAdic as ii','i.idInventario','=','ii.idInventario')
                            ->select('i.idInventario','d.idDepartamentos','d.Nombre as Departamento' ,'m.idMarca', 'm.Nombre as Marca', 'cp.idClase','cp.Nombre as Clase','p.idPresentacion','p.Nombre as Presentacion','um.idUnidadMedida','um.Nombre as Unidad','pv.idProveedor','pv.Nombre as Proveedor',
                            'i.Cod_Producto','i.Nombre as Inventario', 'i.Stock', 'i.Descripcion', 'i.PorcComision', 'i.Unidad_de_Paquete','i.Precio1','i.Precio2','i.Precio3','i.Precio4','i.Precio5','i.PrecioDolar1','i.PrecioDolar2','i.PrecioDolar3','i.PrecioDolar4',
                            'i.PrecioDolar5','i.CantidadMax1','i.CantidadMax2','i.CantidadMax3','i.CantidadMax4','i.CantidadMax5','i.CantidadMin1','i.CantidadMin2','i.CantidadMin3','i.CantidadMin4','i.CantidadMin5','i.Estado','i.Cod_Barra','i.Imagen','i.NombreOriginal',
                            'ii.info1','ii.info2','ii.info3','ii.info4','ii.info5')
                            ->where([['i.'.$filtro,'like','%'.$buscar.'%'],['i.'.$filtro,'like','%'.$buscar2.'%']])
                            ->orderBy('i.Nombre', 'asc')->paginate(10);
        }


        return [
            'pagination'=>[
                'total'=>$productos->total(),
                'current_page'=>$productos->currentPage(),
                'per_page'=>$productos->perPage(),
                'last_page'=>$productos->lastPage(),
                'from'=>$productos->firstItem(),
                'to'=>$productos->lastItem(),
            ], 
            'productos' => $productos
        ];
        
    }

    public function SeleccionarProducto(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $search = $request->buscar;
        $producto = DB::table('Inventario as i')->leftjoin('InventarioInfoAdic as ii', 'i.idInventario', '=', 'ii.idInventario')->join('Unidad_Medida as um', 'i.idUndMedida', '=', 'um.idUnidadMedida')->join('Presentacion as p','i.idPresentacion','=','p.idPresentacion')
        ->select('i.idInventario','i.Cod_Producto','i.Nombre as Productos',DB::RAW("concat(i.Cod_Producto,' -- ',i.Nombre) as Nombre"), 'i.Stock', 'i.PorcComision', 'i.Unidad_de_Paquete', DB::RAW('convert(varchar,CAST(i.Precio1 as money),1) as Precio11'), 'i.Precio1','i.Precio2', 'i.Precio3', 'i.Precio4', 'i.Precio5', 'i.PrecioDolar1', 'i.PrecioDolar2', 
        'i.PrecioDolar3', 'i.PrecioDolar4', 'i.PrecioDolar5', 'i.Imagen', 'ii.info1', 'ii.info2', 'ii.info3', 'ii.info4', 'ii.info5', 'um.Nombre as Unidad', 'i.Bonificacion', 'i.CantidadMax1', 'i.CantidadMin1', 'i.CantidadMax1',
        'i.CantidadMin2','i.CantidadMax2', 'i.CantidadMin3', 'i.CantidadMax3', 'i.CantidadMin4', 'i.CantidadMax4', 'i.CantidadMin5', 'i.CantidadMax5', 'p.Nombre as Presentacion','p.Descripcion')
        ->where([
            ['i.Nombre','like','%'.$search.'%'],
            ['i.Estado', '=', 'Activo']
            ])
        ->orWhere([
            ['i.Cod_Producto','=',$search],
            ['i.Estado', '=', 'Activo']
            ])
        ->orderBy('i.Stock','desc')->take(50)->get();

        return['producto'=>$producto];
    }

    public function SeleccionarServicio(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $search = $request->buscar;
        $producto = DB::table('Inventario as i')->leftjoin('InventarioInfoAdic as ii', 'i.idInventario', '=', 'ii.idInventario')->join('Unidad_Medida as um', 'i.idUndMedida', '=', 'um.idUnidadMedida')->join('Presentacion as p','i.idPresentacion','=','p.idPresentacion')
        ->select('i.idInventario','i.Cod_Producto',"i.Nombre as Producto", DB::RAW("concat(i.Cod_Producto,' -- ',i.Nombre) as Nombre"), 'i.Stock', 'i.PorcComision', 'i.Unidad_de_Paquete', 'i.Precio1', 'i.Precio2', 'i.Precio3', 'i.Precio4', 'i.Precio5', 'i.PrecioDolar1', 'i.PrecioDolar2', 
        'i.PrecioDolar3', 'i.PrecioDolar4', 'i.PrecioDolar5', 'i.Imagen', 'ii.info1', 'ii.info2', 'ii.info3', 'ii.info4', 'ii.info5', 'um.Nombre as Unidad', 'i.Bonificacion', 'i.CantidadMax1', 'i.CantidadMin1', 'i.CantidadMax1',
        'i.CantidadMin2','i.CantidadMax2', 'i.CantidadMin3', 'i.CantidadMax3', 'i.CantidadMin4', 'i.CantidadMax4', 'i.CantidadMin5', 'i.CantidadMax5', 'p.Nombre as Presentacion','i.Servicio')
        ->where([
            ['i.Nombre','like','%'.$search.'%'],
            ['i.Estado', '=', 'Activo'],
            ['i.Servicio','=','2']
            ])
        ->orWhere([
            ['i.Cod_Producto','like','%'.$search.'%'],
            ['i.Estado', '=', 'Activo'],
            ['i.Servicio','=','2']
            ])
        ->orderBy('i.Nombre')->take(50)->get();

        return['producto'=>$producto];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();
            if (!$request->ajax())  return redirect('/');
            $productos = new Productos();
            $productos->idDepartamento = $request->idDepartamento;
            $productos->idMarca = $request->idMarca;
            $productos->idClase = $request->idClase;
            $productos->idPresentacion = $request->idPresentacion;
            $productos->idUndMedida = $request->idUndMedida;
            $productos->idProveedor = $request->idProveedor;
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Stock = $request->Stock;
            $productos->Descripcion = $request->Descripcion;
            $productos->PorcComision = $request->PorcComision;
            $productos->Unidad_de_Paquete = $request->Unidad_Paquete;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->CantidadMin1 = $request->CantidadMin1;
            $productos->CantidadMin2 = $request->CantidadMin2;
            $productos->CantidadMin3 = $request->CantidadMin3;
            $productos->CantidadMin4 = $request->CantidadMin4;
            $productos->CantidadMin5 = $request->CantidadMin5;
            $productos->CantidadMax1 = $request->CantidadMax1;
            $productos->CantidadMax2 = $request->CantidadMax2;
            $productos->CantidadMax3 = $request->CantidadMax3;
            $productos->CantidadMax4 = $request->CantidadMax4;
            $productos->CantidadMax5 = $request->CantidadMax5;
            $productos->NombreOriginal = $request->NombreOriginal;
            $productos->Cod_Barra =  $request->Cod_Barra;
            $productos->Estado = 'Activo';
            $productos->Bonificacion = 'NO';
    
            if($request->Imagen)
            {
                $nameImg = time().'.' . explode('/', explode(':', substr($request->Imagen, 0, strpos($request->Imagen, ';')))[1])[1];
                //\Image::make($request->Imagen)->save(public_path('./imagenes/productos/').$nameImg);
                \Image::make($request->Imagen)->save(public_path('./imagenes/productos/').$request->Cod_Producto.'.jpg');
            }
    
            //$productos->Imagen = $nameImg;
            $productos->Imagen = $request->Cod_Producto.'.jpg';
            $productos->save();
            
            $informacion = new InventarioInfo();
            $informacion->idInventario = $productos->idInventario;
            if(is_null($request->info1)){
                $informacion->info1 = '';
            }else{
                $informacion->info1 = $request->info1;
            }
            if(is_null($request->info2)){
                $informacion->info2 = '';
            }else{
                $informacion->info2 = $request->info2;
            }
            if(is_null($request->info3)){
                $informacion->info3 = '';
            }else{
                $informacion->info3 = $request->info3;
            }
            if(is_null($request->info4)){
                $informacion->info4 = '';
            }else{
                $informacion->info4 = $request->info4;
            }
            if(is_null($request->info5)){
                $informacion->info5 = '';
            }else{
                $informacion->info5 = $request->info5;
            }
            $informacion->save();

            $kardex = new Kardex();
            $kardex->idInventario = $productos->idInventario;
            $kardex->NumDoc = 'Saldo Inicial';
            $kardex->Descripcion = 'Saldo Inicial';
            $kardex->TipoDoc = 'S.Inicial';
            $kardex->Cantidad = $request->Stock;
            $kardex->Costo = 0;
            $kardex->save();
            
            DB::commit();

        } catch (\Throwable $th) {
            DB::rollBack();
        }
       
    }
  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store1(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();
            if (!$request->ajax())  return redirect('/');
            $productos = new Productos();
            $productos->idDepartamento = $request->idDepartamento;
            $productos->idMarca = $request->idMarca;
            $productos->idClase = $request->idClase;
            $productos->idPresentacion = $request->idPresentacion;
            $productos->idUndMedida = $request->idUndMedida;
            $productos->idProveedor = $request->idProveedor;
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Stock = $request->Stock;
            $productos->Descripcion = $request->Descripcion;
            $productos->PorcComision = $request->PorcComision;
            $productos->Unidad_de_Paquete = $request->Unidad_Paquete;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->CantidadMin1 = $request->CantidadMin1;
            $productos->CantidadMin2 = $request->CantidadMin2;
            $productos->CantidadMin3 = $request->CantidadMin3;
            $productos->CantidadMin4 = $request->CantidadMin4;
            $productos->CantidadMin5 = $request->CantidadMin5;
            $productos->CantidadMax1 = $request->CantidadMax1;
            $productos->CantidadMax2 = $request->CantidadMax2;
            $productos->CantidadMax3 = $request->CantidadMax3;
            $productos->CantidadMax4 = $request->CantidadMax4;
            $productos->CantidadMax5 = $request->CantidadMax5;
            $productos->NombreOriginal = $request->NombreOriginal;
            $productos->Cod_Barra =  $request->Cod_Barra;
            $productos->Estado = 'Activo';
            $productos->Bonificacion = 'NO';    
            $productos->save();
            
            $informacion = new InventarioInfo();
            $informacion->idInventario = $productos->idInventario;
            if(is_null($request->info1)){
                $informacion->info1 = '';
            }else{
                $informacion->info1 = $request->info1;
            }
            if(is_null($request->info2)){
                $informacion->info2 = '';
            }else{
                $informacion->info2 = $request->info2;
            }
            if(is_null($request->info3)){
                $informacion->info3 = '';
            }else{
                $informacion->info3 = $request->info3;
            }
            if(is_null($request->info4)){
                $informacion->info4 = '';
            }else{
                $informacion->info4 = $request->info4;
            }
            if(is_null($request->info5)){
                $informacion->info5 = '';
            }else{
                $informacion->info5 = $request->info5;
            }
            $informacion->save();

            $kardex = new Kardex();
            $kardex->idInventario = $productos->idInventario;
            $kardex->NumDoc = 'Saldo Inicial';
            $kardex->Descripcion = 'Saldo Inicial';
            $kardex->Cantidad = $request->Stock;
            $kardex->Costo = 0;
            $kardex->save();
            
            DB::commit();

        } catch (\Throwable $th) {
            DB::rollBack();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            
            DB::beginTransaction();
            if (!$request->ajax())  return redirect('/');
            $productos = Productos::findOrFail($request->idInventario);
            $productos->idDepartamento = $request->idDepartamento;
            $productos->idMarca = $request->idMarca;
            $productos->idClase = $request->idClase;
            $productos->idPresentacion = $request->idPresentacion;
            $productos->idUndMedida = $request->idUndMedida;
            $productos->idProveedor = $request->idProveedor;
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Stock = $request->Stock;
            $productos->Descripcion = $request->Descripcion;
            $productos->PorcComision = $request->PorcComision;
            $productos->Unidad_de_Paquete = $request->Unidad_Paquete;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->CantidadMin1 = $request->CantidadMin1;
            $productos->CantidadMin2 = $request->CantidadMin2;
            $productos->CantidadMin3 = $request->CantidadMin3;
            $productos->CantidadMin4 = $request->CantidadMin4;
            $productos->CantidadMin5 = $request->CantidadMin5;
            $productos->CantidadMax1 = $request->CantidadMax1;
            $productos->CantidadMax2 = $request->CantidadMax2;
            $productos->CantidadMax3 = $request->CantidadMax3;
            $productos->CantidadMax4 = $request->CantidadMax4;
            $productos->CantidadMax5 = $request->CantidadMax5;
            $productos->NombreOriginal = $request->NombreOriginal;
            $productos->Cod_Barra =  $request->Cod_Barra;
            $productos->Estado = 'Activo';

                if($request->Imagen)
                {
                    $nameImg = time().'.' . explode('/', explode(':', substr($request->Imagen, 0, strpos($request->Imagen, ';')))[1])[1];
                    \Image::make($request->Imagen)->save(public_path('./imagenes/productos/').$request->Cod_Producto.'.jpg');
            }
    
            //$productos->Imagen = $nameImg;
            $productos->Imagen = $request->Cod_Producto.'.jpg';

            $productos->save();

            $informacion = InventarioInfo::where('idInventario', $productos->idInventario)->first();
            if(is_null($request->info1)){
                $informacion->info1 = '';
            }else{
                $informacion->info1 = $request->info1;
            }
            if(is_null($request->info2)){
                $informacion->info2 = '';
            }else{
                $informacion->info2 = $request->info2;
            }
            if(is_null($request->info3)){
                $informacion->info3 = '';
            }else{
                $informacion->info3 = $request->info3;
            }
            if(is_null($request->info4)){
                $informacion->info4 = '';
            }else{
                $informacion->info4 = $request->info4;
            }
            if(is_null($request->info5)){
                $informacion->info5 = '';
            }else{
                $informacion->info5 = $request->info5;
            }
            $informacion->save();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function update1(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        try {
            DB::beginTransaction();
            $productos = Productos::findOrFail($request->idInventario);
            $productos->idDepartamento = $request->idDepartamento;
            $productos->idMarca = $request->idMarca;
            $productos->idClase = $request->idClase;
            $productos->idPresentacion = $request->idPresentacion;
            $productos->idUndMedida = $request->idUndMedida;
            $productos->idProveedor = $request->idProveedor;
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Stock = $request->Stock;
            $productos->Descripcion = $request->Descripcion;
            $productos->PorcComision = $request->PorcComision;
            $productos->Unidad_de_Paquete = $request->Unidad_Paquete;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->CantidadMin1 = $request->CantidadMin1;
            $productos->CantidadMin2 = $request->CantidadMin2;
            $productos->CantidadMin3 = $request->CantidadMin3;
            $productos->CantidadMin4 = $request->CantidadMin4;
            $productos->CantidadMin5 = $request->CantidadMin5;
            $productos->CantidadMax1 = $request->CantidadMax1;
            $productos->CantidadMax2 = $request->CantidadMax2;
            $productos->CantidadMax3 = $request->CantidadMax3;
            $productos->CantidadMax4 = $request->CantidadMax4;
            $productos->CantidadMax5 = $request->CantidadMax5;
            $productos->NombreOriginal = $request->NombreOriginal;
            $productos->Cod_Barra =  $request->Cod_Barra;
            $productos->Estado = 'Activo';

            $productos->save();

            $informacion = InventarioInfo::where('idInventario', $productos->idInventario)->first();
            if(is_null($request->info1)){
                $informacion->info1 = '';
            }else{
                $informacion->info1 = $request->info1;
            }
            if(is_null($request->info2)){
                $informacion->info2 = '';
            }else{
                $informacion->info2 = $request->info2;
            }
            if(is_null($request->info3)){
                $informacion->info3 = '';
            }else{
                $informacion->info3 = $request->info3;
            }
            if(is_null($request->info4)){
                $informacion->info4 = '';
            }else{
                $informacion->info4 = $request->info4;
            }
            if(is_null($request->info5)){
                $informacion->info5 = '';
            }else{
                $informacion->info5 = $request->info5;
            }
            $informacion->save();

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
        
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $productos = Productos::findOrFail($request->idInventario);
        $productos->Estado = 'Descontinuado';
        $productos->save();
    }

    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $productos = Productos::findOrFail($request->idInventario);
        $productos->Estado = 'Activo';
        $productos->save();
    }

    public function ListadoProductoExcel()
    {   
        return Excel::download(new ProductoExport, 'ListadoProducto.xlsx');
        //return (new ProductoExport)->download('invoices.pdf', \Maatwebsite\Excel\Excel::DOMPDF);

        //return (new ProductoExport)->download('ListadoProducto.xlsx');
    }

    public function cantidad(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $id = $request->idprod;
        $cantidad = DB::table('Inventario')->select('Stock')->where('idInventario','=',$id)->pluck('Stock')->first(); 

        return['StockF'=>$cantidad];
    }

    public function SeleccionarAll(Request $request)
    {
       if (!$request->ajax())  return redirect('/');
       $buscar = $request->buscar;
       if( $buscar == ''){
           $productos=Productos::orderBy('Nombre','asc')->get();
       }else{
            $productos=Productos::where('Nombre','like','%'.$buscar.'%')->orWhere('Cod_Producto','like','%'.$buscar.'%')->orderBy('Nombre','asc')->take(50)->get();
       }
        return ['productos'=>$productos];
        
    }
    
    public function servindex(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;
        $filtro = $request->filtro;

       if ($buscar == '') {
            $productos = DB::table('Inventario as i')->join('Departamentos as d', 'i.idDepartamento', '=', 'd.idDepartamentos')->join('Marcas as m','i.idMarca', '=', 'm.idMarca')
                            ->join('Clase_Producto as cp','i.idClase', '=', 'cp.idClase')->join('Presentacion as p','i.idPresentacion', '=', 'p.idPresentacion')
                            ->join('Unidad_Medida as um','i.idUndMedida', '=', 'um.idUnidadMedida')->join('Proveedores as pv','i.idProveedor', '=', 'pv.idProveedor')->leftjoin('InventarioInfoAdic as ii','i.idInventario','=','ii.idInventario')
                            ->select('i.idInventario','d.idDepartamentos','d.Nombre as Departamento' ,'m.idMarca', 'm.Nombre as Marca', 'cp.idClase','cp.Nombre as Clase','p.idPresentacion','p.Nombre as Presentacion','um.idUnidadMedida','um.Nombre as Unidad','pv.idProveedor','pv.Nombre as Proveedor',
                            'i.Cod_Producto','i.Nombre as Inventario', 'i.Stock', 'i.Descripcion', 'i.PorcComision', 'i.Unidad_de_Paquete','i.Precio1','i.Precio2',DB::RAW("convert(varchar,cast(i.Precio3 as money), 1) as Precio3"),'i.Precio4','i.Precio5','i.PrecioDolar1','i.PrecioDolar2','i.PrecioDolar3','i.PrecioDolar4',
                            'i.PrecioDolar5','i.CantidadMax1','i.CantidadMax2','i.CantidadMax3','i.CantidadMax4','i.CantidadMax5','i.CantidadMin1','i.CantidadMin2','i.CantidadMin3','i.CantidadMin4','i.CantidadMin5','i.Estado','i.Cod_Barra','i.Imagen','i.NombreOriginal',
                            'ii.info1','ii.info2','ii.info3','ii.info4','ii.info5')->where('i.Servicio','=','2')
                            ->orderBy('i.Cod_Producto', 'desc')->paginate(10);
        } else {
            $productos =  DB::table('Inventario as i')->join('Departamentos as d', 'i.idDepartamento', '=', 'd.idDepartamentos')->join('Marcas as m','i.idMarca', '=', 'm.idMarca')
                            ->join('Clase_Producto as cp','i.idClase', '=', 'cp.idClase')->join('Presentacion as p','i.idPresentacion', '=', 'p.idPresentacion')
                            ->join('Unidad_Medida as um','i.idUndMedida', '=', 'um.idUnidadMedida')->join('Proveedores as pv','i.idProveedor', '=', 'pv.idProveedor')->leftjoin('InventarioInfoAdic as ii','i.idInventario','=','ii.idInventario')
                            ->select('i.idInventario','d.idDepartamentos','d.Nombre as Departamento' ,'m.idMarca', 'm.Nombre as Marca', 'cp.idClase','cp.Nombre as Clase','p.idPresentacion','p.Nombre as Presentacion','um.idUnidadMedida','um.Nombre as Unidad','pv.idProveedor','pv.Nombre as Proveedor',
                            'i.Cod_Producto','i.Nombre as Inventario', 'i.Stock', 'i.Descripcion', 'i.PorcComision', 'i.Unidad_de_Paquete','i.Precio1','i.Precio2',DB::RAW("convert(varchar,cast(i.Precio3 as money), 1) as Precio3"),'i.Precio4','i.Precio5','i.PrecioDolar1','i.PrecioDolar2','i.PrecioDolar3','i.PrecioDolar4',
                            'i.PrecioDolar5','i.CantidadMax1','i.CantidadMax2','i.CantidadMax3','i.CantidadMax4','i.CantidadMax5','i.CantidadMin1','i.CantidadMin2','i.CantidadMin3','i.CantidadMin4','i.CantidadMin5','i.Estado','i.Cod_Barra','i.Imagen','i.NombreOriginal',
                            'ii.info1','ii.info2','ii.info3','ii.info4','ii.info5')
                            ->where([['i.Nombre','like','%'.$buscar.'%'],['i.Servicio','=','2']])
                            ->orderBy('i.Cod_Producto','desc')->paginate(10);
        }


        return [
            'pagination'=>[
                'total'=>$productos->total(),
                'current_page'=>$productos->currentPage(),
                'per_page'=>$productos->perPage(),
                'last_page'=>$productos->lastPage(),
                'from'=>$productos->firstItem(),
                'to'=>$productos->lastItem(),
            ], 
            'servicio' => $productos
        ];
    }

    public function servstore(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        
        try {
            DB::beginTransaction();
            $productos = new Productos();
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Descripcion = NULL;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->NombreOriginal = $request->Nombre;
            $productos->idDepartamento = 2;
            $productos->idMarca = 1;
            //$productos->idClase = 24;
            $productos->idClase = 1;
            $productos->idPresentacion = 1;
            $productos->idUndMedida = 1;
            $productos->idProveedor = 2;
            $productos->Stock = 0;
            $productos->PorcComision = 0;
            $productos->Unidad_de_Paquete = 0;
            $productos->CantidadMin1 = 0;
            $productos->CantidadMin2 = 0;
            $productos->CantidadMin3 = 0;
            $productos->CantidadMin4 = 0;
            $productos->CantidadMin5 = 0;
            $productos->CantidadMax1 = 0;
            $productos->CantidadMax2 = 0;
            $productos->CantidadMax3 = 0;
            $productos->CantidadMax4 = 0;
            $productos->CantidadMax5 = 0;
            $productos->Cod_Barra = NULL;
            $productos->Estado = 'Activo';
            $productos->Bonificacion = 'NO';    
            $productos->Servicio = 2;    
            $productos->save();

            $informacion = new InventarioInfo();
            $informacion->idInventario = $productos->idInventario;
            $informacion->info1 = NULL;
            $informacion->info2 = NULL;           
            $informacion->info3 = NULL;
            $informacion->info4 = NULL;
            $informacion->info5 = NULL;
            $informacion->save();

            DB::commit();

        } catch (\Throwable $th) {
            DB::rollBack();
        }
    }

    public function servupdate(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
            $productos = Productos::findOrFail($request->idInventario);
            $productos->Cod_Producto= $request->Cod_Producto;
            $productos->Nombre = $request->Nombre;
            $productos->Descripcion = $request->Descripcion;
            $productos->Precio1 = $request->Precio1;
            $productos->Precio2 = $request->Precio2;
            $productos->Precio3 = $request->Precio3;
            $productos->Precio4 = $request->Precio4;
            $productos->Precio5 = $request->Precio5;
            $productos->PrecioDolar1 = $request->PrecioDolar1;
            $productos->PrecioDolar2 = $request->PrecioDolar2;
            $productos->PrecioDolar3 = $request->PrecioDolar3;
            $productos->PrecioDolar4 = $request->PrecioDolar4;
            $productos->PrecioDolar5 = $request->PrecioDolar5;
            $productos->Estado = 'Activo';

            $productos->save();
    }

    public function servdeshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $productos = Productos::findOrFail($request->idInventario);
        $productos->Estado = 'Descontinuado';
        $productos->save();
    }

    public function servhabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $productos = Productos::findOrFail($request->idInventario);
        $productos->Estado = 'Activo';
        $productos->save();
    }

    public function prodVencidos()
    {
        $productos = DB::select('exec sp_ProdVencido');
        return ['productos'=>$productos];
    }
}
