<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use App\Proveedores;
use Illuminate\Support\Facades\DB;

class ProveedoresController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $proveedor = Proveedores::orderBy('Nombre','asc')->paginate(10);
        } else {
            $proveedor = Proveedores::where('Nombre','like','%'.$buscar.'%')->orderBy('Nombre','asc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$proveedor->total(),
                'current_page'=>$proveedor->currentPage(),
                'per_page'=>$proveedor->perPage(),
                'last_page'=>$proveedor->lastPage(),
                'from'=>$proveedor->firstItem(),
                'to'=>$proveedor->lastItem(),
            ], 
            'proveedor' => $proveedor
        ];
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $proveedor = new Proveedores();
        $proveedor->Nombre = $request->Nombre;
        $proveedor->Pais = $request->Pais;
        $proveedor->NombreCompleto = $request->NombreCompleto;
        $proveedor->Email = $request->Email;
        $proveedor->PaginaWeb = $request->PaginaWeb;
        $proveedor->Direccion = $request->Direccion;
        $proveedor->Telefono = $request->Telefono;
        $proveedor->Wechat = $request->Wechat;
        $proveedor->Vendedor = $request->Vendedor;
        $proveedor->QQ = $request->QQ;

        if($request->Imagen)
        {
            $nameImg = time().'.' . explode('/', explode(':', substr($request->Imagen, 0, strpos($request->Imagen, ';')))[1])[1];
            \Image::make($request->Imagen)->save(public_path('./imagenes/proveedor/').$nameImg);
        }

       $proveedor->Imagen = $nameImg;
        $proveedor->save();


    }
  /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store1(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $proveedor = new Proveedores();
        $proveedor->Nombre = $request->Nombre;
        $proveedor->Pais = $request->Pais;
        $proveedor->NombreCompleto = $request->NombreCompleto;
        $proveedor->Email = $request->Email;
        $proveedor->PaginaWeb = $request->PaginaWeb;
        $proveedor->Direccion = $request->Direccion;
        $proveedor->Telefono = $request->Telefono;
        $proveedor->Wechat = $request->Wechat;
        $proveedor->Vendedor = $request->Vendedor;
        $proveedor->QQ = $request->QQ;
        $proveedor->save();


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $proveedor = Proveedores::findOrFail($request->idProveedor);
        $proveedor->Nombre = $request->Nombre;
        $proveedor->Pais = $request->Pais;
        $proveedor->NombreCompleto = $request->NombreCompleto;
        $proveedor->Email = $request->Email;
        $proveedor->PaginaWeb = $request->PaginaWeb;
        $proveedor->Direccion = $request->Direccion;
        $proveedor->Telefono = $request->Telefono;
        $proveedor->Wechat = $request->Wechat;
        $proveedor->Vendedor = $request->Vendedor;
        $proveedor->QQ = $request->QQ;
            if($request->Imagen)
            {
                $nameImg = time().'.' . explode('/', explode(':', substr($request->Imagen, 0, strpos($request->Imagen, ';')))[1])[1];
                \Image::make($request->Imagen)->save(public_path('imagenes/proveedor/').$nameImg);
            }
            $proveedor->Imagen = $nameImg;

        $proveedor->save();
    }

    public function update1(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $proveedor = Proveedores::findOrFail($request->idProveedor);
        $proveedor->Nombre = $request->Nombre;
        $proveedor->Pais = $request->Pais;
        $proveedor->NombreCompleto = $request->NombreCompleto;
        $proveedor->Email = $request->Email;
        $proveedor->PaginaWeb = $request->PaginaWeb;
        $proveedor->Direccion = $request->Direccion;
        $proveedor->Telefono = $request->Telefono;
        $proveedor->Wechat = $request->Wechat;
        $proveedor->Vendedor = $request->Vendedor;
        $proveedor->QQ = $request->QQ;
        $proveedor->save();
    }

    public function SeleccionarProveedor(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $proveedor = DB::select('exec sp_listarProveedor');

        return ['proveedor'=>$proveedor];
    }

}
