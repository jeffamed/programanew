<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SalidaProducto;
use App\DetalleSalida;
use App\Kardex;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class SalidaController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if ($buscar == '') {
            $salida = SalidaProducto::orderBy('idSalidaProducto','desc')->paginate(10);
        } else {
            $salida = SalidaProducto::where('idSalidaProducto','like','%'.$buscar.'%')
                    ->orderBy('idSalidaProducto','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$salida->total(),
                'current_page'=>$salida->currentPage(),
                'per_page'=>$salida->perPage(),
                'last_page'=>$salida->lastPage(),
                'from'=>$salida->firstItem(),
                'to'=>$salida->lastItem(),
            ], 
            'salida' => $salida
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        try {
            DB::beginTransaction();

            $salida = new SalidaProducto();
            $salida->Comentario = $request->Observacion;
            $salida->Total = $request->Total;
            $salida->FechaSalida = $request->FechaSalida;
            $salida->Usuario = \Auth::user()->name;
            $salida->Estado = 'Activo';
            $salida->save();

            $detalles = $request->data;

            foreach ($detalles as $key => $det) {
                $detalle = new DetalleSalida();
                $detalle->idSalida = $salida->idSalidaProducto;
                $detalle->idInventario = $det['idproducto'];
                $detalle->Cantidad = $det['salida'];
                $detalle->Precio = $det['precio'];
                $detalle->save();

                $kardex = new Kardex();
                $kardex->idInventario = $det['idproducto'];
                $kardex->NumDoc = $salida->idSalidaProducto;
                $kardex->Descripcion = 'Salida No.: '.$salida->idSalidaProducto;
                $kardex->TipoDoc = 'Salida';
                $kardex->Cliente = \Auth::user()->name;
                $kardex->Cantidad = $det['salida'];
                $kardex->Costo =  $det['precio'];
                $kardex->Observacion = $request->Observacion;
                $kardex->save();
            }

            DB::commit();
            
        } catch (\Throwable $th) {
            DB::rollback();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $salida = SalidaProducto::findOrFail($request->idSalida);
        $salida->Comentario = $request->Observacion;
        $salida->Total = $request->Total;
        $salida->FechaSalida = $request->FechaSalida;
        $salida->Estado = 'Activo';
        $salida->save();

        $detalles = $request->data;

        foreach ($detalles as $key => $det) {
            $detalle = DetalleSalida::where([['idSalida','=',$request->idSalida],['idInventario','=',$det['idInventario']]])->firstOrFail();
            $detalle->Cantidad = $det['Cantidad'];
            $detalle->save();

            $kardex = Kardex::where([['TipoDoc','=','Salida'],['idInventario','=',$det['idInventario']],['NumDoc','=',$request->idSalida]])->firstOrFail();
            $kardex->Cantidad = $det['Cantidad'];
            $kardex->save();
        }

    }
    public function DatosSalida(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idSalida = $request->id;
        $salida=DB::select('exec sp_DatosSalida ?', [$idSalida]);

        return ['salida'=>$salida];
    }
    
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $salida = SalidaProducto::findOrFail($request->idSalida);
        $salida->Estado = 'Inactivo';
        $salida->save();
    }

    public function ultimoRegistro(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $id = SalidaProducto::select('idSalidaProducto')->orderBy('idSalidaProducto','desc')->pluck('idSalidaProducto')->first();
        return ['id'=>$id];
    }

    public function printSalida($id)
    {
        $fecha = Carbon::now();
        $hoy = $fecha->format('d/m/Y');
        $usuario = \Auth::user()->name;
        $datos = DB::table('Salida_Productos as e')
                ->select('e.idSalidaProducto',DB::RAW('CONVERT(varchar,e.FechaSalida,103) as fecha'),'e.Comentario','e.Total')
                ->where('e.idSalidaProducto','=',$id)
                ->get();
        $detalle = DB::table('Detalle_Salidas as de')
                ->join('Inventario as i','de.idInventario','=','i.idInventario')
                ->select('i.Cod_Producto','i.Nombre','de.Cantidad','de.Precio')
                ->where('idSalida','=',$id)
                ->get();
        $pdf = \PDF::loadView('pdf.salida', ['datos'=>$datos,'detalle'=>$detalle,"user"=>$usuario ,"hoy"=>$hoy])->setPaper("letter","portrait");
        return $pdf->stream();
    }
}
