<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Talonario;
use Illuminate\Support\Facades\DB;

class TalonarioController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $buscar = $request->buscar;
        $filtro = $request->filtro;

        if($buscar == ''){
            $talonario = DB::table('Talonarios as t')->join('Vendedores as v','t.idVendedor','=','v.idVendedor')
                            ->select('t.idTalonario','t.idVendedor','v.Nombre','t.NumeracionInicial','t.Estado', 'Usuario')
                            ->where('t.Estado','=',$filtro)->paginate(10);
        }else{
            $talonario =  DB::table('Talonarios as t')->join('Vendedores as v','t.idVendedor','=','v.idVendedor')
                            ->select('t.idTalonario','t.idVendedor','v.Nombre','t.NumeracionInicial','t.Estado', 'Usuario')
                            ->where([
                                ['v.Nombre','like','%'. $buscar .'%'],
                                ['t.Estado','=', $filtro]
                                ])
                            ->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$talonario->total(),
                'current_page'=>$talonario->currentPage(),
                'per_page'=>$talonario->perPage(),
                'last_page'=>$talonario->lastPage(),
                'from'=>$talonario->firstItem(),
                'to'=>$talonario->lastItem(),
            ],
            'talonario' => $talonario
        ];
    }

    public function ultimoRegistro(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $ultimo = Talonario::select('NumeracionInicial')->OrderBy('idTalonario', 'desc')->first();
        return['ultimo'=>$ultimo];
    }

    public function VerificarVendedor(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $idVendedor = $request->idVendedor;
        $pendientes = DB::select('exec sp_listarVendedorRecibo ?', [$idVendedor]);
        return['pendientes'=>$pendientes];
    }

    public function ConfirmarTalonario(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $contraseña = $request->contraseña;
        $verificar = DB::table('Autorizacion')->select('Nombre')->where('contraseña','=',$contraseña)->get();
        return ['verificar'=>$verificar];
    }

    public function listadoAsignacion(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $idvendedor = $request->idVend;
        $listado = DB::select('exec sp_listarVendedoresRecibosAsignados ?', [$idvendedor]);
        return ['listado'=>$listado];
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        
        $inicio = $request->NumInicial;
        $fin = $request->NumFinal;
        $idvendedor = $request->idVendedor;
        $user= \Auth::user()->name;
        DB::statement('exec sp_agregarTalonarios ?, ?, ?, ?', [$idvendedor,$inicio,$fin, $user]);

    }

    public function SeleccionarTalonario(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $talonario = DB::select('exec sp_listartalonario');

        return ['talonario'=>$talonario];
    }

    public function RecibosDisponible(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $idVendedor = $request->idVend;
        $pendientes = DB::select('exec sp_listarTalonarioxVendedor ?', [$idVendedor]);

        return ['pendientes'=>$pendientes];
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $talonario = Talonario::findOrFail($request->idTalonario);
        $talonario->Estado = 'Anulado';
        $talonario->MotivoAnulacion = $request->Motivo.' Generado por: '.\Auth::user()->name;
        $talonario->save();
    }

    public function EliminarTalonario(Request $request)
    {
       if (!$request->ajax())  return redirect('/');

        $inicio = $request->inicio;
        $final = $request->final;

        DB::statement('exec sp_anularTalonarios ?, ?',[$inicio, $final]);
    }

}
