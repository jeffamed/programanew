<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnidadMedida;

class UnidadMedidaController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        //$UnidadMedida = UnidadMedida::all();
        $buscar = $request->buscar;

        if ($buscar=='') {
            $UnidadMedida = UnidadMedida::paginate(10);
        } else {
            $UnidadMedida = UnidadMedida::where('Nombre','like','%'.$buscar.'%')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$UnidadMedida->total(),
                'current_page'=>$UnidadMedida->currentPage(),
                'per_page'=>$UnidadMedida->perPage(),
                'last_page'=>$UnidadMedida->lastPage(),
                'from'=>$UnidadMedida->firstItem(),
                'to'=>$UnidadMedida->lastItem(),
            ],
            'UnidadMedida' => $UnidadMedida
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $UnidadMedida = new UnidadMedida();
        $UnidadMedida->nombre = $request->nombre;
        $UnidadMedida->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $UnidadMedida = UnidadMedida::findOrFail($request->idUnidadMedida);
        $UnidadMedida->nombre = $request->nombre;
        $UnidadMedida->save();
    }

    public function SeleccionarUnidad(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $UnidadMedida = DB::select('exec sp_listarUnidadMedida');

        return ['UnidadMedida'=>$UnidadMedida];
    }

}
