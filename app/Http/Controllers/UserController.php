<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Permission;
use App\User;
use App\PermissionUser;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        
        $buscar = $request->buscar;

        if ($buscar == '') {
            $user = User::orderBy('name','desc')->paginate(10);
        } else {
            $user = User::where('name','like','%'.$buscar.'%')->orderBy('name','desc')->paginate(10);
        }
        return [
            'pagination'=>[
                'total'=>$user->total(),
                'current_page'=>$user->currentPage(),
                'per_page'=>$user->perPage(),
                'last_page'=>$user->lastPage(),
                'from'=>$user->firstItem(),
                'to'=>$user->lastItem(),
            ], 
            'user' => $user
        ];
    }

    public function listarusuario()
    {
        $usuarios = User::where('state','=','Activo')->get();

        return ['usuarios'=>$usuarios];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $ser = new User();
        $ser->name = $request->name;
        $ser->email = $request->email;
        $ser->password = bcrypt($request->pass);
        $ser->state = 'Activo';
        $ser->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $user = User::findOrFail($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->pass);
        $user->state = 'Activo';
        $user->save();
    }
    /* Deactivar Registro */
    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $user = User::findOrFail($request->id);
        $user->state = 'Inactivo';
        $user->save();
    }
    /* Activar Registro */
    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $user = User::findOrFail($request->id);
        $user->state = 'Activo';
        $user->save();
    }

    public function permisos(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $buscar = $request->buscador;
        $id = $request->id;
        /*$permiso = Permission::where('slug','like',$buscar.'%')->get();*/
        $permiso = DB::select('exec sp_PermisionUsuarios ?, ?', [$id,$buscar]);

        return ['permiso' => $permiso];
    }

    public function permisosUsuarios(Request $request)
    {
        if (!$request->ajax())  return redirect('/main');
        $id = $request->iduser;
        $permisousuario = DB::table('permission_user as pu')->join('permissions as p','pu.permission_id','=','p.id')
                        ->select ('pu.id','p.name')->where('user_id','=',$id)->get();

        return ['permisousuario' => $permisousuario];
    }

    public function storepermisos(Request $request)
    {
        if (!$request->ajax()) return redirect('/main');
        $permUser = new PermissionUser();
        $permUser->permission_id = $request->idperm;
        $permUser->user_id = $request->iduser;
        $permUser->save();
    }

    public function destroypermiso(Request $request,$id)
    {
        if (!$request->ajax()) return redirect('/main');
        $permiso = PermissionUser::findOrFail($id);
        $permiso->delete();
    }
}
