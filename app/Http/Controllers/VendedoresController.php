<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendedores;
use Illuminate\Support\Facades\DB;

class VendedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $buscar = $request->buscar;

        if($buscar == ''){
            $vendedor = DB::table('Vendedores as v')->join('Zonas as z','v.idZona','=','z.idZona')
                        ->select('v.idVendedor', 'z.idZona','v.Nombre as Vendedores', 'z.Nombre as Zonas', 'v.Telefono','v.Celular', 'v.Estado','v.Pin')
                        ->orderBy('v.nombre', 'asc')->paginate(10);
        }else{
            $vendedor = DB::table('Vendedores as v')->join('Zonas as z','v.idZona','=','z.idZona')
                        ->select('v.idVendedor', 'z.idZona', 'v.Nombre as Vendedores', 'z.Nombre as Zonas', 'v.Telefono','v.Celular', 'v.Estado','v.Pin')
                        ->where('v.Nombre','like','%'. $buscar .'%')
                        ->orderBy('v.nombre', 'asc')->paginate(10);
        }

        return [
            'pagination'=>[
                'total'=>$vendedor->total(),
                'current_page'=>$vendedor->currentPage(),
                'per_page'=>$vendedor->perPage(),
                'last_page'=>$vendedor->lastPage(),
                'from'=>$vendedor->firstItem(),
                'to'=>$vendedor->lastItem(),
            ],
            'vendedor' => $vendedor
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $vendedor = new Vendedores();
        $vendedor->idZona = $request->idZona;
        $vendedor->Nombre = $request->Nombre;
        $vendedor->Telefono = $request->Telefono;
        $vendedor->Celular = $request->Celular;
        $vendedor->Pin = $request->Pin;
        $vendedor->Estado = 'Activo';
        $vendedor->save();
    }

    public function SeleccionarVendedor(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $vendedor = DB::select('exec sp_listarVendedor');
        return ['vendedor' => $vendedor];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $vendedor = Vendedores::findOrFail($request->idVendedor);
        $vendedor->idZona = $request->idZona;
        $vendedor->Nombre = $request->Nombre;
        $vendedor->Telefono = $request->Telefono;
        $vendedor->Celular = $request->Celular;
        $vendedor->Pin = $request->Pin;
        $vendedor->Estado = 'Activo';
        $vendedor->save();
    }

    public function deshabilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $vendedor = Vendedores::findOrFail($request->idVendedor);
        $vendedor->Estado = 'Inactivo';
        $vendedor->save();
    }

    public function habilitar(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $vendedor = Vendedores::findOrFail($request->idVendedor);
        $vendedor->Estado = 'Activo';
        $vendedor->save();
    }
    public function VendedorAll(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $buscar = $request->buscar;
        if($buscar == ''){
            $vendedor = Vendedores::where('Estado','=','Activo')->orderBy('Nombre','asc')->get();
        }else{
            $vendedor = Vendedores::where('Nombre','like','%'.$buscar.'%')->where('Estado','=','Activo')->orderBy('Nombre','asc')->get();
        }
        return['vendedor'=>$vendedor];
    }
 
}
