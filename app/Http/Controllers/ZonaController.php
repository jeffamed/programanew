<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Zona;

class ZonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        //$zona = Zona::all();
        $buscar = $request->buscar;
        if ($buscar == '') {
            $zona  = Zona::orderBy('Nombre','asc')->paginate(10);
        } else {
            $zona = Zona::where('Nombre','like','%'.$buscar.'%')->orderBy('Nombre','asc')->paginate(10);
        }
        
        return [
            'pagination'=>[
                'total'=>$zona->total(),
                'current_page'=>$zona->currentPage(),
                'per_page'=>$zona->perPage(),
                'last_page'=>$zona->lastPage(),
                'from'=>$zona->firstItem(),
                'to'=>$zona->lastItem(),
            ],
            'zona' => $zona
        ];
        //return $zona;
    }

    public function SeleccionarZona(Request $request)
    {
        if (!$request->ajax())  return redirect('/');

        $zona = DB::select('exec sp_listarZona');

        return ['zona'=>$zona];
    }

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $zona = new Zona();
        $zona->Nombre = $request->Nombre;
        $zona->save(); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $zona = Zona::findOrFail($request->idZona);
        $zona->Nombre = $request->Nombre;
        $zona->save();
    }

    public function SeleccionarAll(Request $request)
    {
        if (!$request->ajax())  return redirect('/');
        $buscar = $request->buscar;
        if($buscar == ''){
            $zonas = Zona::orderBy('Nombre', 'asc')->get();
        }else{
            $zonas = Zona::where('Nombre','LIKE','%'.$buscar.'%')->orderBy('Nombre', 'asc')->get();
        }
        return ['zonas'=>$zonas];
    }

}
