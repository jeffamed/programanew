<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventarioInfo extends Model
{
    protected $table = 'InventarioInfoAdic';

    protected $primaryKey = 'idInventarioInfo';

    protected $fillable = ['idInventario','info1','info2','info3','info4','info5'];
}