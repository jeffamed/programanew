<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kardex extends Model
{
    protected $table = 'Kardex';

    protected $primaryKey = 'idKardex';

    protected $fillable = ['idInventario','NumDoc','Descripcion','Cantidad','Costo','TipoDoc','Cliente','Observacion'];
}
