<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class DemoEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $cont;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Corte MARNOR';
    
    public function __construct($cont)
    
    {
        $this->cont = $cont;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.demo');
    }
}
