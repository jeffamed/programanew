<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaDebito extends Model
{
    protected $table = 'NotaDebito';

    protected $primaryKey = 'idNota';

    protected $fillable = ['idRecibo','idFactAplic','idCliente','Monto','Estado'];
}
