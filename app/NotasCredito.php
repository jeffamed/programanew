<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotasCredito extends Model
{
    protected $table = 'NotasCreditos';

    protected $primaryKey = 'idNota';

    protected $fillable = ['idCliente','idFactura','Monto','TipoNota','TasaCambio','Observacion','NoDevolucion'];

    // tipo de nota Devolucion, Desc x pronto pago, cheque sin fondo, varios

}
