<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagosCxC extends Model
{
    protected $table = 'Pagos_CxC';

    protected $primaryKey = 'idPagoCxC';

    protected $fillable = ['idTalonario','idCliente','Abono','Estado', 'MotivoAnulacion','Observacion','Usuario','FechaRegistro'];
}
