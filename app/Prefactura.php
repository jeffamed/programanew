<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prefactura extends Model
{
    protected $table = 'Prefacturas';

    protected $primaryKey = 'idPrefactura';

    protected $fillable = [
        'NumPrefactura',
        'idCliente',
        'idVendedor',
        'TipoFactura',
        'TipoCompra',
        'Estado',
        'Total',
        'TotalComisiones',
        'NombreCliente',
        'NumFactura',
        'FechaExp',
        'created_at',
        'updated_at',
        'idFactura'
    ];
}
