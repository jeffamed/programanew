<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presentacion extends Model
{
    protected $table = 'Presentacion';
    protected $primaryKey = 'idPresentacion';

    public $fillable = ['Nombre', 'Descripcion'];
}
