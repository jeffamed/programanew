<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoVencidos extends Model
{
    protected $table = 'Producto_Vencer';

    protected $primaryKey = 'idProdVen';

    protected $fillable = [
        'idInventario',
        'Nombre',
        'FechaVenc',
        'created_at',
        'updated_at'
    ];
}
