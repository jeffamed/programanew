<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = 'Inventario';

    protected $primaryKey = 'idInventario';

    protected $fillable = ['idDepartamento', 'idMarca', 'idClase','idPresentacion','idUndMedida','idProveedor','Cod_Producto','Nombre','Stock','Descripcion','PorcComision','Unidad_de_Paquete','Precio1',
                        'Precio2','Precio3','Precio4','Precio5','PrecioDolar1','PrecioDolar2','PrecioDolar3','PrecioDolar4','PrecioDolar5','CantidadMin1','CantidadMax1','CantidadMin2','CantidadMax2','CantidadMin3',
                        'CantidadMax3','CantidadMin4','CantidadMax4','CantidadMin5','CantidadMax5','NombreOriginal','Cod_Barra','Imagen','Estado','Bonificacion'];
}
