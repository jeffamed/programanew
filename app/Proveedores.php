<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $table = "Proveedores";
    protected $primaryKey = 'idProveedor';

    public $fillable = ['Nombre','NombreCompleto','Email', 'PaginaWeb','Direccion','Telefono','Wechat','Vendedor','QQ','Pais', 'Imagen'];
}
