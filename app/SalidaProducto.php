<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalidaProducto extends Model
{
    protected $table = 'Salida_Productos';

    protected $primaryKey = 'idSalidaProducto';

    protected $fillable = ['FechaSalida','Comentario','Total','Estado','Usuario'];
}
