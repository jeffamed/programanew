<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Talonario extends Model
{
    protected $primaryKey = 'idTalonario';

    protected $fillable = ['idVendedor','NumeriacionInicial','Serie'];
}
