<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $table = "Unidad_Medida";

    protected $primaryKey = "idUnidadMedida";

    public $fillable = ['Nombre'];
}
