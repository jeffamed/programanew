<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedores extends Model
{
    protected $table = "Vendedores";

    protected $primaryKey = "idVendedor";

    public $fillable = ['idZona','Nombre','Telefono','Celular','Estado','Pin'];
}
