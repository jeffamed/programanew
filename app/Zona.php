<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    //protected $table = "Unidad_Medida";

    protected $primaryKey = "idZona";

    public $fillable = ['Nombre'];
}
