/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Permissions from './mixins/Permissions';
Vue.mixin(Permissions);


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('banco', require('./components/Banco.vue').default);
Vue.component('marca', require('./components/Marca.vue').default);
Vue.component('unidad-medida', require('./components/UnidadMedida.vue').default);
Vue.component('zona', require('./components/Zonas.vue').default);
Vue.component('clase-producto', require('./components/ClaseProducto.vue').default);
Vue.component('presentacion', require('./components/Presentacion.vue').default);
Vue.component('departamento', require('./components/Departamento.vue').default);
Vue.component('tasa-cambio', require('./components/TasaCambio.vue').default);
Vue.component('vendedores', require('./components/Vendedores.vue').default);
Vue.component('clientes', require('./components/Clientes.vue').default);
Vue.component('producto', require('./components/Producto.vue').default);
Vue.component('proveedor', require('./components/Proveedor.vue').default);
Vue.component('factura', require('./components/Factura.vue').default);
Vue.component('devolucion', require('./components/Devolucion.vue').default);
Vue.component('talonario', require('./components/Talonario.vue').default);
Vue.component('pagos-cxc', require('./components/Pagos.vue').default);
Vue.component('comision', require('./components/Comision.vue').default);
Vue.component('cheque', require('./components/Cheque.vue').default);
Vue.component('entrada-producto', require('./components/Entrada.vue').default);
Vue.component('salida-producto', require('./components/Salida.vue').default);
Vue.component('admin-bonificacion', require('./components/AdmonBonificacion.vue').default);
Vue.component('cotizacion', require('./components/Cotizacion.vue').default);
Vue.component('compra-producto', require('./components/Compra.vue').default);
Vue.component('nota-credito', require('./components/NotaCredito.vue').default);
Vue.component('nota-debito', require('./components/NotaDebito.vue').default);
Vue.component('user-system', require('./components/User.vue').default);
Vue.component('kardex', require('./components/Kardex.vue').default);
Vue.component('estado-cuenta', require('./components/EstadoCuenta.vue').default);
Vue.component('informe-transacciones', require('./components/Transacciones.vue').default);
Vue.component('informe-cxc', require('./components/CxCInforme.vue').default);
Vue.component('estadistica-venta', require('./components/EstVenta.vue').default);
Vue.component('cheque-minuta', require('./components/ChequeMinuta.vue').default);
Vue.component('pre-factura', require('./components/Prefactura.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data :{
        menu: 0,  
        //ruta: 'http://www.sinevol.com/ProgramaNew/public',
        ruta: '',
    }
});
