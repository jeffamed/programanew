@extends('principal')
@section('contenido')
    <template v-if="menu==0">
        <example-component :ruta="ruta"></example-component>
    </template>

    <template v-if="menu==1">
        <factura :ruta="ruta"></factura>
    </template>

    <template v-if="menu==2">
        <cotizacion :ruta="ruta"></cotizacion>
    </template>

    <template v-if="menu==3">
        <devolucion :ruta="ruta"></devolucion>        
    </template>

    <template v-if="menu==4">
        <clientes :ruta="ruta"></clientes>
    </template>

    <template v-if="menu==5">
        <departamento :ruta="ruta"></departamento>
    </template>

    <template v-if="menu==6">
        <clase-producto :ruta="ruta"></clase-producto>
    </template>

    <template v-if="menu==7">
        <marca :ruta="ruta"></marca>
    </template>

    <template v-if="menu==8">
        <unidad-medida :ruta="ruta"></unidad-medida>
    </template>

    <template v-if="menu==9">
        <presentacion :ruta="ruta"></presentacion>
    </template>

    <template v-if="menu==10">
        <vendedores :ruta="ruta"></vendedores>
    </template>

    <template v-if="menu==11">
        <zona :ruta="ruta"></zona>
    </template>

    <template v-if="menu==12">
        <tasa-cambio :ruta="ruta"></tasa-cambio>
    </template>

    <template v-if="menu==13">
        <producto :ruta="ruta"></producto>
    </template>

    <template v-if="menu==14">
        <entrada-producto :ruta="ruta"></entrada-producto>
    </template>

    <template v-if="menu==15">
        <salida-producto :ruta="ruta"></salida-producto>
    </template>

    <template v-if="menu==16">
        <compra-producto :ruta="ruta"></compra-producto>
    </template>

    <template v-if="menu==17">
        <proveedor :ruta="ruta"></proveedor>
    </template>

    <template v-if="menu==18">
        <h2>contenido del menu 18</h2>
    </template>

    <template v-if="menu==19">
        <pagos-cxc :ruta="ruta"></pagos-cxc>
    </template>

    <template v-if="menu==20">
        <cheque :ruta="ruta"></cheque>
    </template>

    <template v-if="menu==21">
        <nota-credito :ruta="ruta"></nota-credito>
    </template>

    <template v-if="menu==22">
        <nota-debito :ruta="ruta"></nota-debito>
    </template>

    <template v-if="menu==23">
        <estado-cuenta :ruta="ruta"></estado-cuenta>
    </template>

    <template v-if="menu==24">
        <banco :ruta="ruta"></banco>
    </template>

    <template v-if="menu==25">
        <user-system :ruta="ruta"></user-system>
    </template>

    <template v-if="menu==26">
        <comision :ruta="ruta"></comision>
    </template>

    <template v-if="menu==27">
        <talonario :ruta="ruta"></talonario>
    </template>

    <template v-if="menu==28">
        <informe-transacciones :ruta="ruta"></informe-transacciones>
    </template>

    <template v-if="menu==29">
        <informe-cxc :ruta="ruta"></informe-cxc>
    </template>

    <template v-if="menu==30">
        <h2>contenido del menu 30</h2>
    </template>

    <template v-if="menu==31">
        <h2>contenido del menu 31</h2>
    </template>

    <template v-if="menu==32">
        <h2>contenido del menu 32</h2>
    </template>

    <template v-if="menu==33">
        <kardex :ruta="ruta"></kardex>
    </template>

    <template v-if="menu==34">
        <estadistica-venta :ruta="ruta"></estadistica-venta>
    </template> 

    <template v-if="menu==35">
        <h2>contenido del menu 35 se agrego al final</h2>
    </template>

    <template v-if="menu==36">
        <admin-bonificacion :ruta="ruta"></admin-bonificacion>
    </template>
    <template v-if="menu==37">
        <h2>contenido del menu 37 FACTURAS VENCIDAS</h2>
    </template>
    <template v-if="menu==38">
        <pre-factura :ruta="ruta"></pre-factura>
    </template>
    <template v-if="menu==39">
        <cheque-minuta :ruta="ruta"></cheque-minuta>
    </template>
@endsection