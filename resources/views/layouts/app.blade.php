<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Sistema') }}</title>

    <!-- Scripts 
    <script src="js/app.js" defer></script>-->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles 
    <link href="css/all.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
    <div id="app">
       <!-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                </div>
            </div>
        </nav>-->

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <style>
        body{
            background-color: rgb(90, 169, 51);
            height: 100vh;
            background-image: url('./img/Herramientas-manuales.png'), url('./img/LOGO-EL-CARPINTERO.png');
            background-size: 35%, 13.8%;
            background-repeat: no-repeat;
            background-position: left bottom, center top;
        }
        .card{
            margin-top: 30%
        }
        .card-header{
            background-color: rgb(82, 159, 47);
            /*background: url('/img/madrea.jpg');
            background-repeat: no-repeat;
            background-size: cover;*/
            height: 8em !important;
            position: relative;
        }
        .card-body{
            height: 30vh;
        }
        .card-header > h4{
            color: white;
            margin-top: 6%;
            font-weight: bold;
        }
        .mb-0 > .col-md-8{
            margin: auto;
        }
        label{
            color: grey;
        }
        input[type=text]{
            border: 0px;
            border-bottom: 1px solid grey;
            border-radius: 0;
            width: 100%;
            margin-top: 4%;
        }
        input[type=text]:hover{
            border-bottom: 1.5px solid green;
        }
        input[type=password]{
            border: 0px;
            border-bottom: 1px solid grey;
            border-radius: 0;
            width: 100%;
            margin-top: 4%;
        }
        input[type=password]:hover{
            border-bottom: 1.5px solid green;
        }
    </style>
</body>
</html>
