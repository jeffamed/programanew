<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
        }
        @page {
            margin: 0cm 0cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 10vh;
        }
        main{
            position: relative;
            top:125px;
            font-size: 12px 
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            /*margin-top: 25px;*/
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
            margin-top: 25px;
            border-spacing: 0;
            border-collapse: collapse;
        }
        th{
            border-top: 1px solid;
            border-bottom: 1px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
            <div class="titulo">
                <h3 class="bold text-center">HERRAJE</h3>
                <h4 class="text-center">Calendario de Cobro por Vendedor</h4>

            </div>
            <hr>
            <div class="f-left">
                <p>Fecha de Cobro: {{$del}}</p>
                @foreach ($zona as $det)
                    <p>Vendedor: {{$det->Vendedor}}</p>
                @endforeach
            </div>
                <div>
                    <p class="right">Fecha Impresión: {{$hoy}}</p>
                    <p class="right">Usuario: {{$user}}</p>
                </div>
            <table class="tabla">
                <thead>
                    <tr>
                        <th>Pedido</th>
                        <th>FRegistro</th>
                        <th>FVenc</th>
                        <th>DiasVenc</th>
                        <th>Monto C$</th>
                        <th>Saldo Vencido C$</th>
                        <th>Cobro Semanal C$</th>
                    </tr>
                </thead>
            </table>
    </header>
    <main>
        @foreach ($calendario as $d)
        <p style="margin-top: 10px" class="f-left"><b>Cliente: </b>{{$d->Cliente}}</p>
        <p class="right"><b>Mercado: </b>{{ $d->Zona }}      <b>Teléfono: </b>{{$d->Telefono1}}</p>
        <p><b>Dirección: </b>{{$d->Direccion}}</p>
            <table class="tabla">
                <tbody>
                    <tr>
                        <td class="center" style="width: 10%">{{ $d->idFactura }}</td>
                        <td class="center" style="width: 18%">{{ $d->FRegistro }}</td>
                        <td class="center" style="width: 18%">{{$d->FVencimiento }}</td>
                        <td class="center" style="width: 10%">{{ $d->DiasVencidos }}</td>
                        <td class="center" style="width: 20%">C$ {{ round(floatval($d->Total),2) }}</td>
                        <td class="center" style="width: 20%">C$ {{ round(floatval($d->SaldoRestante),2) }}</td>
                        <td class="center" style="width: 25%">C$ {{ round(floatval($d->CobroSemanal),2) }}</td>
                    </tr>
                    <tr>
                        <td class="center" style="width: 10%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 18%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 18%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 10%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 20%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 20%; height: 25px; border: 1px solid"></td>
                        <td class="center" style="width: 25%; height: 25px; border: 1px solid"></td>
                    </tr>
                </tbody>
            </table>
            @endforeach
            <table class="tabla">
                <tfoot>
                    <tr>
                        @foreach ($zona as $det)
                            <td class="right" style="width: 56%">Total por Zona ({{ $det->Nombre }}):</td>
                            <td class="text-center" style="width: 20% ">C$ {{ round(floatval($det->TotalZona1),2) }}</td>
                            <td class="text-center" style="width: 20% ">C$ {{ round(floatval($det->TotalZona2),2) }}</td>
                            <td class="text-center" style="width: 25% ">C$ {{ round(floatval($det->TotalZona2),2) }}</td>
                        @endforeach
                    </tr>
                    @foreach ($cliente as $det)
                    <tr>
                        <td class="right" style="width: 56%">Total de {{$det->Nombre}}:</td>
                            <td class="text-center" style="width: 20% ">C$ {{ round(floatval($det->TotalCliente1),2) }}</td>
                            <td class="text-center" style="width: 20% ">C$ {{ round(floatval($det->TotalCliente2),2) }}</td>
                            <td class="text-center" style="width: 25% ">C$ {{ round(floatval($det->TotalCliente2),2) }}</td>
                        </tr>
                    @endforeach
                </tfoot>
            </table>
    </main>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 765, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>