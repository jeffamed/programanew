<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
        }
        @page {
            margin: 0cm 0cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 10vh;
        }
        main{
            position: relative;
            top:125px;
            font-size: 12px 
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
            border-spacing: 0;
            border-collapse: collapse;
            margin-top: 10px;
        }
        tbody{
            border-bottom: 1px solid;
        }
        th{
            border-top: 1px solid;
            border-bottom: 1px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
            <div class="titulo">
                <h3 class="bold text-center">HERRAJE</h3>
                @foreach ($total as $det)
                    <h4 class="text-center">Listado de Cheque Cobrado</h4>
                    <h4 class="text-center"><b>{{$det->Banco}}</b></h4>
                @endforeach
            </div>
            <hr>
            <p class="f-left"></p>
                <div>
                    <p class="right">Fecha Impresión: {{$hoy}}</p>
                    <p class="right">Usuario: {{$user}}</p>
                </div>
            <hr>
    </header>
    <main>
        <h3>Detalle de los Cheques</h3>
        <table class="tabla" style="margin-top: 10px">
            <thead>
                <tr>
                    <th >FRegistro</th>
                    <th>Referencia</th>
                    <th>#Cheque</th>
                    <th>Beneficiario</th>
                    <th>Concepto</th>
                    <th>Estado</th>
                    <th>Vendedor</th>
                    <th>Monto C$</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cheque as $d)
                    <tr>
                        <td class="center">{{ $d->Fecha }}</td>
                        <td class="center">{{ $d->idCheque }}</td>
                        <td class="center">{{$d->NumCheque}}</td>
                        <td class="center">{{$d->Beneficiario }}</td>
                        <td class="center">{{$d->Nota }}</td>
                        <td class="center">{{ $d->Estado }}</td>
                        <td class="center">{{ $d->Vendedor }}</td>
                        <td class="center">{{ round(floatval($d->Monto),2) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        <div class="f-left">
            @foreach ($total as $det)
                <p>Total Cheque: {{ $det->TotalCheque }}</p>
            @endforeach
        </div>
        <div class="right" style="margin-right: 4%">
            @foreach ($total as $det)
                <p>Total Monto: C$ {{round(floatval($det->TotalMonto),2)}}</p>
            @endforeach
        </div>
    </main>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 765, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>