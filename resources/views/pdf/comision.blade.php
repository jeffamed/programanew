<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Comision</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
        }
        @page {
            margin: 0cm 0cm;
        }
        /*body {
            margin: 3cm 2cm 2cm;
        }*/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 10vh;
            /*background-color: #2a0927;
            color: white;*/
        }
        main{
            position: relative;
            top:200px
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            /*margin-top: 25px;*/
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
           /* border: 1px solid;*/
            border-spacing: 0;
            border-collapse: collapse;
        }
        tbody{
            border-bottom: 1px solid;
        }
        th{
            border-top: 1px solid;
            border-bottom: 1px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }

        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
            <div class="titulo">
                <h3 class="bold text-center" style="margin-top:20px">HERRAJE</h3>
                <h4 class="text-center">Calculo de Comisiones</h4>
            </div>
            <hr>
                <div>
                    <p class="right">Fecha Impresión: {{$hoy}}</p>
                    <p class="right">Usuario: Lorem Ipsum</p>
                </div>
            <hr>
            <div class="f-left">
                <p>Hoja de Comisión: {{$total}}</p>
                <p>Fecha de Planilla: {{$fecha}}</p>
                <p>Vendedor: {{$detalle1->Nombre}}</p>
                <p style="font-weight: bold">Observación: SIN GUARDAR</p>
            </div>
            <div class="right">
                <p>Fecha Inicio: {{$primero}}</p>
                <p>Fecha Fin: {{$ultimo}}</p>
            </div>
    </header>
    <main>
        <table class="tabla">
            <thead>
                <tr>
                    <th>No.Factura</th>
                    <th>F.Factura</th>
                    <th>F.Vencimiento</th>
                    <th>F.Cancelado</th>
                    <th>Rec/Nic</th>
                    <th>Cliente</th>
                    <th>Total Venta</th>
                    <th>Comisión</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($comision as $d)
                    <tr>
                        <td class="center">{{ $d->idFactura }}</td>
                        <td class="center">{{ $d->FechaFactura }}</td>
                        <td class="center">{{ $d->FechaVencimiento }}</td>
                        <td class="center">{{ $d->FechaCancelacion }}</td>
                        <td class="center">{{ $d->Recibo }}</td>
                        <td class="center">{{ $d->Cliente }}</td>
                        <td class="center">{{ round(floatval($d->TotalDeuda),2) }}</td>
                        <td class="center">{{ round(floatval($d->TotalComision),2) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="f-left">
            @foreach ($totalfact as $t)
                <p>Total Facturas: {{ $t->totalFactura }}</p>
            @endforeach    
        </div>
        <div class="right" style="margin-right: 24px">
            @foreach ($detalle as $d)
             <p>Total Comisión: C$ {{round(floatval($d->Total),2)}}</p>
           @endforeach
        </div>
    </main>
    <footer>
    </footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 765, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>