<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
            margin-bottom: 5px;
        }
        @page {
            margin: 0cm 0cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: -22cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        main{
            position: relative;
            top:125px
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
            border-spacing: 0;
            border-collapse: collapse;
        }
        tbody{
            border-bottom: 0px solid;
        }
        th{
            border-top: 0px solid;
            border-bottom: 0px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
        <div class="titulo">
            <h3 class="bold text-center">HERRAJE</h3>
            <h4 class="text-center">LISTADO DE DEVOLUCIONES DE CLIENTES</h4>
        </div>
        <hr>
        <p class="f-left">Del {{$del}} al {{$al}}</p>
        <div>
            <p class="right"><b>Fecha Impresión:</b> {{$hoy}}</p>
            <p class="right"><b>Usuario:</b> {{$user}}</p>
        </div>
        <hr>
    </header>
    <main style="margin-top: -20px">
        @foreach ($total as $t)
            <table style="width: 100%; border-spacing: 0; border-collapse: collapse" cellpadding="0" cellspacing="0">
                <tr>
                    <td ><p><b>Numero Documento:</b> {{$t->idDevoluciones}}</p></td>
                    <td ><p><b>Fecha: </b> {{$t->Fecha}}</p></td>
                </tr>
                <tr>
                    <td><p><b>Cliente:</b> {{$t->Codigo}} - {{$t->Nombre}}</p></td>
                    <td><p><b>Factura: </b> {{$t->idFactura}}</p></td>
                </tr>
                <tr>
                    <td><p><b>Observaciones:</b> {{$t->Comentario}}</p></td>
                    <td><p><b>Tipo: </b> {{$t->TipoCompra}}</p></td>
                </tr>
            </table>

            <table style="width: 100%">
                <thead style="width: 100%">
                    <tr>
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ventas as $v)
                        @if ($t->idDevoluciones == $v->idDevoluciones)
                            <tr>
                                <td>{{$v->Cod_Producto}}</td>
                                <td>{{$v->Nombre}}</td>
                                <td class="text-center">{{$v->Cantidad}}</td>
                                <td class="text-center">{{$v->Precio}}</td>
                                <td class="text-center">{{$v->Cantidad * $v->Precio}}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>

            <p class="right" style="margin-right: 40px"><b>SubTotal: </b>{{$t->Total}}</p>
            <p class="right" style="margin-right: 40px; padding-right: 25px"><b>Descuento: </b>0</p>
            <p class="right" style="margin-right: 40px; padding-right: 25px"><b>Impuesto: </b>0</p>
            <p class="right" style="margin-right: 40px; padding-right: 25px"><b>Retencion: </b>0</p>
            <p class="right" style="margin-right: 40px"><b>Total Neto: </b>{{$t->Total}}</p>
            <p>-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</p>
            
        @endforeach
    </main>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 765, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>