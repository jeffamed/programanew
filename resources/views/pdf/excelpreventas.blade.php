<table>
    <thead>
        <tr>
            <th colspan="7" style="font-weight: bold; font-size: 15px; text-align: center">MARNOR</th>
        </tr>
        <tr>
            <th colspan="7" style="font-weight: bold; font-size: 12px; text-align: center; border-bottom: 5px solid black">PRODUCTOS APARTADOS POR PREFACTURAS</th>
        </tr>
        <tr>
            <th colspan="4">Del: {{ $finicio }} al {{ $ffin }}</th>
            <th colspan="3" style="text-align: right">F.Impresion: {{ date("d") }} / {{ date("m") }} / {{ date("Y") }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: right">Usuario: {{ $user }}</th>
        </tr>
    </thead>
</table>
@foreach ($total as $t)
    <table>
        <tbody>
            <tr>
                <td colspan="7"><b>{{$t->Cod_Producto}}-{{$t->Producto}}</b></td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th><b>Fecha</b></th>
                <th><b>Cliente</b></th>
                <th><b>Cantidad</b></th>
                <th><b>Precio</b></th>
            </tr>
        </thead>
        @foreach ($ventas as $v)
            @if ($t->Producto == $v->Producto)
                <tbody style="border: none">
                    <tr>
                        <td style="width: 15px;">{{$v->Fecha}}</td>
                        <td style="width: 70px;">Pref No.{{$v->idPrefactura}} - {{$v->NombreCliente}}</td>
                        <td style="text-align: center">{{$v->Cantidad}}</td>
                        <td style="text-align: center">{{$v->Precio}}</td>
                    </tr>
                </tbody>
            @endif
        @endforeach
        <tfoot>
            <tr>
                <td colspan="2" style="margin-left: 100px"><b>Total de {{$t->Cod_Producto}}-{{$t->Producto}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{$t->TotalCantidad}}</b></td>
            </tr>
        </tfoot>
    </table>
@endforeach