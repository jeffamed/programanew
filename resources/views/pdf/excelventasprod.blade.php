<table>
    <thead>
        <tr>
            <th colspan="7" style="font-weight: bold; font-size: 15px; text-align: center">ARTICULOS ESCOLARES MIRIAM</th>
        </tr>
        <tr>
            <th colspan="7" style="font-weight: bold; font-size: 12px; text-align: center; border-bottom: 5px solid black">VENTAS DE PRODUCTOS POR CLIENTE Y BODEGA</th>
        </tr>
        <tr>
            <th colspan="4">Del: {{ $finicio }} al {{ $ffin }}</th>
            <th colspan="3" style="text-align: right">F.Impresion: {{ date("d") }} / {{ date("m") }} / {{ date("Y") }}</th>
        </tr>
        <tr>
            <th colspan="7" style="text-align: right">Usuario: {{ $user }}</th>
        </tr>
    </thead>
</table>
@foreach ($total as $t)
    <table>
        <tbody>
            <tr>
                <td colspan="7"><b>{{$t->Codigo}}-{{$t->Nombre}}</b></td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <th><b>Codigo</b></th>
                <th><b>Nombre</b></th>
                <th><b>Cantidad</b></th>
                <th><b>Costos</b></th>
                <th><b>Ventas</b></th>
                <th><b>Utilidad</b></th>
                <th><b>%Utilidad</b></th>
            </tr>
        </thead>
        @foreach ($ventas as $v)
            @if ($t->Nombre == $v->Nombre)
                <tbody style="border: none">
                    <tr>
                        <td style="width: 15px;">{{$v->Cod_Producto}}</td>
                        <td style="width: 70px;">{{$v->Producto}}</td>
                        <td style="text-align: center">{{$v->Cantidad}}</td>
                        <td style="text-align: center">{{$v->Costo}}</td>
                        <td style="text-align: center">{{$v->Precio}}</td>
                        <td style="text-align: center">{{$v->Utilidad}}</td>
                        <td style="text-align: center">{{$v->PorcUtilidad}}</td>
                    </tr>
                </tbody>
            @endif
        @endforeach
        <tfoot>
            <tr>
                <td colspan="2" style="margin-left: 100px"><b>Total por {{$t->Codigo}}-{{$t->Nombre}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{$t->Cantidad}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{round($t->Costo,2)}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{round($t->Precio,2)}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{round($t->Utilidad,2)}}</b></td>
                <td style="border-top: 1px solid black; text-align: center"><b>{{round($t->PorcUtilidad,2)}}</b></td>
            </tr>
        </tfoot>
    </table>
@endforeach
<table>
    <tbody>
        @foreach ($final as $f)
            <tr>
                <td colspan="2" style="text-align: right"><b>GRAN TOTAL</b></td>
                <td style="border-top: 2px double black"><b>{{$f->Cantidad}}</b></td>
                <td style="border-top: 2px double black"><b>{{round($f->Costo,2)}}</b></td>
                <td style="border-top: 2px double black"><b>{{round($f->Precio,2)}}</b></td>
                <td style="border-top: 2px double black"><b>{{round($f->Utilidad,2)}}</b></td>
                <td style="border-top: 2px double black"><b>{{round($f->PorcUtilidad,2)}}%</b></td>
            </tr>
        @endforeach
    </tbody>
</table>