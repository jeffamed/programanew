<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Factura Miniatura</title>
    <style>
        *{
            margin: 0px 1px;
            padding: 0;
        }
        body{
            font-size: 12px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        .contenido{
            width: 100%;
        }
        .factura1, .factura2{
            width: 49%;
        }
        .factura2{
            display: inline-block;
            margin-left: 50%;
            margin-top: -120%;
        }
        .titulo{
            font-size: 18px;
            font-weight: 100;
            text-align: center;
        }
        .dato1, .dato2, .dato3, .dato4{
            display: inline;
        }
        .datos1 > .item1{
            width: 33%;
        }
        .item > p{
            display: inline;
        }
        .item1 > label{
            padding-left: 15%;
        }
        .dato2 > .item2{
            width: 90%;
        }
        .dato3 > .item3{
            width: 100%;
        }
        .item3 > .lblfecha{
            padding-left: 43%;
        }
        .item3 > .lbltelefono{
            padding-left: 25%;
        }
        .item3 > .lblDescuento{
            padding-left: 28%;
        }
        .tabla{
        width: 95%;
        border-spacing: 0;
        padding: 0;
        margin: 0 auto;
        }
        th,td{
            text-align: left;
        /* border: 1px solid;*/
            border-spacing: 0;
            border-collapse: collapse;
        }
        tbody{
            border-bottom: 1px solid;
        }
        th{
            border-top: 1px solid;
            border-bottom: 1px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .m-top{
            margin-top: -5px;
        }
    </style>
</head>
<body>
    <div class="contenido">
        <div class="factura1">
            <div class="encabezado">
                @foreach ($factura as $f)
                <div class="titulo">
                    <h3>ORDEN DE PEDIDO</h3>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Contado___</p>
                        <p>Credito___</p>
                        <label>Plazo:</label>
                        <p>{{$f->PlazoPago}} dias</p>
                        <label>No. Orden:</label>
                       <p>{{$f->idFactura}}</p>
                    </div>
                </div>
                <div class="dato2">
                    <div class="item item2 m-top">
                        <label>Cliente:</label>
                    <p>{{$f->CodCliente}} - {{$f->Cliente}} <span>{{$notas}}</span></p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Teléfono:</label>
                        <p>+505 {{$f->Telefono1}}</p>
                        <label class="lblfecha">Fecha:</label>
                        <p>{{$f->Fecha}}</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Vendedor:</label>
                        <p>{{$f->Vendedor}}</p>
                        <label class="lbltelefono">Teléfono:</label>
                        <p>+505 {{$f->Telefono}}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="contenido">
                <div class="tbl">
                    <table class="tabla">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Pres</th>
                                <th>Medida</th>
                                <th>P.Unit</th>
                                <th>Unidad</th>
                                <th>SubTotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detalle as $d)
                                <tr>
                                    <td style="width: 51%">{{$d->Producto}}</td>
                                    <td class="center">{{$d->Presentacion}}</td>
                                    <td class="center">{{$d->medida}}</td>
                                    <td class="center">{{round(floatval($d->Precio),2)}}</td>
                                    <td class="center">{{$d->Cantidad}}</td>
                                    <td class="center">{{round(floatval($d->SubTotal),2)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @foreach ($total as $t)
                                <tr>
                                    <td colspan="4" style="text-align: right; padding-right: 25px">Suma Total</td>
                                    <td class="center">{{$t->TotalCantidad}}</td>
                                    <td class="center">C$ {{$t->TotalPrecio}}</td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <label>Bultos:</label>
                        <p>________________</p>
                        <label class="lblDescuento">Descuento por Bonificación:</label>
                        <p>0.0</p>
                    </div>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Empaquetado por</p>
                        <p style="margin-left: 20%">Recibi por</p>
                        <p style="margin-left: 25%">Entrega Conforme</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <h2 style="width: 25%; height: 50px; float: left">ORIGINAL</h2>
                        <div style="width: 70%; margin-left: 27%; text-align: justify">
                            <p >"El deudor se obliga a pagar al acreedor el diferencial cambiario en la misma proporción en que se devalúe el córdoba con respecto al dólar norteamericano"</p>
                        </div>
                    </div>
                </div>
                <div class="datos">
                    @foreach ($factura as $f)
                        <div  class="item-datos" style="width: 100%">
                            <p style="text-align: center">{{$f->Impresion}}</p>
                        </div>
                    @endforeach
                </div>
                <hr>
                @foreach ($factura as $f)
                    <div class="datos" style="text-align: left;">
                        <p>Región: {{$f->Zona}}</p>
                        <p>Mercado: {{$f->Zona}}</p>
                        <p>Dirección: {{$f->Direccion}}</p>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="factura2">
            <div class="encabezado">
                @foreach ($factura as $f)
                <div class="titulo">
                    <h3>ORDEN DE PEDIDO</h3>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Contado___</p>
                        <p>Credito___</p>
                        <label>Plazo:</label>
                        <p>{{$f->PlazoPago}} dias</p>
                        <label>No. Orden:</label>
                       <p>{{$f->idFactura}}</p>
                    </div>
                </div>
                <div class="dato2">
                    <div class="item item2 m-top">
                        <label>Cliente:</label>
                        <p>{{$f->CodCliente}} - {{$f->Cliente}} <span>{{$notas}}</span></p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Teléfono:</label>
                        <p>+505 {{$f->Telefono1}}</p>
                        <label class="lblfecha">Fecha:</label>
                        <p>{{$f->Fecha}}</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Vendedor:</label>
                        <p>{{$f->Vendedor}}</p>
                        <label class="lbltelefono">Teléfono:</label>
                        <p>+505 {{$f->Telefono}}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="contenido">
                <div class="tbl">
                    <table class="tabla">
                        <thead>
                            <tr>
                                <th style="width: 51%">Producto</th>
                                <th>Pres</th>
                                <th>Medida</th>
                                <th>P.Unit</th>
                                <th>Unidad</th>
                                <th>SubTotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detalle as $d)
                                <tr>
                                    <td>{{$d->Producto}}</td>
                                    <td class="center">{{$d->Presentacion}}</td>
                                    <td class="center">{{$d->medida}}</td>
                                    <td class="center">{{round(floatval($d->Precio),2)}}</td>
                                    <td class="center">{{$d->Cantidad}}</td>
                                    <td class="center">{{round(floatval($d->SubTotal),2)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @foreach ($total as $t)
                                <tr>
                                    <td colspan="4" style="text-align: right; padding-right: 25px">Suma Total</td>
                                    <td class="center">{{$t->TotalCantidad}}</td>
                                    <td class="center">C$ {{$t->TotalPrecio}}</td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <label>Bultos:</label>
                        <p>________________</p>
                        <label class="lblDescuento">Descuento por Bonificación:</label>
                        <p>0.0</p>
                    </div>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Empaquetado por</p>
                        <p style="margin-left: 20%">Recibi por</p>
                        <p style="margin-left: 25%">Entrega Conforme</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <h2 style="width: 25%; height: 50px; float: left">COPIA 1</h2>
                        <div style="width: 70%; margin-left: 27%; text-align: justify">
                            <p >"El deudor se obliga a pagar al acreedor el diferencial cambiario en la misma proporción en que se devalúe el córdoba con respecto al dólar norteamericano"</p>
                        </div>
                    </div>
                </div>
                <div class="datos">
                    @foreach ($factura as $f)
                        <div  class="item-datos" style="width: 100%">
                            <p style="text-align: center">{{$f->Impresion}}</p>
                        </div>
                    @endforeach
                </div>
                <hr>
                @foreach ($factura as $f)
                    <div class="datos" style="text-align: left;">
                        <p>Región: {{$f->Zona}}</p>
                        <p>Mercado: {{$f->Zona}}</p>
                        <p>Dirección: {{$f->Direccion}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div style="border-left: 1px dashed black; height: 100vh; position: fixed; margin-left: 49.5%"></div>
    <div style="page-break-after:always;"></div> 
    <!-- Salto de Pagina -->
    <div class="contenido">
        <div class="factura1">
            <div class="encabezado">
                @foreach ($factura as $f)
                <div class="titulo">
                    <h3>ORDEN DE PEDIDO</h3>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Contado___</p>
                        <p>Credito___</p>
                        <label>Plazo:</label>
                        <p>{{$f->PlazoPago}} dias</p>
                        <label>No. Orden:</label>
                       <p>{{$f->idFactura}}</p>
                    </div>
                </div>
                <div class="dato2">
                    <div class="item item2 m-top">
                        <label>Cliente:</label>
                        <p>{{$f->CodCliente}} - {{$f->Cliente}} <span>{{$notas}}</span> </p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Teléfono:</label>
                        <p>+505 {{$f->Telefono1}}</p>
                        <label class="lblfecha">Fecha:</label>
                        <p>{{$f->Fecha}}</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Vendedor:</label>
                        <p>{{$f->Vendedor}}</p>
                        <label class="lbltelefono">Teléfono:</label>
                        <p>+505 {{$f->Telefono}}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="contenido">
                <div class="tbl">
                    <table class="tabla">
                        <thead>
                            <tr>
                                <th>Producto</th>
                                <th>Pres</th>
                                <th>Medida</th>
                                <th>P.Unit</th>
                                <th>Unidad</th>
                                <th>SubTotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detalle as $d)
                                <tr>
                                    <td style="width: 51%">{{$d->Producto}}</td>
                                    <td class="center">{{$d->Presentacion}}</td>
                                    <td class="center">{{$d->medida}}</td>
                                    <td class="center">{{round(floatval($d->Precio),2)}}</td>
                                    <td class="center">{{$d->Cantidad}}</td>
                                    <td class="center">{{round(floatval($d->SubTotal),2)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @foreach ($total as $t)
                                <tr>
                                    <td colspan="4" style="text-align: right; padding-right: 25px">Suma Total</td>
                                    <td class="center">{{$t->TotalCantidad}}</td>
                                    <td class="center">C$ {{round(floatval($t->TotalPrecio),2)}}</td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <label>Bultos:</label>
                        <p>________________</p>
                        <label class="lblDescuento">Descuento por Bonificación:</label>
                        <p>0.0</p>
                    </div>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Empaquetado por</p>
                        <p style="margin-left: 20%">Recibi por</p>
                        <p style="margin-left: 25%">Entrega Conforme</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <h2 style="width: 25%; height: 50px; float: left">COPIA 2</h2>
                        <div style="width: 70%; margin-left: 27%; text-align: justify">
                            <p >"El deudor se obliga a pagar al acreedor el diferencial cambiario en la misma proporción en que se devalúe el córdoba con respecto al dólar norteamericano"<b>REVISE SU MERCANCIA, NO ACEPTAMOS DEVOLUCIONES</b></p>
                        </div>
                    </div>
                </div>
                <div class="datos">
                    @foreach ($factura as $f)
                        <div  class="item-datos" style="width: 100%">
                            <p style="text-align: center">{{$f->Impresion}}</p>
                        </div>
                    @endforeach
                </div>
                <hr>
                @foreach ($factura as $f)
                    <div class="datos" style="text-align: left;">
                        <p>Región: {{$f->Zona}}</p>
                        <p>Mercado: {{$f->Zona}}</p>
                        <p>Dirección: {{$f->Direccion}}</p>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="factura2">
            <div class="encabezado">
                @foreach ($factura as $f)
                <div class="titulo">
                    <h3>ORDEN DE PEDIDO</h3>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Contado___</p>
                        <p>Credito___</p>
                        <label>Plazo:</label>
                        <p>{{$f->PlazoPago}} dias</p>
                        <label>No. Orden:</label>
                       <p>{{$f->idFactura}}</p>
                    </div>
                </div>
                <div class="dato2">
                    <div class="item item2 m-top">
                        <label>Cliente:</label>
                        <p>{{$f->CodCliente}} - {{$f->Cliente}} <span>{{$notas}}</span></p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Teléfono:</label>
                        <p>+505 {{$f->Telefono1}}</p>
                        <label class="lblfecha">Fecha:</label>
                        <p>{{$f->Fecha}}</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3 m-top">
                        <label>Vendedor:</label>
                        <p>{{$f->Vendedor}}</p>
                        <label class="lbltelefono">Teléfono:</label>
                        <p>+505 {{$f->Telefono}}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="contenido">
                <div class="tbl">
                    <table class="tabla">
                        <thead>
                            <tr>
                                <th style="width: 51%">Producto</th>
                                <th>Pres</th>
                                <th>Medida</th>
                                <th>P.Unit</th>
                                <th>Unidad</th>
                                <th>SubTotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($detalle as $d)
                                <tr>
                                    <td>{{$d->Producto}}</td>
                                    <td class="center">{{$d->Presentacion}}</td>
                                    <td class="center">{{$d->medida}}</td>
                                    <td class="center">{{round(floatval($d->Precio),2)}}</td>
                                    <td class="center">{{$d->Cantidad}}</td>
                                    <td class="center">{{round(floatval($d->SubTotal),2)}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            @foreach ($total as $t)
                                <tr>
                                    <td colspan="4" style="text-align: right; padding-right: 25px">Suma Total</td>
                                    <td class="center">{{$t->TotalCantidad}}</td>
                                    <td class="center">C$ {{round(floatval($t->TotalPrecio),2)}}</td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <label>Bultos:</label>
                        <p>________________</p>
                        <label class="lblDescuento">Descuento por Bonificación:</label>
                        <p>0.0</p>
                    </div>
                </div>
                <div class="dato1">
                    <div class="item item1">
                        <p>Empaquetado por</p>
                        <p style="margin-left: 20%">Recibi por</p>
                        <p style="margin-left: 25%">Entrega Conforme</p>
                    </div>
                </div>
                <div class="dato3">
                    <div class="item item3">
                        <h2 style="width: 25%; height: 50px; float: left">COPIA 3</h2>
                        <div style="width: 70%; margin-left: 27%; text-align: justify">
                            <p >"El deudor se obliga a pagar al acreedor el diferencial cambiario en la misma proporción en que se devalúe el córdoba con respecto al dólar norteamericano"</p>
                        </div>
                    </div>
                </div>
                <div class="datos">
                    @foreach ($factura as $f)
                        <div  class="item-datos" style="width: 100%">
                            <p style="text-align: center">{{$f->Impresion}}</p>
                        </div>
                    @endforeach
                </div>
                <hr>
                @foreach ($factura as $f)
                    <div class="datos" style="text-align: left;">
                        <p>Región: {{$f->Zona}}</p>
                        <p>Mercado: {{$f->Zona}}</p>
                        <p>Dirección: {{$f->Direccion}}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</body>
</html>