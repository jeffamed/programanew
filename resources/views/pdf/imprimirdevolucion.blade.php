<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
        }
        @page {
            margin: 0cm 0cm;
        }
        /*body {
            margin: 3cm 2cm 2cm;
        }*/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: -23cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
            /*background-color: #2a0927;
            color: white;*/
        }
        main{
            position: relative;
            top:125px
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            /*margin-top: 25px;*/
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
           /* border: 1px solid;*/
            border-spacing: 0;
            border-collapse: collapse;
        }
        tbody{
            border-bottom: 1px solid;
        }
        th{
            border-top: 1px solid;
            border-bottom: 1px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
        <div class="titulo">
            <h3 class="bold text-center">HERRAJE</h3>
            <h4 class="text-center">Devolución a Clientes</h4>
        </div>
        <hr>
        <div>
            <p class="right">Fecha Impresión: {{$hoy}}</p>
            <p class="right">Usuario: {{$user}}</p>
        </div>
        <hr>
    </header>
    <main>
        <div class="contenido" style="margin-top: -30px">
            @foreach ($datos as $d)
                <table style="width: 100%">
                    <tbody style="border: none">
                        <tr>
                            <td style="width: 27.5%"><b>Fecha:</b> {{$d->fecha}}</td>
                            <td><b>Factura:</b> {{$d->idFactura}}</td>
                            <td><b>Tipo:</b> {{$d->TipoCompra}}</td>
                        </tr>
                    </tbody>
                </table>
                <table style="width: 50%">
                    <tbody style="border: none">
                        <tr >
                            <td><b>Numero Documento:</b></td>
                            <td>{{$d->idDevoluciones}}</td>
                        </tr>
                        <tr>
                            <td><b>Cliente:</b></td>
                            <td>{{$d->Codigo}} - {{$d->Cliente}}</td>
                        </tr>
                        <tr>
                            <td><b>Observaciones:</b></td>
                            <td>{{$d->Comentario}}</td>
                        </tr>
                    </tbody>
                </table>
            @endforeach 
        </div>
        <table class="tabla">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Precio Unit</th>
                    <th>Subtotal</th>
                </tr>
            </thead>
            <tbody style="border: none">
                @foreach ($detalle as $d)
                    <tr>
                        <td class="text-center">{{$d->Cod_Producto}}</td>
                        <td>{{$d->Nombre}}</td>
                        <td class="text-center">{{$d->Cantidad}}</td>
                        <td class="text-center">{{$d->Precio}}</td>
                        <td class="text-center">{{$d->Cantidad * $d->Precio}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </main>
    <footer>
        <div class="text-center" style="width: 100%; padding-left: 150px; border-bottom: 2px solid">
            <p class="f-left"><b>*** TOTALES ***</b></p>
            <div style="border-bottom: 5px double; margin-left: 500px;width: 150px"></div>
            <div style="width: 100%">
                <table style="margin-left: 500px">
                    <tbody style="border: none">
                        @foreach ($datos as $d)
                            <tr class="bold">
                                <td>Subtotal:</td>
                                <td class="right">{{$d->Total}}</td>
                            </tr>
                            <tr>
                                <td class="bold">Impuestos:</td>
                                <td class="right">0</td>
                            </tr>
                            <tr>
                                <td class="bold">Descuentos:</td>
                                <td class="right">0</td>
                            </tr>
                            <tr>
                                <td class="bold">Retención:</td>
                                <td class="right">0</td>
                            </tr>
                            <tr class="bold">
                                <td>Total General:</td>
                                <td class="right">{{$d->Total}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(270, 765, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>