<table>
    <tbody>
        <tr>
            <td colspan="3" style="font-weight: bold; font-size: 18px; text-align: center">
                HERRAJE
            </td>
        </tr>
        <tr>
            <td colspan="3" style="font-weight: bold; font-size: 12px; text-align: center">
                Listado de Producto ({{ date("d") }} / {{ date("M") }} / {{ date("Y") }})
            </td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
    <tr>
        <th style="width: 15px; font-weight: bold">Codigo Producto</th>
        <th style="width: 60px; font-weight: bold">Nombre</th>
        <th style="width: 15px; font-weight: bold; text-align: center">Cantidad Exist.</th>
    </tr>
    </thead>
    <tbody>
    @foreach($productos as $producto)
        <tr>
            <td>{{ $producto->Cod_Producto }}</td>
            <td>{{ $producto->Nombre }}</td>
            <td style="text-align: center">{{ $producto->Stock }}</td>
        </tr>
    @endforeach
    </tbody>
</table>