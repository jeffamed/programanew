<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hoja Principal</title>
</head>
<style>
    *{
        margin: 3px;
        padding: 0;
    }
    body{
        margin: 0;
        margin-left: 10px;
        font-size: 13px;
        font-family: Verdana, Geneva, Tahoma, sans-serif;
    }
    h5{
    font-weight: normal;
    font-size: 30px;
    font-family: Arial;
    text-transform: uppercase;
    }
    .d-flex{
        display: flex;
    }
    .bold{
        font-weight: bold
    }
    .text-center{
        text-align: center
    }
    .f-left{
        float: left;
    }
    .w-33{
        width: 32%;
    }
    .border{
        border: 1px solid;
    }
    .datos{
        /*margin-top: 25px;*/
        width: 100%;
        height: 3%;
        text-align: center;
    }
    .item-datos{
        width: 30%;
        display: inline-block;
    }
    .datos2{
        width: 100%;
        height: 3%;
        text-align: left;
    }
    .b-bottom{
        padding-bottom: -10px;
        margin-bottom: 10px;
        border-bottom: 2px solid;
    }
    main{
        width: 100%;
    }
    .tabla{
        width: 100%;
        border-spacing: 0;
        padding: 0;
        margin: 0;
    }
    th,td{
        text-align: left;
       /* border: 1px solid;*/
        border-spacing: 0;
        border-collapse: collapse;
    }
    tbody{
        border-bottom: 1px solid;
    }
    th{
        border-top: 1px solid;
        border-bottom: 1px solid;
        text-align: center;
    }
    tfoot tr td{
        padding-top: 10px;
    }
    .center{
        text-align: center
    }
</style>
<body>
    <header>
        @foreach ($factura as $f)
            <div class="titulo">
                <h5 class="bold text-center" style="margin-top:20px">ORDEN DE PEDIDO</h5>
            </div>
            <div class="datos" style="margin-top: -10px">
                <div class="item-datos">
                    <p class="f-left">Contado ___</p>
                    <p>Crédito ___</p>
                </div>
                <div class="item-datos">
                    <label class="f-left">Plazo:</label>
                    <p>{{$f->PlazoPago}} dias</p>
                </div>
                <div  class="item-datos">
                    <label class="f-left">No. orden</label>
                <p style="font-size: 16px">{{$f->idFactura}}</p>
                </div>
            </div>
            <div class="datos2" style="margin-top: -15px">
                <label class="f-left">Cliente:</label>
                <p>{{$f->CodCliente}} - {{$f->Cliente}} </p> 
            </div>
            <div class="datos" style="margin-top: -15px">
                <div class="item-datos" style="width: 49%; text-align: left">
                    <label class="f-left">Teléfono:   </label>
                     <p>+505 {{$f->Telefono1}}</p>
                </div>
                <div  class="item-datos" style="width: 49%">
                    <label class="f-left" style="margin-left: 20%">Fecha: </label>
                    <p style="padding: 0">{{$f->Fecha}}</p>
                </div>
            </div>
            <div class="datos b-bottom" style="margin-top: -15px">
                <div class="item-datos" style="width: 49%; text-align: left">
                    <label class="f-left">Vendedor:   </label>
                    <p>{{$f->Vendedor}}</p>
                </div>
                <div  class="item-datos" style="width: 49%; text-align: left">
                    <label class="f-left" style="float: left">Teléfono: </label>
                    <p style="padding: 0">+505 {{$f->Telefono}}</p>
                </div>
            </div>
        @endforeach
    </header>
    <main>
        <table class="tabla">
            <thead>
                <tr>
                    <th style="width: 40%">Producto</th>
                    <th>Pres</th>
                    <th>Medida</th>
                    <th>P.Unit</th>
                    <th>Unidades</th>
                    <th>SubTotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($detalle as $d)
                    <tr>
                        <td>{{$d->Producto}}</td>
                        <td class="center">{{$d->Presentacion}}</td>
                        <td class="center">{{$d->medida}}</td>
                        <td class="center">{{round(floatval($d->Precio),2)}}</td>
                        <td class="center">{{$d->Cantidad}}</td>
                        <td class="center">{{round(floatval($d->SubTotal),2)}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
                @foreach ($total as $t)
                    <tr>
                        <td colspan="4" style="text-align: right; padding-right: 25px">Suma Total</td>
                        <td class="center">{{$t->TotalCantidad}}</td>
                        <td class="center">C$ {{round(floatval($t->TotalPrecio),2)}}</td>
                    </tr>
                @endforeach
            </tfoot>
        </table>
        <div class="datos" style="margin-top: 0px">
            <div class="item-datos" style="width: 49%; text-align: left">
                <label class="f-left">Bultos:</label>
                <p>_____________</p>
            </div>
            <div  class="item-datos" style="width: 49%">
            <label class="f-left" style="margin-left: 10%; padding-right: 40px">Descuento por Bonificación:</label>
                <p style="padding: 0">0.0</p>
            </div>
        </div>
        <div class="datos" style="margin-top: 0px">
            <div class="item-datos">
                <p class="f-left" style="margin-right: 0; margin-left: 0; padding: 0">Empaquetado por</p>
            </div>
            <div class="item-datos">
                <label class="f-left" style="margin-right: 0; margin-left: -25px; padding: 0">Recibi Conforme</label>
            </div>
            <div  class="item-datos">
                <label class="f-left" style="margin-right: 0; margin-left: -25px;">Entrega Conforme</label>
            </div>
        </div>
        <div class="datos" style="margin-bottom: 50px; text-align: center">
            <div class="item-datos" sytle="width: 49%;">
                <h1>ORIGINAL</h1>
            </div>
            <div  class="item-datos" style="width: 49%; padding-left: 20%">
                <p style="padding: 0; text-align: justify">"El deuda se obliga a pagar al acreedor el diferencial cambiario en la misma proporción en que se devalúe el córdoba con respecto al dólar norteamericano"</p>
            </div>
        </div>
        <div class="datos" style="">
            @foreach ($factura as $f)
                <div  class="item-datos" style="width: 100%">
                    <p style="padding: 0">{{$f->Impresion}}</p>
                </div>
            @endforeach
        </div>
        <hr>
        @foreach ($factura as $f)
            <div class="datos" style="text-align: left;">
                <p>Región: {{$f->Zona}}</p>
                <p>Mercado: {{$f->Zona}}</p>
                <p>Dirección: {{$f->Direccion}}</p>
            </div>
        @endforeach
    </main>
</body>
</html>