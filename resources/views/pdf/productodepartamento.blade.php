<table>
    <thead>
        <tr>
            <th colspan="9" style="font-weight: bold; font-size: 15px; text-align: center">MARNOR</th>
        </tr>
        <tr>
            <th colspan="9" style="font-weight: bold; font-size: 12px; text-align: center; border-bottom: 5px solid black">LISTA DE PRODUCTO SEGUN DEPARTAMENTO "{{ $dpto }}"</th>
        </tr>
        <tr>
            <th colspan="9" style="text-align: right">F.Impresion: {{ date("d") }} / {{ date("m") }} / {{ date("Y") }}</th>
        </tr>
        <tr>
            <th colspan="9" style="text-align: right">Usuario: {{ $user }}</th>
        </tr>
    </thead>
</table>
    <table>
        <thead>
            <tr>
                <th><b>Codigo</b></th>
                <th><b>Nombre</b></th>
                <th style="text-align: center"><b>Existencia</b></th>
                <th style="text-align: center"><b>Unidad de Medida</b></th>
                <th style="text-align: center"><b>Existencia en Caja</b></th>
                <th style="text-align: center"><b>Precios C$</b></th>
                <th style="text-align: center"><b>Fecha Ingreso</b></th>
                <th style="text-align: center"><b>Cant. Ingresada</b></th>
                <th style="text-align: center"><b>Precio Compra</b></th>
            </tr>
        </thead>
        @foreach ($info as $v)
            <tbody style="border: none">
                <tr>
                    <td style="width: 15px;">{{$v->Cod_Producto}}</td>
                    <td style="width: 70px;">{{$v->Producto}}</td>
                    <td style="text-align: center; width: 15px">{{$v->Stock}}</td>
                    <td style="text-align: center; width: 20px">{{$v->um}}</td>
                    <td style="text-align: center; width: 20px">{{$v->ExistCaja}}</td>
                    <td style="text-align: center; width: 40px">{{$v->Precios}}</td>
                    <td style="text-align: center; width: 15px">{{$v->FechaCompra}}</td>
                    <td style="text-align: center; width: 15px">{{$v->Vino}}</td>
                    <td style="text-align: center; width: 15px">{{$v->PrecioCompra}}</td>
                </tr>
            </tbody>
        @endforeach
    </table>
