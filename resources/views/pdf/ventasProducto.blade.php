<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informe</title>
    <style>
        *{
            margin: 3px;
            padding: 0;
            margin-bottom: 5px;
        }
        @page {
            margin: 0cm 0cm;
        }
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        footer {
            position: fixed;
            bottom: -22cm;
            left: 0cm;
            right: 0cm;
            height: 20vh;
        }
        main{
            position: relative;
            top:125px
        }
        body{
            margin: 0;
            margin-left: 10px;
            font-size: 13px;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        h5{
        font-weight: normal;
        font-family: Arial;
        text-transform: uppercase;
        }
        .d-flex{
            display: flex;
        }
        .bold{
            font-weight: bold
        }
        .text-center{
            text-align: center
        }
        .f-left{
            float: left;
        }
        .w-33{
            width: 32%;
        }
        .border{
            border: 1px solid;
        }
        .datos{
            width: 100%;
            height: 3%;
            text-align: center;
        }
        .item-datos{
            width: 30%;
            display: inline-block;
        }
        .datos2{
            width: 100%;
            height: 3%;
            text-align: left;
        }
        .b-bottom{
            padding-bottom: -10px;
            margin-bottom: 10px;
            border-bottom: 2px solid;
        }
        main{
            width: 100%;
        }
        .tabla{
            width: 100%;
            border-spacing: 0;
            padding: 0;
            margin: 0;
        }
        th,td{
            text-align: left;
            border-spacing: 0;
            border-collapse: collapse;
        }
        tbody{
            border-bottom: 0px solid;
        }
        th{
            border-top: 0px solid;
            border-bottom: 0px solid;
            text-align: center;
        }
        tfoot tr td{
            padding-top: 10px;
        }
        .center{
            text-align: center
        }
        .right{
            text-align: right
        }
    </style>
</head>
<body>
    <!--- ORIGINAL -->
    <header>
        <div class="titulo">
            <h3 class="bold text-center">HERRAJE</h3>
            <h4 class="text-center">VENTAS DE PRODUCTOS POR CLIENTE Y BODEGA (DETALLE)</h4>
        </div>
        <hr>
        <p class="f-left">Del {{$del}} al {{$al}}</p>
        <div>
            <p class="right"><b>Fecha Impresión:</b> {{$hoy}}</p>
            <p class="right"><b>Usuario:</b> {{$user}}</p>
        </div>
        <hr>
    </header>
    <main style="margin-top: -30px">
        @foreach ($total as $t)
            <div class="contenido" style="margin-top: 10px">
                <table style="width: 50%">
                    <tbody style="border: none">
                        <tr>
                            <td><h4><u>{{$t->Codigo}}-{{$t->Nombre}}</u></h4></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table class="tabla">
                <thead>
                    <tr>
                        <th style="width: 10%">Codigo</th>
                        <th style="width: 40%">Nombre</th>
                        <th style="width: 10%">Cantidad</th>
                        <th style="width: 10%">Costos</th>
                        <th style="width: 10%">Ventas</th>
                        <th style="width: 10%">Utilidad</th>
                        <th style="width: 10%">% Utilidad</th>
                    </tr>
                </thead>
                @foreach ($ventas as $v)
                    @if ($t->Nombre == $v->Nombre)
                        <tbody style="border: none">
                            <tr>
                                <td class="text-center">{{$v->Cod_Producto}}</td>
                                <td>{{$v->Producto}}</td>
                                <td class="text-center">{{$v->Cantidad}}</td>
                                <td class="text-center">{{$v->Costo}}</td>
                                <td class="text-center">{{$v->Precio}}</td>
                                <td class="text-center">{{$v->Utilidad}}</td>
                                <td class="text-center">{{$v->PorcUtilidad}}</td>
                            </tr>
                        </tbody>
                    @endif
                @endforeach
                    <tfoot>
                        <tr>
                            <td colspan="2" style="margin-left: 100px"><b>Total por {{$t->Codigo}}-{{$t->Nombre}}</b></td>
                            <td style="border-top: 1px solid" class="text-center"><b>{{$t->Cantidad}}</b></td>
                            <td style="border-top: 1px solid" class="text-center"><b>{{round($t->Costo,2)}}</b></td>
                            <td style="border-top: 1px solid" class="text-center"><b>{{round($t->Precio,2)}}</b></td>
                            <td style="border-top: 1px solid" class="text-center"><b>{{round($t->Utilidad,2)}}</b></td>
                            <td style="border-top: 1px solid" class="text-center"><b>{{round($t->PorcUtilidad,2)}}</b></td>
                        </tr>
                    </tfoot>
                </table>
            @endforeach
        <!--Gran Total-->
        <table class="tabla">
            <tbody>
                @foreach ($final as $f)
                    <tr>
                        <td class="text-center" style="font-weight: bold; padding-left: 150px" colspan="2">GRAN TOTAL</td>
                        <td class="text-center" style="border-top: 3px double; width: 10%">{{$f->Cantidad}}</td>
                        <td class="text-center" style="border-top: 3px double; width: 10%">{{round($f->Costo,2)}}</td>
                        <td class="text-center" style="border-top: 3px double; width: 10%">{{round($f->Precio,2)}}</td>
                        <td class="text-center" style="border-top: 3px double; width: 10%">{{round($f->Utilidad,2)}}</td>
                        <td class="text-center" style="border-top: 3px double; width: 10%">{{round($f->PorcUtilidad,2)}}%</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </main>
    <footer>
        <div class="text-center" style="width: 100%; padding-left: 150px; border-bottom: 1px solid">
            <div style="width: 100%">
            </div>
        </div>
    </footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            $pdf->page_script('
                $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
                $pdf->text(360, 590, "Página $PAGE_NUM de $PAGE_COUNT", $font, 10);
            ');
        }
    </script>
</body>
</html>