    <!--    MENU VERTICAL O SIDEBAR -->
    <div class="sidebar" data-color="Dark" data-image="./img/LOGO-EL-CARPINTERO1.png" id="menusidebar">
        <!--
            Tip 1:Si deseo cambiar los colores del sidebar entrar al_sidebar-and-main-panel.scss
            Tip 2: la imagen entra mas grande mejor se ajusta al sidebar
        -->
    	<div class="sidebar-wrapper">
            <div class="logo">
                <a @click="menu=0" href="#" class="simple-text">
                    Sist. Prueba
                </a>
            </div>
            <ul class="nav">

                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuPanel" aria-expanded="false" aria-controls="MenuPanel" id="Panel">
                       <img src="./img/icons8_news_32.png" class="img-sidebar">
                        <p>Panel Principal</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up"></i>
                    </a>
                    <ul id="MenuPanel" class="collapse">
                        <div class="caja">
                            @can('factura.index')
                                <li @click="menu=1" class="submenu" id="menufactura"><a href="#"></a>Factura</li>
                            @endcan
                            @can('cotizacion.index')
                                <li @click="menu=2" class="submenu"><a href="#"></a>Cotización</li>
                            @endcan
                            @can('devolucion.index')
                                <li @click="menu=3" class="submenu"><a href="#"></a>Devoluciones</li>
                            @endcan
                            @can('cliente.index')
                                <li @click="menu=4" class="submenu"><a href="#"></a>Clientes</li>
                            @endcan
                            @can('prefactura.index')
                             <li @click="menu=38" class="submenu"><a href="#"></a>Prefacturas</li>
                            @endcan
                            @can('cambio.index')
                             <li @click="menu=35" class="submenu"><a href="#"></a>Cambio de Producto</li>
                            @endcan
                        </div>
                    </ul>
                </li>

                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuCatalogo" aria-expanded="false" aria-controls="MenuCatalogo" id="Catalogo">
                        <img src="./img/icons8_clipboard_32.png" class="img-sidebar">
                        <p>Catalogo</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down1"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up1"></i>
                    </a>
                    <ul id="MenuCatalogo" class="collapse">
                        <div class="caja">
                            @can('departamento.index')
                                <li @click="menu=5" class="submenu"><a href="#"></a>Departamentos</li>
                            @endcan
                            @can('clase.index')
                                <li @click="menu=6" class="submenu"><a href="#"></a>Clase de Producto</li>
                            @endcan
                            @can('marca.index')
                                <li @click="menu=7" class="submenu"><a href="#"></a>Marca</li>
                            @endcan
                            @can('unidad.index')
                                <li @click="menu=8" class="submenu"><a href="#"></a>Unidad de Medida</li>
                            @endcan
                            @can('presentacion.index')
                                <li @click="menu=9" class="submenu"><a href="#"></a>Presentación</li>
                            @endcan
                            @can('vendedor.index')
                                <li @click="menu=10" class="submenu"><a href="#"></a>Vendedores</li>
                            @endcan
                            @can('zona.index')
                                <li @click="menu=11" class="submenu"><a href="#"></a>Zona</li>
                            @endcan
                            @can('dolar.index')
                                <li @click="menu=12" class="submenu"><a href="#"></a>Tasa de Cambio</li>
                            @elsecan('yuan.index')
                                <li @click="menu=12" class="submenu"><a href="#"></a>Tasa de Cambio</li>
                            @endcan
                            
                        </div>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuInventario" aria-expanded="false" aria-controls="MenuInventario" id="Inventario">
                        <img src="./img/icons8_trolley_32.png" class="img-sidebar">
                        <p>Inventario</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down2"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up2"></i>
                    </a>
                    <ul id="MenuInventario" class="collapse">
                        <div class="caja">
                            @can('producto.index')
                                <li @click="menu=13" class="submenu"><a href="#"></a>Productos</li>
                            @endcan
                            @can('entrada.index')
                                <li @click="menu=14" class="submenu"><a href="#"></a>Entrada</li>
                            @endcan
                            @can('salida.index')
                                <li @click="menu=15" class="submenu"><a href="#"></a>Salida</li>
                            @endcan
                            @can('compra.index')
                                <li @click="menu=16" class="submenu"><a href="#"></a>Compra</li>
                            @endcan
                            @can('proveedor.index')
                                <li @click="menu=17" class="submenu"><a href="#"></a>Proveedor</li>
                            @endcan
                            @can('pedido.index')
                            <li @click="menu=18" class="submenu"><a href="#"></a>Pedidos</li>
                            @endcan
                            @can('admon.index')
                                <li @click="menu=36" class="submenu"><a href="#"></a>Admon Bonificación</li>
                            @endcan
                        </div>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuCxC" aria-expanded="false" aria-controls="MenuCxC" id="CxC">
                        <img src="./img/icons8_cash_32.png" class="img-sidebar">
                        <p>Cuentas por Cobrar</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down3"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up3"></i>
                    </a>
                    <ul id="MenuCxC" class="collapse">
                        <div class="caja">
                            @can('recibo.index')
                                <li @click="menu=19"  class="submenu"><a href="#"></a>Recibo</li>
                            @endcan
                            @can('chequeminuta.index')
                                <li @click="menu=39"  class="submenu"><a href="#"></a>Cheque-Minuta</li>
                            @endcan
                            @can('cheque.index')
                                <li @click="menu=20"  class="submenu"><a href="#"></a>Cheque</li>
                            @endcan
                            @can('nota.index')
                                <li @click="menu=21"  class="submenu"><a href="#"></a>Nota de Crédito</li>
                            @endcan
                            @can('notad.index')
                                <li @click="menu=22"  class="submenu"><a href="#"></a>Nota de Débito</li>
                            @endcan
                            @can('estadocuenta')
                                <li @click="menu=23"  class="submenu"><a href="#"></a>Estado de Cuentas</li>
                            @endcan
                            @can('banco.index')
                                <li @click="menu=24"  class="submenu"><a href="#"></a>Bancos</li>
                            @endcan
                        </div>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuUtilidades" aria-expanded="false" aria-controls="MenuUtilidades" id="Utilidades">
                        <img src="./img/icons8_conference_32.png" class="img-sidebar">
                        <p>Utilidades</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down4"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up4"></i>
                    </a>
                    <ul id="MenuUtilidades" class="collapse">
                        <div class="caja">
                            @can('user.index')
                                <li @click="menu=25" class="submenu"><a href="#"></a>Usuarios</li>
                            @endcan
                            @can('comision.index')
                                <li @click="menu=26" class="submenu"><a href="#"></a>Comisión</li>
                            @endcan
                            @can('talonario.index')
                                <li @click="menu=27" class="submenu"><a href="#"></a>Talonario</li>
                            @endcan
                        </div>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuInformes" aria-expanded="false" aria-controls="MenuInformes" id="Informes">
                        <img src="./img/icons8_report_file_32.png" class="img-sidebar">
                        <p>Informes</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down5"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up5"></i>
                    </a>
                    <ul id="MenuInformes" class="collapse">
                        <div class="caja">
                            @can('transacciones')
                                <li @click="menu=28" class="submenu"><a href="#"></a>Transacciones</li>
                            @endcan
                            @can('informecxc')
                                <li @click="menu=29" class="submenu"><a href="#"></a>Cuentas por Cobrar</li>
                            @endcan
                           <!-- <li @click="menu=30" class="submenu"><a href="#"></a>Control de Existencia</li>
                            <li @click="menu=31" class="submenu"><a href="#"></a>Estadistica de Venta</li>
                            <li @click="menu=32" class="submenu"><a href="#"></a>Catálogo de Producto</li>
                            <li @click="menu=37" class="submenu"><a href="#"></a>Facturas Vencidas</li>-->
                        </div>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#MenuConsultas" aria-expanded="false" aria-controls="MenuConsultas" id="Consultas">
                        <img src="./img/icons8_graph_32.png" class="img-sidebar">
                        <p>Consultas</p>
                        <i class="pe-7s-angle-down flecha" id="flecha-down6"></i>
                        <i class="pe-7s-angle-up flecha2" id="flecha-up6"></i>
                    </a>
                    <ul id="MenuConsultas" class="collapse">
                        <div class="caja">
                            @can('kardex')
                                <li @click="menu=33" class="submenu"><a href="#"></a>Kardex</li>
                            @endcan
                            @can('estadisticaventa')
                                <li @click="menu=34" class="submenu"><a href="#"></a>Estadistica de Venta</li>
                            @endcan
                        </div>
                    </ul>
                </li>
            
				<!--<li class="active-pro">
                    <a href="" class="position-fixed">
                        <i class="pe-7s-close"></i>
                        <p>Salir</p>
                    </a>
                </li> -->

            </ul>
    	</div>
    </div>
