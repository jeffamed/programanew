<!doctype html>
<html lang="es">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="./img/icons8_nailer_32.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Sist Prueba</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--   Archivo compilado     -->
    <link href="css/all.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>

</head>
<body>
    <div class="wrapper" id="app">
            
        <!--     SIDEBAR O MENU     -->
               @include('plantilla.sidebar')

        <!--         CONTENIDO PRINCIPAL     -->

        <div class="main-panel" id="mainpanel">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div id="btnToggle" class="toggle-btn">
                            <span>&#9776</span>
                        </div>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>                    
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <h5 style="color: white; margin-top: 20%"> {{ Auth::user()->name }}</h5>
                                <!--<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                       
                                    </div>
                                </a>-->
                                
                            </li>
                            <li>
                                <a id="logout" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Sesión') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                            <li class="separator hidden-lg"></li>
                        </ul>
                    </div>
                </div>
            </nav>

        <!--  CUERPO DEL SISTEMA FORM, VIEW ETC-->

                @yield('contenido')

        <!--FIN DEL CUERPO O CONTENIDO PRINCIPAL-->
        </div>   
    </div>
</body>
    <style>
        #logout:hover{
            color: red
        }
        
    </style>
	<!-- Compilacion completa de js -->
    <script src="./js/app.js"></script>
    <script src="./js/all.js"></script>
    <script src="/dist/vuejs-datatable.js" defer></script> 
    <script>
        @auth
          window.Permissions = {!! json_encode(Auth::user()->allPermissions, true) !!};
        @else
          window.Permissions = [];
        @endauth
    </script>

    <script type="text/javascript">
    
        function DisableTxt() {
            $("#txtCompany").prop( "disabled", true );
            $("#txtUser").prop( "disabled", true );
            $("#txtEmail").prop( "disabled", true );
            $("#txtName").prop( "disabled", true );
            $("#txtLastName").prop( "disabled", true );
        }
        function EnableTxt() {
            $("#txtCompany").prop( "disabled", false );
            $("#txtUser").prop( "disabled", false );
            $("#txtEmail").prop( "disabled", false );
            $("#txtName").prop( "disabled", false );
            $("#txtLastName").prop( "disabled", false );
        }
    	$(document).ready(function(){

            $('[data-toggle="tooltip"]').tooltip();

            DisableTxt();

            $("#btnToggle").click(()=>{
                $("#menusidebar").toggle('active');
                document.getElementById('mainpanel').classList.toggle('active');
            });

            $("#Panel").click(function() {
                $("#flecha-down").toggle();
            });
            $("#Panel").click(function() {
                $("#flecha-up").toggle();
            });
            $("#Catalogo").click(function() {
                $("#flecha-down1").toggle();
            });
            $("#Catalogo").click(function() {
                $("#flecha-up1").toggle();
            });
            $("#Inventario").click(function() {
                $("#flecha-down2").toggle();
            });
            $("#Inventario").click(function() {
                $("#flecha-up2").toggle();
            });
            $("#CxC").click(function() {
                $("#flecha-down3").toggle();
            });
            $("#CxC").click(function() {
                $("#flecha-up3").toggle();
            });
            $("#Utilidades").click(function() {
                $("#flecha-down4").toggle();
            });
            $("#Utilidades").click(function() {
                $("#flecha-up4").toggle();
            });
            $("#Informes").click(function() {
                $("#flecha-down5").toggle();
            });
            $("#Informes").click(function() {
                $("#flecha-up5").toggle();
            });
            $("#Consultas").click(function() {
                $("#flecha-down6").toggle();
            });
            $("#Consultas").click(function() {
                $("#flecha-up6").toggle();
            });
            $("#btnnew").click(function() {
                $("#btnAdd").toggle();
                $("#btnReset").toggle();
                $("#btnnew").toggle();
                EnableTxt();
            });
            $("#btnReset").click(function() {
                $("#btnAdd").toggle();
                $("#btnReset").toggle();
                $("#btnnew").toggle();
                DisableTxt();
            });

           

    	});
	</script>

</html>
