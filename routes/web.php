<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/main', function () {
    return view('content/contenido');
})->name('main');*/

Route::middleware(['guest'])->group(function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
});
Route::middleware(['auth'])->group(function () {

    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/', function () {
        return view('content/contenido');
    })->name('main')->where('path', '.*');
    
    //Ruta de bancos
    Route::get('/banco', 'BancoController@index')->name('banco.index')->middleware('permission:banco.index');
    Route::post('/banco/registrar', 'BancoController@store')->name('banco.store')->middleware('permission:banco.store');
    Route::put('/banco/actualizar', 'BancoController@update')->name('banco.update')->middleware('permission:banco.update');
    Route::put('/banco/active', 'BancoController@habilitar')->name('banco.active')->middleware('permission:banco.active');
    Route::put('/banco/inactive', 'BancoController@deshabilitar')->name('banco.inactive')->middleware('permission:banco.inactive');;
    Route::get('/banco/seleccionar', 'BancoController@ListarBancos')->name('banco.select');

    //Rutas de Marca
    Route::get('/marca','MarcaController@index')->name('marca.index')->middleware('permission:marca.index');
    Route::post('/marca/registrar', 'MarcaController@store')->name('marca.store')->middleware('permission:marca.store');
    Route::put('/marca/actualizar', 'MarcaController@update')->name('marca.update')->middleware('permission:marca.update');
    Route::put('/marca/active', 'MarcaController@habilitar')->name('marca.active')->middleware('permission:marca.active');
    Route::put('/marca/inactive', 'MarcaController@deshabilitar')->name('marca.inactive')->middleware('permission:marca.inactive');
    Route::get('/marca/seleccionar', 'MarcaController@SeleccionarMarca')->name('marca.select');

    //Rutas de Unidad de Medida
    Route::get('/unidad','UnidadMedidaController@index')->name('unidad.index')->middleware('permission:unidad.index');
    Route::post('/unidad/registrar', 'UnidadMedidaController@store')->name('unidad.store')->middleware('permission:unidad.store');
    Route::put('/unidad/actualizar', 'UnidadMedidaController@update')->name('unidad.update')->middleware('permission:unidad.update');
    Route::get('/unidad/seleccionar', 'UnidadMedidaController@SeleccionarUnidad')->name('unidad.select');

    //Rutas de Marca
    Route::get('/marca','MarcaController@index')->name('marca.index')->middleware('permission:marca.index');
    Route::post('/marca/registrar', 'MarcaController@store')->name('marca.store')->middleware('permission:marca.store');
    Route::put('/marca/actualizar', 'MarcaController@update')->name('marca.update')->middleware('permission:marca.update');
    Route::put('/marca/active', 'MarcaController@habilitar')->name('marca.active')->middleware('permission:marca.active');
    Route::put('/marca/inactive', 'MarcaController@deshabilitar')->name('marca.inactive')->middleware('permission:marca.inactive');
    Route::get('/marca/seleccionar', 'MarcaController@SeleccionarMarca')->name('marca.select');

    //Rutas de zonas
    Route::get('/zona','ZonaController@index')->name('zona.index')->middleware('permission:zona.index');
    Route::post('/zona/registrar', 'ZonaController@store')->name('zona.store')->middleware('permission:zona.store');
    Route::put('/zona/actualizar', 'ZonaController@update')->name('zona.update')->middleware('permission:zona.update');
    Route::get('/zona/seleccionar', 'ZonaController@SeleccionarZona')->name('zona.select');
    Route::get('/zona/all', 'ZonaController@SeleccionarAll')->name('zona.all');

    //Rutas de Clase de Producto o Tipo
    Route::get('/clase','ClaseProductoController@index')->name('clase.index')->middleware('permission:clase.index');
    Route::post('/clase/registrar', 'ClaseProductoController@store')->name('clase.store')->middleware('permission:clase.store');
    Route::put('/clase/actualizar', 'ClaseProductoController@update')->name('clase.update')->middleware('permission:clase.update');
    Route::get('/clase/seleccionar', 'ClaseProductoController@SeleccionarClase')->name('clase.select');
    Route::get('/clase/all', 'ClaseProductoController@allSelect')->name('clase.all');

    //Rutas de Presentacion del producto
    Route::get('/presentacion','PresentacionController@index')->name('presentacion.index')->middleware('permission:presentacion.index');
    Route::post('/presentacion/registrar', 'PresentacionController@store')->name('presentacion.store')->middleware('permission:presentacion.store');
    Route::put('/presentacion/actualizar', 'PresentacionController@update')->name('presentacion.update')->middleware('permission:presentacion.update');
    Route::get('/presentacion/seleccionar', 'PresentacionController@SeleccionarPresentacion')->name('presentacion.select');

    //Rutas de Departamentos
    Route::get('/departamento','DepartamentosController@index')->name('departamento.index')->middleware('permission:departamento.index');
    Route::post('/departamento/registrar', 'DepartamentosController@store')->name('departamento.store')->middleware('permission:departamento.store');
    Route::put('/departamento/actualizar', 'DepartamentosController@update')->name('departamento.update')->middleware('permission:departamento.update');
    Route::get('/departamento/seleccionar', 'DepartamentosController@SeleccionarDepartamentos')->name('departamento.select');
    Route::get('/departamento/all', 'DepartamentosController@SeleccionarAll')->name('departamento.all');

    //Rutas de Tasa de Cambio
    Route::get('/dolar','CambioDolarController@index')->name('dolar.index')->middleware('permission:dolar.index');
    Route::post('/dolar/registrar', 'CambioDolarController@store')->name('dolar.store')->middleware('permission:dolar.store');
    Route::put('/dolar/actualizar', 'CambioDolarController@update')->name('dolar.update')->middleware('permission:dolar.update');
    Route::get('/dolar/seleccionar','CambioDolarController@UltimaTasa')->name('dolar.select');
    Route::get('/dolar/verificar','CambioDolarController@UltimoRegistro')->name('dolar.last');

    //Rutas de Tasa de Cambio
    Route::get('/yuan','CambioYuanController@index')->name('yuan.index')->middleware('permission:yuan.index');
    Route::post('/yuan/registrar', 'CambioYuanController@store')->name('yuan.store')->middleware('permission:yuan.index');
    Route::put('/yuan/actualizar', 'CambioYuanController@update')->name('yuan.update')->middleware('permission:update.index');

    //Rutas de Vendedor
    Route::get('/vendedor','VendedoresController@index')->name('vendedor.index')->middleware('permission:vendedor.index');
    Route::post('/vendedor/registrar', 'VendedoresController@store')->name('vendedor.store')->middleware('permission:vendedor.store');
    Route::put('/vendedor/actualizar', 'VendedoresController@update')->name('vendedor.update')->middleware('permission:vendedor.update');
    Route::put('/vendedor/active', 'VendedoresController@habilitar')->name('vendedor.enable')->middleware('permission:vendedor.enable');
    Route::put('/vendedor/inactive', 'VendedoresController@deshabilitar')->name('vendedor.disable')->middleware('permission:vendedor.disable');
    Route::get('/vendedor/seleccionar', 'VendedoresController@SeleccionarVendedor')->name('vendedor.select');
    Route::get('/vendedor/all', 'VendedoresController@VendedorAll')->name('vendedor.all');
    Route::get('/vendedor/listar', 'VendedoresController@ListarVendedor');

    //Rutas de Cliente
    Route::get('/cliente','ClienteController@index')->name('cliente.index')->middleware('permission:cliente.index');
    Route::post('/cliente/registrar', 'ClienteController@store')->name('cliente.store')->middleware('permission:cliente.store');
    Route::put('/cliente/actualizar', 'ClienteController@update')->name('cliente.update')->middleware('permission:cliente.update');
    Route::put('/cliente/active', 'ClienteController@habilitar')->name('cliente.active')->middleware('permission:cliente.active');
    Route::put('/cliente/inactive', 'ClienteController@deshabilitar')->name('cliente.inactive')->middleware('permission:cliente.inactive');
    Route::get('/cliente/seleccionar', 'ClienteController@SeleccionarCliente')->name('cliente.select');
    Route::get('/cliente/factura', 'ClienteController@ClienteFactura')->name('cliente.invoice');
    Route::get('/cliente/cheque', 'ClienteController@ClienteCheque')->name('cliente.check'); //paycheck -> bank draft
    Route::get('/cliente/all', 'ClienteController@SeleccionarAll')->name('cliente.check'); //paycheck -> bank 
    Route::get('/cliente/factura', 'ClienteController@SelectClienteFact');
    Route::get('/cliente/nombre', 'ClienteController@BuscarNombre'); 
    Route::get('/cliente/nombrecliente', 'ClienteController@SeleccionarClienteNombre');
    Route::get('/cliente/deuda', 'ClienteController@ActualizarDeuda');  

    //Rutas de Proveedor
    Route::get('/proveedor','ProveedoresController@index')->name('proveedor.index')->middleware('permission:proveedor.index');
    Route::post('/proveedor/registrar', 'ProveedoresController@store')->name('proveedor.store')->middleware('permission:proveedor.store');
    Route::post('/proveedor/registrar1', 'ProveedoresController@store1')->name('proveedor.store1')->middleware('permission:proveedor.store1');
    Route::put('/proveedor/actualizar', 'ProveedoresController@update')->name('proveedor.update')->middleware('permission:proveedor.update');
    Route::put('/proveedor/actualizar1', 'ProveedoresController@update1')->name('proveedor.update1')->middleware('permission:proveedor.update1');
    Route::get('/proveedor/seleccionar', 'ProveedoresController@SeleccionarProveedor')->name('proveedor.select');

    //Rutas de Productos
    //Route::get('/producto','ProductosController@index')->name('producto.index')->middleware('permission:producto.index');
    Route::post('/producto','ProductosController@index')->name('producto.index')->middleware('permission:producto.index');
    Route::post('/producto/registrar', 'ProductosController@store')->name('producto.store')->middleware('permission:producto.store');
    Route::post('/producto/registrar1', 'ProductosController@store1')->name('producto.store1');
    Route::put('/producto/actualizar', 'ProductosController@update')->name('producto.update')->middleware('permission:producto.update');
    Route::put('/producto/actualizar1', 'ProductosController@update1')->name('producto.update1');
    Route::put('/producto/deshabilitar', 'ProductosController@deshabilitar')->name('producto.disable')->middleware('permission:producto.disable');
    Route::put('/producto/habilitar', 'ProductosController@habilitar')->name('producto.enable')->middleware('permission:producto.enable');
    //Route::get('/producto/seleccionar', 'ProductosController@SeleccionarProducto')->name('producto.select');
    Route::get('/producto/listadoExcel', 'ProductosController@ListadoProductoExcel')->name('producto.listadoexcel');
    Route::get('/producto/cantidadfinal', 'ProductosController@cantidad')->name('producto.cantidad');
    Route::get('/producto/all', 'ProductosController@SeleccionarAll')->name('producto.all');
    Route::post('/producto/seleccionar', 'ProductosController@SeleccionarProducto')->name('producto.select');
    Route::post('/producto/codigobarra', 'ProductosController@BuscarCodigo');
    Route::get('/producto/verificar', 'ProductosController@VerificarCodigo')->name('producto.codigo');

    //Rutas de Facturas
    Route::get('/factura','FacturaController@index')->name('factura.index')->middleware('permission:factura.index');
    Route::post('/factura/registrar', 'FacturaController@store')->name('factura.store')->middleware('permission:factura.store');
    Route::put('/factura/deshabilitar', 'FacturaController@deshabilitar')->name('factura.disable')->middleware('permission:factura.disable');
    Route::get('/detallefactura/seleccionar','FacturaController@DetalleFactura')->name('factura.select');
    Route::get('/factura/seleccionarfacturas','FacturaController@FacturasClienteD')->name('factura.selectinvoice');
    Route::get('/factura/seleccionarproductos','FacturaController@DetalleProductoClienteD')->name('factura.selectproduct');
    Route::get('/factura/clientes','FacturaController@FacturasxCobrar')->name('factura.customer');
    Route::get('/factura/vencida','FacturaController@FacturasVencidas')->name('factura.expire');
    
    // Rutas de Prefactura
    Route::get('/prefactura', 'PrefacturaController@index')->name('prefactura.index')->middleware('permission:prefactura.index');
    Route::post('/prefactura/invoce', 'PrefacturaController@invoce')->name('prefactura.invoce')->middleware('permission:prefactura.invoice');
    Route::put('/prefactura/deshabilitar', 'PrefacturaController@deshabilitar')->name('prefactura.dehabilitar')->middleware('permission:prefactura.delete');
    Route::put('/prefactura/actualizar','PrefacturaController@update')->name('prefactura.update');
    Route::get('/prefactura/datos', 'PrefacturaController@datos')->name('prefactura.data');
    Route::get('/prefactura/detalle', 'PrefacturaController@detalle')->name('prefactura.data');
    Route::put('/prefactura/estado', 'PrefacturaController@Estado')->name('prefactura.state');

    // Rutas de Devoluciones
    Route::get('/devolucion','DevolucionesController@index')->name('devolucion.index')->middleware('permission:devolucion.index');
    Route::post('/devolucion/registrar', 'DevolucionesController@store')->name('devolucion.store')->middleware('permission:devolucion.store');
    Route::put('/devolucion/anular', 'DevolucionesController@deshabilitar')->name('devolucion.delete');
    Route::get('/devolucion/datos','DevolucionesController@DatosDevolucion')->name('devolucion.data');
    Route::get('/devolucion/print/{id}','DevolucionesController@PrintDevoluciones')->name('devolucion.print');
    
    //Rutas de Talonario
    Route::get('/talonario','TalonarioController@index')->name('talonario.index')->middleware('permission:talonario.index');
    Route::post('/talonario/registrar', 'TalonarioController@store')->name('talonario.store')->middleware('permission:talonario.store');
    Route::get('/talonario/ultimo','TalonarioController@ultimoRegistro')->name('talonario.last');
    Route::get('/talonario/pendiente','TalonarioController@VerificarVendedor');
    Route::get('/talonario/contraseña','TalonarioController@ConfirmarTalonario')->name('talonario.password')->middleware('permission:talonario.password');;
    Route::get('/talonario/listado','TalonarioController@listadoAsignacion')->name('talonario.list');
    Route::put('/talonario/deshabilitar', 'TalonarioController@deshabilitar')->name('talonario.disable');
    Route::post('/talonario/eliminar', 'TalonarioController@EliminarTalonario')->name('talonario.delete')->middleware('permission:talonario.delete');
    Route::get('/talonario/disponible','TalonarioController@RecibosDisponible')->name('talonario.enable');
    
    // Rutas De Recibos
    Route::get('/recibo', 'PagosCxCController@index')->name('recibo.index')->middleware('permission:recibo.index');
    Route::post('/recibo/registrar', 'PagosCxCController@store')->name('recibo.store')->middleware('permission:recibo.store');
    Route::get('/recibo/contraseña','PagosCxCController@ConfirmarAnulacion')->name('recibo.password');
    Route::put('/recibo/deshabilitar', 'PagosCxCController@deshabilitar')->name('recibo.disable')->middleware('permission:recibo.disable');
    Route::get('/recibo/datos','PagosCxCController@DatosRecibo')->name('recibo.data');
    Route::get('/recibo/datos2','PagosCxCController@DatosRecibo2')->name('recibo.data2');
    Route::put('/recibo/actualizar', 'PagosCxCController@update')->name('recibo.update')->middleware('permission:recibo.update');
    Route::get('/recibo/last', 'PagosCxCController@lastRef')->name('recibo.last');

    //Rutas de Comision
    Route::get('/comision','ComisionController@index')->name('comision.index')->middleware('permission:comision.index');
    Route::post('/comision/registrar', 'ComisionController@store')->name('comision.store')->middleware('permission:comision.store');
    Route::get('/comision/fecha','ComisionController@Fechas')->name('comision.date');
    Route::get('/comision/fechaactual','ComisionController@fechaActual')->name('comision.now');
    Route::get('/comision/datos','ComisionController@DatosComision')->name('comision.data');
    Route::get('/comision/detalle','ComisionController@DetalleFactura')->name('comision.details');
    Route::get('/comision/solicitud/{id}/{qunice}/{mes}/{anio}','ComisionController@generarComisiones')->name('comision_pdf');
    Route::get('/comision/vendedor/{id}','ComisionController@DetalleComisiones')->name('comisionvendedor_pdf');
    
    //Rutas de Cuentas por Cobrar
    Route::get('/cxc/comision','CuentasxCobrarController@Comision')->name('cxc.gain');
    Route::get('/cxc/usuario','CuentasxCobrarController@NameUser')->name('cxc.user');

    //Rutas de CHEQUE
    Route::get('/cheque', 'ChequeController@index')->name('cheque.index')->middleware('permission:cheque.index');
    Route::post('/cheque/registrar', 'ChequeController@store')->name('cheque.store')->middleware('permission:cheque.store');
    Route::get('/cheque/contraseña','ChequeController@ConfirmarAnulacion')->name('cheque.password')->middleware('permission:cheque.password');
    Route::put('/cheque/actualizar', 'ChequeController@update')->name('cheque.update')->middleware('permission:cheque.update');
    Route::put('/cheque/active', 'ChequeController@habilitar')->name('cheque.active')->middleware('permission:cheque.active');
    Route::put('/cheque/inactive', 'ChequeController@deshabilitar')->name('cheque.inactive')->middleware('permission:cheque.inactive');
    Route::get('/cheque/datos','ChequeController@DatosCheque')->name('cheque.data');
    
    // RUTAS DE ENTRADAS
    Route::get('/entrada','EntradaController@index')->name('entrada.index')->middleware('permission:entrada.index');
    Route::post('/entrada/registrar', 'EntradaController@store')->name('entrada.store')->middleware('permission:entrada.store');
    Route::get('/entrada/datos','EntradaController@DatosEntrada')->name('entrada.data');
    Route::put('/entrada/inactive', 'EntradaController@deshabilitar')->name('entrada.inactive')->middleware('permission:entrada.inactive');
    Route::put('/entrada/actualizar', 'EntradaController@update')->name('entrada.update')->middleware('permission:entrada.update');
    Route::get('/entrada/costo','EntradaController@CostoProducto')->name('entrada.costo');
    Route::get('/entrada/id','EntradaController@ultimoRegistro')->name('entrada.id');
    Route::get('/entrada/print/{id}','EntradaController@printEntrada');
    
    // RUTAS DE SALIDAS
    Route::get('/salida','SalidaController@index')->name('salida.index')->middleware('permission:salida.index');
    Route::post('/salida/registrar', 'SalidaController@store')->name('salida.store')->middleware('permission:salida.store');
    Route::get('/salida/datos','SalidaController@DatosSalida')->name('salida.data');
    Route::put('/salida/inactive', 'SalidaController@deshabilitar')->name('salida.inactive')->middleware('permission:salida.inactive');
    Route::put('/salida/actualizar', 'SalidaController@update')->name('salida.update')->middleware('permission:salida.update');
    Route::get('/salida/id','SalidaController@ultimoRegistro')->name('salida.id');
    Route::get('/salida/print/{id}','SalidaController@printSalida');

    // RUTAS DE ADMON BONIFICACION
    Route::get('/admon','AdmonBonificacionController@index')->name('admon.index')->middleware('permission:admon.index');
    Route::post('/admon/registrar', 'AdmonBonificacionController@store')->name('admon.store')->middleware('permission:admon.store');
    Route::post('/admon/actualizar', 'AdmonBonificacionController@update')->name('admon.update')->middleware('permission:admon.update');
    Route::get('/admon/datos','AdmonBonificacionController@DatosAdmonBonificacion')->name('admon.data');
    Route::put('/admon/inactive', 'AdmonBonificacionController@deshabilitar')->name('admon.inactive')->middleware('permission:admon.inactive');
    Route::get('/admon/regalia', 'AdmonBonificacionController@ProdBonificacion')->name('admon.bonus');
    
    // RUTAS DE COTIZACION
    Route::get('/cotizacion','CotizacionController@index')->name('cotizacion.index')->middleware('permission:cotizacion.index');
    Route::post('/cotizacion/registrar', 'CotizacionController@store')->name('cotizacion.store')->middleware('permission:cotizacion.store');
    Route::post('/cotizacion/facturar','CotizacionController@facturar')->name('cotizacion.invoice')->middleware('permission:cotizacion.invoce');
    Route::put('/cotizacion/actualizar','CotizacionController@update')->name('cotizacion.update');
    Route::put('/cotizacion/estado','CotizacionController@Estado')->name('cotizacion.state');
    Route::get('/cotizacion/detalle','CotizacionController@DetalleCotizacion')->name('cotizacion.detail');
    Route::get('/cotizacion/datos','CotizacionController@DatosCotizacion')->name('cotizacion.data');
    
    // Rutas de Compra
    Route::get('/compra','CompraController@index')->name('compra.index')->middleware('permission:compra.index');
    Route::post('/compra/registrar', 'CompraController@store')->name('compra.store')->middleware('permission:compras.store');
    Route::get('/compra/datos','CompraController@DatosCompra')->name('compra.data');
    Route::put('/compra/actualizar', 'CompraController@update')->name('compra.update')->middleware('permission:compra.update');
    
    // Rutas de Nota Credito
    Route::get('/nota','NotasCreditoController@index')->name('nota.index')->middleware('permission:nota.index');
    Route::post('/nota/registrar', 'NotasCreditoController@store')->name('nota.store')->middleware('permission:nota.store');
    Route::get('/nota/datos','NotasCreditoController@DatosNotaC')->name('nota.data');
    Route::put('/nota/actualizar', 'NotasCreditoController@update')->name('nota.update')->middleware('permission:nota.update');
    Route::get('/nota/contraseña','NotasCreditoController@ConfirmarActualizacion')->name('nota.password');
    Route::get('/nota/saldo','NotasCreditoController@SaldoRestante')->name('nota.balances');
    
    // Rutas de Nota Debito
    Route::get('/notad','NotaDebitoController@index')->name('notad.index')->middleware('permission:notad.index');
    Route::get('/notad/datos','NotaDebitoController@DatosNotaD')->name('notad.data');
    Route::put('/notad/actualizar', 'NotaDebitoController@update')->name('notad.update')->middleware('permission:notad.update');
    Route::get('/notad/contraseña','NotaDebitoController@ConfirmarActualizacion')->name('notad.password');
    Route::put('/notad/aplicar','NotaDebitoController@aplicar')->name('notad.apply')->middleware('permission:notad.apply');
    
    //Rutas de Usarios del sistema
    Route::get('/usuario', 'UserController@index')->name('user.index')->middleware('permission:user.index');
    Route::post('/usuario/registrar', 'UserController@store')->name('user.store')->middleware('permission:user.store');
    Route::put('/usuario/actualizar', 'UserController@update')->name('user.update')->middleware('permission:user.update');
    Route::put('/usuario/inactive', 'UserController@deshabilitar')->name('user.inactive')->middleware('permission:user.inactive');
    Route::put('/usuario/active', 'UserController@habilitar')->name('user.active')->middleware('permission:user.active');
    Route::get('/usuario/seleccionar', 'UserController@listarusuario')->name('user.select');
    Route::get('/permiso','UserController@permisos')->name('user.permiso');
    Route::get('/permisousuario','UserController@permisosUsuarios')->name('user.permisouser');
    Route::post('/permiso/registrar','UserController@storepermisos')->name('permiso.agregar');
    Route::delete('/permiso/eliminar/{id}','UserController@destroypermiso')->name('permiso.eliminar');
    //Route::get('/login','UserController@show')->name('usuarios.login');

    //Rutas de CHEQUE MINUTA
    Route::get('/chequeminuta', 'ChequeMinutaController@index')->name('chequeminuta.index')->middleware('permission:chequeminuta.index');
    Route::put('/chequeminuta/actualizar', 'ChequeMinutaController@update')->name('chequeminuta.update')->middleware('permission:chequeminuta.update');
    Route::put('/chequeminuta/active', 'ChequeMinutaController@habilitar')->name('chequeminuta.active')->middleware('permission:chequeminuta.active');
    Route::put('/chequeminuta/inactive', 'ChequeMinutaController@deshabilitar')->name('chequeminuta.inactive')->middleware('permission:chequeminuta.inactive');
    Route::get('/chequeminuta/datos','ChequeMinutaController@DatosCheque')->name('chequeminuta.data');
    //Route::get('/cheque/contraseña','ChequeController@ConfirmarAnulacion')->name('cheque.password')->middleware('permission:cheque.password');
    
    Route::get('/kardex','KardexController@CargarKardex')->name('kardex.productname');
    Route::get('/estadocuenta','EstadoController@CargarEstado')->name('estadocuenta.cliente');

    Route::get('/informe/factcontcred','InformesController@FacturaContCred')->name('factura.ContadoCredito');
    Route::get('/informe/factcontado','InformesController@FacturaContado')->name('factura.Contado');
    Route::get('/informe/factcredito','InformesController@FacturaCredito')->name('factura.Credito');
    Route::get('/informe/factanulado','InformesController@FacturaAnulada')->name('factura.Anulada');
    Route::get('/informe/factvendedor','InformesController@FacturaVendedor')->name('factura.Vendedor');
    Route::get('/informe/factcliente','InformesController@FacturaCliente')->name('factura.Clientes');
    Route::get('/informe/factzona','InformesController@FacturaZona')->name('factura.Zonas');
    Route::get('/informe/factzonaall','InformesController@FacturaZonaAll')->name('factura.Zonas');
    Route::get('/informecxc/recibo','InformesController@ReciboVendedor')->name('informe.recibo');
    Route::get('/informecxc/calendario','InformesController@CalendarioCobro')->name('informe.calendario');
    Route::get('/informecxc/calendarioall','InformesController@CalendarioCobroAll')->name('informe.calendarioall');
    Route::get('/informe/chequevend','InformesController@ChequeVendedor')->name('cheque.vendedor');
    Route::get('/informe/chequepend','InformesController@ChequePendiente')->name('cheque.pendiente');
    Route::get('/informe/chequecobrado','InformesController@ChequeCobrado')->name('cheque.cobrado');
    Route::get('/informe/chequeanulado','InformesController@ChequeAnulado')->name('cheque.anulado');
    Route::get('/informe/ventaproductoexcel','InformesController@VentaProducto');
    Route::get('/informe/ventaproducto','InformesController@ExcelVentas');
    Route::get('/informe/devolucioncliente','InformesController@DevolucionCliente');
    Route::get('/informe/estadocuentavivas/{id}','InformesController@FacturasVivas');
    Route::get('/informe/saldovendedor','InformesController@SaldoVendedor');
    Route::get('/informe/controlproducto','InformesController@ControlProducto');
    Route::get('/informe/entradaprod','InformesController@DetalleEntrada');
    Route::get('/informe/salidaprod','InformesController@DetalleSalida');
    Route::get('/informe/prefactVendedor','InformesController@PrefacturaVendedor');

    Route::get('/factura/pdf/{id}/{nota}','FacturaController@generarFactura')->name('factura_pdffactura');
    Route::get('/factura/pdf2/{id}/{nota}','FacturaController@generarFacturaMini')->name('factura_pdf2factura');
    Route::get('/factura/pdfdolar/{id}/{nota}','FacturaController@generarFacturaDolar')->name('factura_pdffactura');
    Route::get('/factura/pdfdolar2/{id}/{nota}','FacturaController@generarFacturaMiniDolar')->name('factura_pdf2factura');
    Route::get('/factura/pdfcinta/{id}/{nota}','FacturaController@generarFacturaCinta')->name('facturacinta');
    //Route::post('/factura/pdfcinta','FacturaController@generarFacturaCinta')->name('facturacinta');
    //Route::get('/factura/pdfcinta2/{id}/{nota}','FacturaController@generarFacturaCinta2')->name('facturacinta');
    Route::get('/cotizacion/pdf/{id}','CotizacionController@generarCotizacion')->name('cotizacion_pdf');
    Route::get('/cotizacion/pdf2/{id}','CotizacionController@generarCotizacionMini')->name('cotizacion_pdf2');
    Route::get('/factura/id', 'FacturaController@ultimoRegistro')->name('factura.id');
    Route::get('/factura/pagina1/{id}','FacturaController@pagina1');
    Route::get('/factura/pagina2/{id}','FacturaController@pagina2');
    Route::get('/factura/pagina3/{id}','FacturaController@pagina3');
    Route::get('/factura/pagina4/{id}','FacturaController@pagina4');
    Route::get('/factura/pagina12/{id}','FacturaController@pagina12');
    Route::get('/factura/pagina34/{id}','FacturaController@pagina34');

    Route::get('estadistica/catVend','EstVentasController@index');

    Route::get('/kardex/print/{idProd}/{Inicio}/{Fin}','KardexController@PrintProduct')->name('kardex.printproduct');
    Route::get('/kardex/summary/{Inicio}/{Fin}','KardexController@SummaryProduct')->name('kardex.summaryproduct');

    Route::get('/fecha','PagosCxCController@fechadia');

    Route::get('/correo/enviar', 'MailDemoController@store');
    Route::get('/prefactura/productos','PrefacturaController@ProdPrefactura');
    Route::get('/prefactura/consulta','PrefacturaController@ConsultaInventario');
    Route::get('/zip','CambiaProductoController@zip');
    Route::get('/deletezip','CambiaProductoController@deleteZip');

    Route::get('/prodvenc','ProductosController@prodVencidos');
    Route::post('/prodvenc/registrar','ProductoVencidosController@store');
});
