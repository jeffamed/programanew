const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/js/app.js', 'public/js');

mix.styles([
    'resources/template/css/bootstrap.min.css',
    'resources/template/css/animate.min.css',
    'resources/template/css/light-bootstrap-dashboard.css',
    'resources/template/css/demo.css',
    'resources/template/css/pe-icon-7-stroke.css',
    'resources/template/css/toastr.min.css'
], 'public/css/all.css')
   .scripts([
       'resources/template/js/jquery.3.2.1.min.js',
       'resources/template/js/bootstrap.min.js',
      'resources/template/js/chartist.min.js',
//       'resources/template/js/bootstrap-notify.js',
       'resources/template/js/light-bootstrap-dashboard.js',
       'resources/template/js/demo.js',
       'resources/template/js/sweetalert2.js',
       'resources/template/js/toastr.js',
], 'public/js/all.js')
    .js('resources/js/app.js', 'public/js/app.js');